package textadventure;

import java.util.HashMap;
import java.util.Map;

public class Inventory
{

    public static void listAll(HashMap<Item, Integer> inventory)
    {
        if (inventory.isEmpty())
        {
            System.out.println("Nothing in inventory!");
            return;
        }

        System.out.println("   - Inventory -");

        int add = 1;
        String name = "";

        for (Map.Entry<Item, Integer> entry : inventory.entrySet())
        {
            if (entry.getKey() instanceof UseableItem)
                name = CP.getColorString(entry.getKey().name_, CP.GREEN);
            else
                name = entry.getKey().name_;

            if (add == 1)
            {
                System.out.print(name + " [x" + entry.getValue() + "]");
                add++;
            } else
            {
                System.out.print("\t");
                System.out.println(name + " [x" + entry.getValue() + "]");
                add = 1;
            }

        }

        if (add == 2)
            System.out.println();
    }

    public static Item getItemStartingWith(HashMap<Item, Integer> inventory, String name)
    {
        if (!inventory.isEmpty())
        {
            for (Map.Entry<Item, Integer> entry : inventory.entrySet())
            {
                if (entry.getKey().name_.toLowerCase().startsWith(name.toLowerCase()))
                {
                    return entry.getKey();
                }

            }
        }

        return null;
    }

}
  
