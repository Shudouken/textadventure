package textadventure;

import textadventure.skills.*;

public abstract class Skill implements java.io.Serializable
{

    public int skilllevel_;
    public int maxlevel_;
    public int basecost_;
    public boolean passive_;
    public boolean offensive_;
    public String name_;

    public final static Skill aura_blade_ = new AuraBlade();
    public final static Skill blessing_ = new Blessing();
    public final static Skill charm_ = new Charm();
    public final static Skill cloaking_ = new Cloaking();
    public final static Skill drop_weapon_ = new DropWeapon();
    public final static Skill exorcise_ = new Exorcise();
    public final static Skill feast_ = new Feast();
    public final static Skill last_resort_ = new LastResort();
    public final static Skill limit_break_ = new LimitBreak();
    public final static Skill longevity_ = new Longevity();
    public final static Skill perfection_ = new Perfection();
    public final static Skill quick_attack_ = new QuickAttack();
    public final static Skill sacrament_ = new Sacrament();
    public final static Skill sanctus_ = new Sanctus();
    public final static Skill scrutinize_ = new Scrutinize();
    public final static Skill shenanigan_ = new Shenanigan();
    public final static Skill shield_bash_ = new ShieldBash();
    public final static Skill soul_breaker_ = new SoulBreaker();
    public final static Skill steal_ = new Steal();
    public final static Skill vastu_shastra_ = new VastuShastra();

    public abstract String getDescription();

    public abstract int getCost();

    public abstract void effect(Game game, Player seme, Player uke);

    public Skill(String name, int cost, int maxlevel)
    {
        name_ = name;
        basecost_ = cost;
        skilllevel_ = 1;
        maxlevel_ = maxlevel;

        if (maxlevel == 0)
            passive_ = true;
        else
            passive_ = false;

        offensive_ = true;
    }

    public void levelUp()
    {
        if (skilllevel_ < maxlevel_)
            skilllevel_ += 1;
    }

    public static boolean skillCost(Player caster, int mp)
    {
        boolean cost_met = false;

        if (caster.stats_.mp_ >= mp)
        {
            cost_met = true;
            caster.stats_.loseMP(mp);
        } else
        {
            System.out.println(caster.name_ + " has run out of mp!");
        }

        return cost_met;
    }

    public String toString()
    {
        String temp = "";
        String eol = System.getProperty("line.separator");

        if (passive_)
            temp += name_ + " - Passive " + eol;
        else
            temp += name_ + " - Level: " + skilllevel_ + "/" + maxlevel_ + " - Cost: "
                    + CP.getColorString("" + getCost(), CP.BLUE) + " MP" + eol;

        temp += "" + getDescription();

        return temp;
    }

    @Override
    public int hashCode()
    {
        return name_.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof Skill))
            return false;
        if (obj == this)
            return true;
        if (((Skill) obj).name_.equals(name_))
            return true;

        return false;
    }

}
