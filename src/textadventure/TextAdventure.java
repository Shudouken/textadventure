package textadventure;

public class TextAdventure
{

    public static void main(String[] args)
    {
        Game game = null;

        if (args.length == 1)
        {
            //load game
            System.out.println("Loading File: " + args[0]);
            game = FileIO.loadFile(args[0]);
            Game.setInstance(game);
            if (game == null)
            {
                System.out.println("Error: Loading Failed!");
                return;
            }
        } else if (args.length > 1)
        {
            System.out.println("Usage: " + Game.title_ + ".jar [file to load]");
            return;
        } else
        {
            //start new game
            game = Game.getInstance();
            game.newGame();
        }

        game.run();

    }

}
