package textadventure;

import java.util.HashMap;

public class SkillTree implements java.io.Serializable
{

    public static enum Profession
    {
        BRUTE, TEMPLAR, WIZARD, MONK, SPECIALIST, PICKPOCKET
    }

    public Profession class_;
    private HashMap<Integer, Skill> skilltree_;

    private final static HashMap<Integer, Skill> brute_skills_;

    static
    {
        brute_skills_ = new HashMap<>();

        brute_skills_.put(0, Skill.last_resort_);
        brute_skills_.put(8, Skill.feast_);
    }

    private final static HashMap<Integer, Skill> templar_skills_;

    static
    {
        templar_skills_ = new HashMap<>();

        templar_skills_.put(0, Skill.blessing_);
        templar_skills_.put(6, Skill.shield_bash_);
        templar_skills_.put(15, Skill.longevity_);
    }

    private final static HashMap<Integer, Skill> wizard_skills_;

    static
    {
        wizard_skills_ = new HashMap<>();

        wizard_skills_.put(0, Skill.sacrament_);
        wizard_skills_.put(5, Skill.soul_breaker_);
        wizard_skills_.put(11, Skill.vastu_shastra_);
    }

    private final static HashMap<Integer, Skill> monk_skills_;

    static
    {
        monk_skills_ = new HashMap<>();

        monk_skills_.put(0, Skill.exorcise_);
        monk_skills_.put(4, Skill.sanctus_);
    }

    private final static HashMap<Integer, Skill> specialist_skills_;

    static
    {
        specialist_skills_ = new HashMap<>();

        specialist_skills_.put(0, Skill.aura_blade_);
        specialist_skills_.put(7, Skill.limit_break_);
        specialist_skills_.put(10, Skill.scrutinize_);
    }

    private final static HashMap<Integer, Skill> pickpocket_skills_;

    static
    {
        pickpocket_skills_ = new HashMap<>();

        pickpocket_skills_.put(0, Skill.steal_);
        pickpocket_skills_.put(2, Skill.quick_attack_);
        pickpocket_skills_.put(8, Skill.drop_weapon_);
        pickpocket_skills_.put(10, Skill.cloaking_);
    }

    public SkillTree(Profession c)
    {
        class_ = c;

        if (c == Profession.BRUTE)
            skilltree_ = brute_skills_;
        else if (c == Profession.TEMPLAR)
            skilltree_ = templar_skills_;
        else if (c == Profession.WIZARD)
            skilltree_ = wizard_skills_;
        else if (c == Profession.MONK)
            skilltree_ = monk_skills_;
        else if (c == Profession.SPECIALIST)
            skilltree_ = specialist_skills_;
        else
            skilltree_ = pickpocket_skills_;
    }

    public Skill advance(int level)
    {
        if (skilltree_.containsKey(level))
        {
            Skill skill = skilltree_.get(level);

            if (skill.passive_)
                skill.effect(null, Game.getInstance().player_, null);

            return skill;
        }

        return null;
    }

    public int getNextSkillCount(int level)
    {
        int r = 0;
        while (level < 99)
        {
            if (skilltree_.containsKey(level))
                return r;
            level++;
            r++;
        }

        return 0;
    }

}
