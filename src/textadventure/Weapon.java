package textadventure;

public abstract class Weapon extends Item
{

    public boolean attack_twice_ = false;
    public boolean targets_res_ = false;
    public float crit_damage_ = 2.0f;

    public Weapon(String name, String desc, int price)
    {
        super(name, desc, price);
    }

    public abstract int calcAttack(Stats stats);

    public abstract float calcAccuracy(Stats stats);

    public abstract float calcCritical(Stats stats);

    public abstract int getScaling(Stats stats);

    public int getScaledAtk(Stats stats)
    {
        return calcAttack(stats) * getScaling(stats);
    }

}
