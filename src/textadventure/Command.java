package textadventure;

import textadventure.tiles.PlayerTile;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class Command
{

    public static boolean debug = true;
    public static boolean enemy_turn_ = false;
    public static boolean freerun_ = false;

    public static String[] last_command_;
    private static String[] params_;
    private static Mode m_;
    public static Skill last_skill_;
    public static Spell last_spell_;

    public static enum Mode
    {
        MAP, BATTLE
    }

    private static boolean sanityCheck(boolean condition)
    {
        if(condition)
        {
            CP.println("Unknown Command: " + Arrays.toString(params_), CP.RED);
            System.out.println("Type h or help for a list of commands");
            return true;
        }
        return false;
    }

    private static void printHelp(Game game)
    {
        System.out.println("List of available commands - (Terms in [] an be omitted)");
        CP.println("  help, h", CP.BOLD);
        System.out.println("    shows this help screen");
        CP.println("  status, s", CP.BOLD);
        System.out.println("    shows the player's status screen");
        CP.println("  info [prefix], i [prefix]", CP.BOLD);
        System.out.println("    omitting [prefix]");
        System.out.println("      displays whole inventory, green colored items are useable");
        System.out.println("    with [prefix]");
        System.out.println("      gives info on first item in inventory starting with prefix");
        CP.println("  monsterinfo [prefix, level], mi [prefix, level]", CP.BOLD);
        System.out.println("    omitting [prefix, level]");
        System.out.println("      displays whole monsterbook, monsters are added when you inspect them");
        System.out.println("    with [prefix]");
        System.out.println("      gives info on first monster in monsterbook starting with prefix");
        System.out.println("    with [prefix, level]");
        System.out.println("      gives info on first monster in monsterbook starting with prefix");
        System.out.println("      leveled up to the specified level");

        if(! game.player_.tome_.isEmpty())
        {
            CP.println("  tome [prefix, line], t [prefix, line]", CP.BOLD);
            System.out.println("    omitting [prefix, line]");
            System.out.println("      displays whole tome, spells are added as you cast them");
            System.out.println("    with [prefix]");
            System.out.println("      gives info on first keyword in the tome starting with prefix");
            System.out.println("    with [prefix, line]");
            System.out.println("      rewrites the first keyword's description starting with prefix to line");
            System.out.println("      put lines including spaces in double quotes");
        }

        CP.println("  use prefix, u prefix", CP.BOLD);
        System.out.println("    uses first item in inventory starting with prefix");
        CP.println("  equip [prefix], e [prefix]", CP.BOLD);
        System.out.println("    omitting [prefix]");
        System.out.println("      displays player's equipment information");
        System.out.println("    with [prefix]");
        System.out.println("      tries to equip item starting with prefix");
        CP.println("  skilltree [prefix], st [prefix]", CP.BOLD);
        System.out.println("    omitting [prefix]");
        System.out.println("      displays player's skill information");
        System.out.println("    with [prefix]");
        System.out.println("      tries to level up skill starting with prefix");
        CP.println("  useskill prefix, us prefix", CP.BOLD);
        System.out.println("    tries to use the skill starting with prefix");
        CP.println("  cast, c", CP.BOLD);
        System.out.println("    enter spellcasting mode, use known keywords to cast spells");
        CP.println("  save filename, sa filename", CP.BOLD);
        System.out.println("    creates savefile with name filename.sav");
        CP.println("  quit, q", CP.BOLD);
        System.out.println("    quit game");
        System.out.println();
        System.out.println("Additionally available commands on map:");
        CP.println("  freerun, f", CP.BOLD);
        System.out.println("    enters freerun mode. move with arrow keys, wasd or numpad(8,4,2,6)");
        System.out.println();
        System.out.println("Additionally available commands in battle:");
        CP.println("  attack, a", CP.BOLD);
        System.out.println("    attack the enemy with your weapon.");
        CP.println("  run, r", CP.BOLD);
        System.out.println("    try to run from a battle, success depends on player's agility");
    }

    private static final Map<String, Consumer<Game>> commands_;
    static
    {
        commands_ = new HashMap<>();

        commands_.put("h"               ,Command::printHelp);
        commands_.put("help"            ,Command::printHelp);
        commands_.put("s"               ,Command::printStatus);
        commands_.put("status"          ,Command::printStatus);
        commands_.put("sa"              ,Command::saveGame);
        commands_.put("save"            ,Command::saveGame);
        commands_.put("e"               ,Command::editEquip);
        commands_.put("equip"           ,Command::editEquip);
        commands_.put("i"               ,Command::infoItem);
        commands_.put("info"            ,Command::infoItem);
        commands_.put("mi"              ,Command::infoMonster);
        commands_.put("monsterinfo"     ,Command::infoMonster);
        commands_.put("u"               ,Command::useItem);
        commands_.put("use"             ,Command::useItem);
        commands_.put("us"              ,Command::useSkill);
        commands_.put("useskill"        ,Command::useSkill);
        commands_.put("c"               ,Command::castSpell);
        commands_.put("cast"            ,Command::castSpell);
        commands_.put("st"              ,Command::skillTree);
        commands_.put("skilltree"       ,Command::skillTree);
        commands_.put("t"               ,Command::tome);
        commands_.put("tome"            ,Command::tome);

        //map only
        commands_.put("f"               ,Command::freeRun);
        commands_.put("freerun"         ,Command::freeRun);

        //battle only
        commands_.put("a"               ,(Game game) -> attack(game.player_,game.enemy_));
        commands_.put("attack"          ,(Game game) -> attack(game.player_,game.enemy_));
        commands_.put("r"               ,Command::run);
        commands_.put("run"             ,Command::run);

        //debug
        commands_.put("hs"              ,Command::printHiddenStatus);
        commands_.put("he"              ,Command::printHiddenEquipInfo);
        commands_.put("lu"              ,Command::levelUp);
        commands_.put("ls"              ,Command::learnSpells);

    }

    public static void handleCommand(String input, Game game, Mode m)
    {
        params_ = input.split(" (?=(([^\"]*[\"]){2})*[^\"]*$)");
        m_ = m;

        int count = 0;
        for (String in : params_)
        {
            if (in.startsWith("\"") && in.endsWith("\""))
            {
                params_[count] = in.replaceAll("\"", "");
            }

            count++;
        }

        String command = params_[0];
        enemy_turn_ = false;
        last_command_ = null;
        last_skill_ = null;
        last_spell_ = null;

        if(commands_.containsKey(command))
            commands_.get(command).accept(game);
        else
            sanityCheck(true);

        last_command_ = params_;

        enemyTurn(game);
    }

    private static void enemyTurn(Game game)
    {
        if (game.enemy_ != null && enemy_turn_)
        {
            if (game.enemy_.stats_.hp_ > 0)
                game.enemy_.action(game, game.player_);

            if (game.enemy_.wait_turn_ && game.player_.wait_turn_)
            {
                game.enemy_.wait_turn_ = false;
                game.player_.wait_turn_ = false;
            }

            if (game.enemy_.stats_.hp_ > 0 && game.player_.wait_turn_)
            {
                game.enemy_.action(game, game.player_);
                game.player_.wait_turn_ = false;
            }

            if (game.player_.stats_.getClass() == Buff.class)
            {
                game.player_.stats_ = ((Buff) game.player_.stats_).countDown();
            }

            if (game.enemy_ != null && game.enemy_.stats_.getClass() == Buff.class)
            {
                game.enemy_.stats_ = ((Buff) game.enemy_.stats_).countDown();
            }

            if(game.player_.aoe_ != null)
            {
                game.player_.aoe_.aoeDamage(game.player_);
            }

            if(game.enemy_.aoe_ != null)
            {
                game.enemy_.aoe_.aoeDamage(game.enemy_);
            }

            StatusEffects.checkPoison(game.enemy_);
        }
        StatusEffects.checkPoison(game.player_);
    }

    public static void skillTree(Game game)
    {
        if (params_.length == 1)
        {
            System.out.println("   - Known Skills -");
            game.player_.listSkills();
            System.out.println("Available Skillpoints: " + game.player_.skillpoints_);
        } else if (params_.length == 2)
        {
            for (Map.Entry<String, Skill> entry : game.player_.skills_.entrySet())
            {
                if (entry.getKey().toLowerCase().startsWith(params_[1]))
                {
                    if (Game.getInstance().player_.skillpoints_ > 0)
                    {
                        if (entry.getValue().skilllevel_ < entry.getValue().maxlevel_)
                        {
                            entry.getValue().levelUp();
                            Game.getInstance().player_.skillpoints_ -= 1;
                            System.out.println(entry.getValue().name_ + "'s level increased to " + entry.getValue().skilllevel_);
                            enemy_turn_ = true;
                        } else
                        {
                            System.out.println(entry.getValue().name_ + " is already at max level!");
                        }

                        return;
                    } else
                    {
                        System.out.println("Not enough skillpoints to level up " + entry.getValue().name_ + "!");
                        return;
                    }
                }
            }
            System.out.println("No skill found for " + params_[1]);
        } else
            System.out.println("Usage: skilltree [skill_to_level]");


    }

    public static void freeRun(Game game)
    {
        if(sanityCheck(m_ == Mode.BATTLE))
            return;

        try
        {
            Game.reader.clearScreen();
            int k = 0;
            freerun_ = true;
            while (freerun_ && (k == 0
                    || k == 52 || k == 54 || k == 50 || k == 56    //numlock directions
                    || k == 2 || k == 6 || k == 14 || k == 16      //arrow keys
                    || k == 97 || k == 100 || k == 115 || k == 119 //wasd
            ))
            {


                System.out.print(game.map_);
                String hp = CP.getColorString("" + game.player_.stats_.hp_, CP.RED);
                String mp = CP.getColorString("" + game.player_.stats_.mp_, CP.BLUE);
                System.out.println();
                System.out.println(game.player_.name_ + "(" + hp + "/" + mp + ")");
                k = Game.reader.readVirtualKey();
                Game.reader.clearScreen();

                if (k == 52 || k == 2 || k == 97)
                    game.pt_.move(game.map_, PlayerTile.Direction.LEFT);
                else if (k == 54 || k == 6 || k == 100)
                    game.pt_.move(game.map_, PlayerTile.Direction.RIGHT);
                else if (k == 56 || k == 16 || k == 119)
                    game.pt_.move(game.map_, PlayerTile.Direction.UP);
                else if (k == 50 || k == 14 || k == 115)
                    game.pt_.move(game.map_, PlayerTile.Direction.DOWN);
            }

            freerun_ = false;
            System.out.print(game.map_);

        } catch (Exception e)
        {
            freerun_ = false;
            System.out.println(e.toString());
        }
    }

    public static void attack(Player seme, Player uke)
    {
        if(sanityCheck(m_ == Mode.MAP))
            return;

        if(StatusEffects.checkCharm(seme,uke))
        {
            enemy_turn_ = true;
            return;
        }

        //seme
        int atk = seme.equip_.weapon_.calcAttack(seme.stats_);
        float acc = seme.equip_.weapon_.calcAccuracy(seme.stats_);
        float crit = seme.equip_.weapon_.calcCritical(seme.stats_);

        //uke
        int def = uke.equip_.armor_.calcDefense(uke.stats_);
        int res = uke.equip_.armor_.calcResistance(uke.stats_);
        float eva = uke.equip_.armor_.calcEvasion(uke.stats_);

        if (seme.equip_.weapon_.attack_twice_)
            uke.takeDamage(seme, atk, acc, crit, def, res, eva);

        uke.takeDamage(seme, atk, acc, crit, def, res, eva);
        enemy_turn_ = true;
    }

    private static void run(Game game)
    {
        if(sanityCheck(m_ == Mode.MAP))
            return;

        System.out.print("Trying to run from " + game.enemy_.name_ + " ");

        for (int i = 0; i < 3; i++)
        {
            try
            {
                Thread.sleep(300);
                System.out.print(".");
            } catch (InterruptedException e)
            {
                //Handle exception
            }
        }

        int success = (game.player_.stats_.agi_ - game.enemy_.stats_.agi_) * 7;
        if (success < 0) success = 0;

        if (Randomizer.randInt(0, 100) <= success)
        {
            game.player_.wait_turn_ = false;
            game.map_.removeTile(game.enemy_.et_.pos_);
            game.map_.enemies_.remove(game.enemy_.et_);
            game.enemy_.et_ = null;
            game.enemy_ = null;
            game.mode_ = Command.Mode.MAP;
            System.out.println(" successfully escaped!");
        } else
        {
            System.out.println(" couldn't escape!");
            enemy_turn_ = true;
        }
    }

    private static void useSkill(Game game)
    {
        if (params_.length != 2)
        {
            System.out.println("Useage: skill [skillname]");
        } else
        {
            for (Map.Entry<String, Skill> entry : game.player_.skills_.entrySet())
            {
                if (entry.getKey().toLowerCase().startsWith(params_[1]))
                {
                    if (entry.getValue().passive_)
                    {
                        System.out.println(entry.getValue().name_ + " is a passive skill, it can't be used!");
                        return;
                    }
                    if(entry.getValue().offensive_)
                        if(StatusEffects.checkCharm(game.player_, game.enemy_))
                        {
                            enemy_turn_ = true;
                            return;
                        }
                    last_skill_ = entry.getValue();
                    last_skill_.effect(game, game.player_, game.enemy_);
                    enemy_turn_ = true;
                    return;
                }
            }

            System.out.println("Unknown skill: " + params_[1]);
        }
    }

    private static void castSpell(Game game)
    {
        if (params_.length != 1)
            System.out.println("Useage: cast");
        else
        {
            Casting.casting(game);
            enemy_turn_ = true;
        }
    }

    private static void levelUp(Game game)
    {
        if(sanityCheck(!debug))
            return;

        if (params_.length == 2)
        {
            int n = Integer.parseInt(params_[1]);
            for (int i = 0; i < n; i++)
            {
                game.player_.levelUp();
            }
        } else
        {
            game.player_.levelUp();
        }
    }

    private static void saveGame(Game game)
    {
        if (params_.length != 2)
            System.out.println("Usage: save filename[.sav]");
        else
            FileIO.saveFile(params_[1], game);
    }

    private static void editEquip(Game game)
    {
        if (params_.length == 1)
        {
            System.out.println(game.player_.equip_);
        } else if (params_.length > 2)
        {
            System.out.println("Use \"equip\" to view the player's equipment");
            System.out.println("Use \"equip [name]\" to equip item if available");
            System.out.println("  (Put items containing spaces in quation marks)");
        } else
        {
            Item change = Inventory.getItemStartingWith(game.player_.inventory_, params_[1]);

            if (change != null)
            {
                if (change.getClass().getGenericSuperclass() == Weapon.class)
                {
                    System.out.println(game.player_.name_ + " swapped out " +
                            CP.getColorString(game.player_.equip_.weapon_.name_, CP.RED)
                            + " for " + CP.getColorString(change.name_, CP.GREEN));

                    game.player_.removeItemFromInventory(change);
                    game.player_.addItemToInventory(game.player_.equip_.weapon_);
                    game.player_.equip_.weapon_ = (Weapon) change;
                    enemy_turn_ = true;
                    return;
                } else if (change.getClass().getGenericSuperclass() == Armor.class)
                {
                    System.out.println(game.player_.name_ + " swapped out " +
                            CP.getColorString(game.player_.equip_.armor_.name_, CP.RED)
                            + " for " + CP.getColorString(change.name_, CP.GREEN));

                    game.player_.removeItemFromInventory(change);
                    game.player_.addItemToInventory(game.player_.equip_.armor_);
                    game.player_.equip_.armor_ = (Armor) change;
                    enemy_turn_ = true;
                    return;
                }
            }

            CP.println(params_[1] + " is not in Inventory or not an equipable Item!", CP.RED);
        }
    }

    private static void infoItem(Game game)
    {
        if (params_.length == 1)
        {
            Inventory.listAll(game.player_.inventory_);
        } else if (params_.length > 2)
        {
            System.out.println("Use \"info/i [prefix]\" to view detailed item info of first item starting with prefix");
            System.out.println("  (Put items containing spaces in quation marks)");
        } else if (!game.player_.inventory_.isEmpty())
        {
            for (Map.Entry<Item, Integer> entry : game.player_.inventory_.entrySet())
            {
                if (entry.getKey().name_.toLowerCase().startsWith(params_[1]))
                {
                    System.out.print(entry.getKey());
                    return;
                }
            }

            CP.println(params_[1] + " not in Inventory!", CP.RED);
        } else
        {
            CP.println(params_[1] + " not in Inventory!", CP.RED);
        }
    }

    private static void infoMonster(Game game)
    {
        if (params_.length == 1)
        {
            game.mb_.listAll();
        } else if (params_.length > 3)
        {
            System.out.println("Use \"monsterinfo/mi [prefix] [level]\" to view detailed monster info");
            System.out.println("of first monster starting with prefix, with optional level");
            System.out.println("  (Put monster names containing spaces in quation marks)");
        } else if (params_.length == 2)
        {
            game.mb_.getMonsterInfoStartingWith(params_[1]);
        }
        else
        {
            game.mb_.getMonsterInfoStartingWith(params_[1], params_[2]);
        }
    }

    private static void tome(Game game)
    {
        if(sanityCheck(game.player_.tome_.isEmpty()))
            return;

        if (params_.length == 1)
        {
            System.out.println("   - Tome -");
            for (Map.Entry<String, String> entry : game.player_.tome_.entrySet())
            {
                for(int i = 7 - entry.getKey().length(); i > 0; i--)
                    System.out.print(" ");
                System.out.println(entry.getKey() + "- " + entry.getValue());
            }

            if(! game.player_.known_spells_.isEmpty())
            {
                System.out.println();
                System.out.println("   - Spells -");
                for(Spell s : game.player_.known_spells_)
                    System.out.println(s);
            }

        } else if (params_.length > 3)
        {
            System.out.println("Use \"monsterinfo/mi [prefix notice]\" to view detailed monster info");
            System.out.println("of first monster starting with prefix, with optional level");
            System.out.println("  (Put monster names containing spaces in quation marks)");
        } else if (params_.length == 2)
        {
            System.out.println("   - Tome -");
            for (Map.Entry<String, String> entry : game.player_.tome_.entrySet())
            {
                if (entry.getKey().toLowerCase().startsWith(params_[1]))
                {
                    for(int i = 7 - entry.getKey().length(); i > 0; i--)
                        System.out.print(" ");
                    System.out.println(entry.getKey() + "- " + entry.getValue());
                    game.player_.listSpellsWith(entry.getKey());
                    return;
                }
            }
            System.out.println("Nothing liste for: " + params_[1]);
        }
        else
        {
            for (Map.Entry<String, String> entry : game.player_.tome_.entrySet())
            {
                if (entry.getKey().toLowerCase().startsWith(params_[1]))
                {
                    if(params_[2].length() > 40)
                    {
                        CP.println("Line too long, please keep it to 40 characters!",CP.RED);
                        return;
                    }
                    Casting.putToTome(entry.getKey(),params_[2]);

                    for(int i = 7 - entry.getKey().length(); i > 0; i--)
                        System.out.print(" ");
                    System.out.println(entry.getKey() + "- " + entry.getValue());
                    return;
                }
            }
            System.out.println("Nothing liste for: " + params_[1]);
        }
    }

    private static void useItem(Game game)
    {
        if (params_.length == 1 || params_.length > 2)
        {
            System.out.println("Use \"use/u [prefix]\" to use the first item starting with prefix");
            System.out.println("  (Put items containing spaces in quation marks)");
        } else if (!game.player_.inventory_.isEmpty())
        {
            for (Map.Entry<Item, Integer> entry : game.player_.inventory_.entrySet())
            {
                if (entry.getKey().name_.toLowerCase().startsWith(params_[1]))
                {
                    if (entry.getKey() instanceof UseableItem)
                    {
                        System.out.print("Using: " + entry.getKey().name_ + " - ");
                        ((UseableItem) entry.getKey()).effect(game.player_);

                        entry.setValue(entry.getValue() - 1);
                        if (entry.getValue() == 0)
                        {
                            game.player_.inventory_.remove(entry.getKey());
                        }
                        enemy_turn_ = true;
                    } else
                    {
                        CP.println(params_[1] + " cannot be used!", CP.RED);
                    }
                    return;
                }
            }

            CP.println(params_[1] + " not in Inventory!", CP.RED);
        } else
        {
            CP.println(params_[1] + " not in Inventory!", CP.RED);
        }
    }

    private static void printStatus(Game game)
    {
        System.out.print(game.player_);
    }

    private static void printHiddenStatus(Game game)
    {
        if(sanityCheck(!debug))
            return;

        game.player_.printHiddenInfo();
    }

    private static void printHiddenEquipInfo(Game game)
    {
        if(sanityCheck(!debug))
            return;

        System.out.print("Attack: " + game.player_.equip_.weapon_.calcAttack(game.player_.stats_));
        if (game.player_.equip_.weapon_.attack_twice_)
            System.out.println("x2");
        else
            System.out.println();
        System.out.println("Accuracy: " + game.player_.equip_.weapon_.calcAccuracy(game.player_.stats_));
        System.out.println("Critical: " + game.player_.equip_.weapon_.calcCritical(game.player_.stats_));

        System.out.println("Defense: " + game.player_.equip_.armor_.calcDefense(game.player_.stats_));
        System.out.println("Resistance: " + game.player_.equip_.armor_.calcResistance(game.player_.stats_));
        System.out.println("Evasion: " + game.player_.equip_.armor_.calcEvasion(game.player_.stats_));
        System.out.println("Scaling: " + (1 + game.player_.equip_.weapon_.getScaling(game.player_.stats_) / 10));
    }

    private static void learnSpells(Game game)
    {
        if(sanityCheck(!debug))
            return;

        Casting.putToTome("Eldur ", "");
        Casting.putToTome("Nen ", "");
        Casting.putToTome("Zrak ", "");
        Casting.putToTome("Aska ", "");
        Casting.putToTome("Calina ", "");
        Casting.putToTome("Myrkur ", "");
        Casting.putToTome("Liv ", "");
        Casting.putToTome("Hlava ", "");
        Casting.putToTome("Yad ", "");
        Casting.putToTome("Volat ", "");
        Casting.putToTome("Sai ", "");
        Casting.putToTome("Obt ", "");
        Casting.putToTome("Gael ", "");
        Casting.putToTome("Jiehu ", "");
        Casting.putToTome("Port ", "");
        Casting.putToTome("Dom ", "");
        Casting.putToTome("Tiet ", "");
        Casting.putToTome("Kuv ", "");
        Casting.putToTome("Rus ", "");
    }

}
