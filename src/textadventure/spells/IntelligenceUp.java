package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class IntelligenceUp extends Spell
{

    public IntelligenceUp()
    {
        super("Gael Dom Myrkur ",
                "Intelligence Up",
                "Increase your intelligence for 3 turns",
                10);
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        seme.buff("Int");
    }
}
