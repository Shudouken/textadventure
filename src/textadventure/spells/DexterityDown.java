package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class DexterityDown extends Spell
{

    public DexterityDown()
    {
        super("Jiehu Tiet Aska ",
                "Dexterity Down",
                "Decrease an enemy's dexterity for 3 turns",
                10);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.debuff("Dex");
    }
}
