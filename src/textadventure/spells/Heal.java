package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class Heal extends Spell
{

    public Heal()
    {
        super("Obt Liv ",
                "Heal",
                "Heals your HP by a small amount",
                3);
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        seme.heal(15);
    }
}
