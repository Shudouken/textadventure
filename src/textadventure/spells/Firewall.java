package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class Firewall extends Spell
{

    public Firewall()
    {
        super("Port Rus Eldur ",
                "Firewall",
                "Traps the enemy in a wall of fire",
                20);
        target_ = true;
        aoe_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.setAOE(this);
    }

    @Override
    public void aoeDamage(Player seme)
    {
        seme.takeAOEDamage((int) (20 / seme.equip_.armor_.getFireRes(seme.stats_)));
    }
}
