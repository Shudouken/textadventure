package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class DexterityUp extends Spell
{

    public DexterityUp()
    {
        super("Gael Dom Aska ",
                "Dexterity Up",
                "Increase your dexterity for 3 turns",
                10);
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        seme.buff("Dex");
    }
}
