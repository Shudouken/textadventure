package textadventure.spells;

import textadventure.Command;
import textadventure.Game;
import textadventure.Player;
import textadventure.Spell;
import textadventure.tiles.ResetTile;

public class Warp extends Spell
{

    public Warp()
    {
        super("Port Dom ",
                "Warp",
                "Teleport back to town, resetting the map",
                3);
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        if (Game.getInstance().mode_ == Command.Mode.MAP)
        {
            System.out.println("Warping back to Town! The holy stone shimmers..");
            System.out.println("The outside world has been reset!");
            ResetTile.resetMaps();
        } else
        {
            System.out.println("Can't use this in battle!");
        }
    }
}
