package textadventure.spells;

import textadventure.Enemy;
import textadventure.Game;
import textadventure.Player;
import textadventure.Spell;

public class Inspect extends Spell
{

    public Inspect()
    {
        super("Jiehu Hlava Tiet ",
                "Inspect",
                "Analyze an enemies status",
                2);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        System.out.println(uke);

        if(uke instanceof Enemy)
            Game.getInstance().mb_.add((Enemy)uke);
    }
}
