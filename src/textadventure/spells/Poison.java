package textadventure.spells;

import textadventure.Player;
import textadventure.Randomizer;
import textadventure.Spell;

public class Poison extends Spell
{

    public Poison()
    {
        super("Gael Tiet Yad ",
                "Poison",
                "Has a chance to poison the enemy",
                8);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        if(! uke.poisoned_ && Randomizer.randInt(0,1) == 1)
        {
            System.out.println(uke.name_ + " was poisoned!");
            uke.poisoned_ = true;
        }
        else
            System.out.println("Spell failed!");
    }
}
