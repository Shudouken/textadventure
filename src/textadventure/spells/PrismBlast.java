package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class PrismBlast extends Spell
{

    public PrismBlast()
    {
        super("Volat Kuv Calina ",
                "Prism Blast",
                "Focus light through a prism",
                15);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.takeSpellDamage(seme, (int) (40 / uke.equip_.armor_.getLightRes(uke.stats_)));
    }
}
