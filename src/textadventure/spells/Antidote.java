package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class Antidote extends Spell
{

    public Antidote()
    {
        super("Sai Yad Dom ",
                "Antidote",
                "Cures Poison",
                12);
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        if (seme.poisoned_)
        {
            System.out.println(seme.name_ + " got cured of poison!");
            seme.poisoned_ = false;
        }
        else
            System.out.println("Spell failed!");
    }
}
