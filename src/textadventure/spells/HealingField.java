package textadventure.spells;

import textadventure.Command;
import textadventure.Game;
import textadventure.Player;
import textadventure.Spell;

public class HealingField extends Spell
{

    public HealingField()
    {
        super("Port Rus Obt Liv ",
                "Healing Field",
                "Surround yourself with a healing field",
                30);
        aoe_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        if (Game.getInstance().mode_ == Command.Mode.BATTLE)
            seme.setAOE(this);
        else
            seme.heal(20 + 10 + 6);
    }

    @Override
    public void aoeDamage(Player seme)
    {
        seme.takeAOEHeal(20);
    }
}
