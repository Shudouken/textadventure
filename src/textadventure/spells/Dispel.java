package textadventure.spells;

import textadventure.Buff;
import textadventure.Player;
import textadventure.Spell;
import textadventure.StatusEffects;

public class Dispel extends Spell
{

    public Dispel()
    {
        super("Sai Hlava Dom ",
                "Dispel",
                "Negate all magical effects",
                22);
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        System.out.println(seme.name_ + " was dispelled!");
        StatusEffects.reset(seme);
        if(seme.stats_ instanceof Buff)
            seme.stats_ = ((Buff) seme.stats_).removeAllBuffs();
    }
}
