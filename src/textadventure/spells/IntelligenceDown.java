package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class IntelligenceDown extends Spell
{

    public IntelligenceDown()
    {
        super("Jiehu Tiet Myrkur ",
                "Intelligence Down",
                "Decrease an enemy's intelligence for 3 turns",
                10);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.debuff("Int");
    }
}
