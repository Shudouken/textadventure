package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class Flare extends Spell
{

    public Flare()
    {
        super("Volat Eldur Port Myrkur ",
                "Flare",
                "Summons a fiery dark flame",
                30);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.takeSpellDamage(seme, (int) (66 / (uke.equip_.armor_.getFireRes(uke.stats_) * uke.equip_.armor_.getDarkRes(uke.stats_))));
    }
}
