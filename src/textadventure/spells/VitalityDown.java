package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class VitalityDown extends Spell
{

    public VitalityDown()
    {
        super("Jiehu Tiet Nen ",
                "Vitality Down",
                "Decrease an enemy's vitality for 3 turns",
                10);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.debuff("Vit");
    }
}
