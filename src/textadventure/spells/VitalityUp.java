package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class VitalityUp extends Spell
{

    public VitalityUp()
    {
        super("Gael Dom Nen ",
                "Vitality Up",
                "Increase your vitality for 3 turns",
                10);
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        seme.buff("Vit");
    }
}
