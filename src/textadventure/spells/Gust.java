package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class Gust extends Spell
{

    public Gust()
    {
        super("Volat Kuv Zrak ",
                "Gust",
                "Causes a fierce storm",
                15);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.takeSpellDamage(seme, (int) (40 / uke.equip_.armor_.getWindRes(uke.stats_)));
    }
}
