package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class StrengthDown extends Spell
{

    public StrengthDown()
    {
        super("Jiehu Tiet Eldur ",
                "Strength Down",
                "Decrease an enemy's strength for 3 turns",
                10);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.debuff("Str");
    }
}
