package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class AgilityDown extends Spell
{

    public AgilityDown()
    {
        super("Jiehu Tiet Zrak ",
                "Agility Down",
                "Decrease an enemy's agility for 3 turns",
                10);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.debuff("Agi");
    }
}
