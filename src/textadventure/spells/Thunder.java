package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class Thunder extends Spell
{

    public Thunder()
    {
        super("Port Rus Aska ",
                "Thunder",
                "Traps the enemy in thundery weather",
                20);
        target_ = true;
        aoe_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.setAOE(this);
    }

    @Override
    public void aoeDamage(Player seme)
    {
        seme.takeAOEDamage((int) (20 / seme.equip_.armor_.getThunderRes(seme.stats_)));
    }
}
