package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class AgilityUp extends Spell
{

    public AgilityUp()
    {
        super("Gael Dom Zrak ",
                "Agility Up",
                "Increase your agility for 3 turns",
                10);
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        seme.buff("Agi");
    }
}
