package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class Lightning extends Spell
{

    public Lightning()
    {
        super("Volat Aska ",
                "Lightning",
                "Summons a lightning strike",
                5);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.takeSpellDamage(seme, (int) (20 / uke.equip_.armor_.getThunderRes(uke.stats_)));
    }
}
