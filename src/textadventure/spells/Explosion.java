package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class Explosion extends Spell
{

    public Explosion()
    {
        super("Volat Kuv Eldur ",
                "Explosion",
                "Causes a fiery explosion",
                15);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.takeSpellDamage(seme, (int) (40 / uke.equip_.armor_.getFireRes(uke.stats_)));
    }
}
