package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class FullHeal extends Spell
{

    public FullHeal()
    {
        super("Sai Myrkur Obt Kuv Liv ",
                "Full Heal",
                "Fully restores your HP",
                64);
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        seme.heal(seme.stats_.max_hp_);
    }
}
