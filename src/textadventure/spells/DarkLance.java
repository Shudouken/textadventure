package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class DarkLance extends Spell
{

    public DarkLance()
    {
        super("Volat Myrkur ",
                "Dark Lance",
                "Pierce the foe with a dark lance",
                5);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.takeSpellDamage(seme, (int) (20 / uke.equip_.armor_.getDarkRes(uke.stats_)));
    }
}
