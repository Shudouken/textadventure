package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class Ray extends Spell
{

    public Ray()
    {
        super("Volat Calina ",
                "Ray",
                "Summons a ray of light",
                5);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.takeSpellDamage(seme, (int) (20 / uke.equip_.armor_.getLightRes(uke.stats_)));
    }
}
