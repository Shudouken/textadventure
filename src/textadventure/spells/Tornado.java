package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class Tornado extends Spell
{

    public Tornado()
    {
        super("Volat Zrak ",
                "Tornado",
                "Summons a tornado",
                5);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.takeSpellDamage(seme, (int) (20 / uke.equip_.armor_.getWindRes(uke.stats_)));
    }
}
