package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class GravitySphere extends Spell
{

    public GravitySphere()
    {
        super("Volat Kuv Myrkur ",
                "Gravity Sphere",
                "Causes a dark gravitational vortex",
                15);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.takeSpellDamage(seme, (int) (40 / uke.equip_.armor_.getDarkRes(uke.stats_)));
    }
}
