package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class Waterfall extends Spell
{

    public Waterfall()
    {
        super("Port Rus Nen ",
                "Waterfall",
                "Traps the enemy in a current of water",
                20);
        target_ = true;
        aoe_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.setAOE(this);
    }

    @Override
    public void aoeDamage(Player seme)
    {
        seme.takeAOEDamage((int) (20 / seme.equip_.armor_.getWaterRes(seme.stats_)));
    }
}
