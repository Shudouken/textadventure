package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class ResistanceDown extends Spell
{

    public ResistanceDown()
    {
        super("Jiehu Tiet Calina ",
                "Resistance Down",
                "Decrease an enemy's resistance for 3 turns",
                10);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.debuff("Res");
    }
}
