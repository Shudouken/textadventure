package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class Cascade extends Spell
{

    public Cascade()
    {
        super("Volat Kuv Nen ",
                "Cascade",
                "Attack with intense water pressure",
                15);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.takeSpellDamage(seme, (int) (40 / uke.equip_.armor_.getWaterRes(uke.stats_)));
    }
}
