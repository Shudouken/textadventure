package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class Splash extends Spell
{

    public Splash()
    {
        super("Volat Nen ",
                "Splash",
                "Englufs the enemy in splashing water",
                5);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.takeSpellDamage(seme, (int) (20 / uke.equip_.armor_.getWaterRes(uke.stats_)));
    }
}
