package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class NegativeGate extends Spell
{

    public NegativeGate()
    {
        super("Port Rus Myrkur ",
                "Negative Gate",
                "Traps the enemy in a field of negativity",
                20);
        target_ = true;
        aoe_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.setAOE(this);
    }

    @Override
    public void aoeDamage(Player seme)
    {
        seme.takeAOEDamage((int) (20 / seme.equip_.armor_.getDarkRes(seme.stats_)));
    }
}
