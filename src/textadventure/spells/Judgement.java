package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class Judgement extends Spell
{

    public Judgement()
    {
        super("Volat Aska Port Calina ",
                "Judgement",
                "Brings forth a lightning strike of inquisition",
                30);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.takeSpellDamage(seme, (int) (66 / (uke.equip_.armor_.getThunderRes(uke.stats_) * uke.equip_.armor_.getLightRes(uke.stats_))));
    }
}
