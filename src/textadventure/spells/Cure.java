package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class Cure extends Spell
{

    public Cure()
    {
        super("Obt Kuv Liv ",
                "Cure",
                "Heals your HP by a large amount",
                9);
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        seme.heal(30);
    }
}
