package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class Spark extends Spell
{

    public Spark()
    {
        super("Volat Kuv Aska ",
                "Spark",
                "Sends a spark through an enemy's body",
                15);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.takeSpellDamage(seme, (int) (40 / uke.equip_.armor_.getThunderRes(uke.stats_)));
    }
}
