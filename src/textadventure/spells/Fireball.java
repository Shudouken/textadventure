package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class Fireball extends Spell
{

    public Fireball()
    {
        super("Volat Eldur ",
                "Fireball",
                "Summons a fireball",
                5);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.takeSpellDamage(seme, (int) (20 / uke.equip_.armor_.getFireRes(uke.stats_)));
    }
}
