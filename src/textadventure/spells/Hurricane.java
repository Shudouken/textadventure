package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class Hurricane extends Spell
{

    public Hurricane()
    {
        super("Port Rus Zrak ",
                "Hurricane",
                "Traps the enemy in a hurricane",
                20);
        target_ = true;
        aoe_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.setAOE(this);
    }

    @Override
    public void aoeDamage(Player seme)
    {
        seme.takeAOEDamage((int) (20 / seme.equip_.armor_.getWindRes(seme.stats_)));
    }
}
