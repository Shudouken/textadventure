package textadventure.spells;

import textadventure.Buff;
import textadventure.Player;
import textadventure.Spell;
import textadventure.StatusEffects;

public class Revert extends Spell
{

    public Revert()
    {
        super("Sai Hlava Tiet ",
                "Revert",
                "Negate all magical effects on a target",
                22);
        target_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        System.out.println(uke.name_ + "'s status was reverted!");
        StatusEffects.reset(uke);
        if(uke.stats_ instanceof Buff)
            uke.stats_ = ((Buff) uke.stats_).removeAllBuffs();
    }
}
