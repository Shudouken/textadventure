package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class StrengthUp extends Spell
{

    public StrengthUp()
    {
        super("Gael Dom Eldur ",
                "Strength Up",
                "Increase your strength for 3 turns",
                10);
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        seme.buff("Str");
    }
}
