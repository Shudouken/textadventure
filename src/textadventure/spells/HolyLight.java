package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class HolyLight extends Spell
{

    public HolyLight()
    {
        super("Port Rus Calina ",
                "Holy Light",
                "Traps the enemy in a cage of light",
                20);
        target_ = true;
        aoe_ = true;
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        uke.setAOE(this);
    }

    @Override
    public void aoeDamage(Player seme)
    {
        seme.takeAOEDamage((int) (20 / seme.equip_.armor_.getLightRes(seme.stats_)));
    }
}
