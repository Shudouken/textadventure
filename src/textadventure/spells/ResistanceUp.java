package textadventure.spells;

import textadventure.Player;
import textadventure.Spell;

public class ResistanceUp extends Spell
{

    public ResistanceUp()
    {
        super("Gael Dom Calina ",
                "Resistance Up",
                "Increase your resistance for 3 turns",
                10);
    }

    @Override
    public void execute(Player seme, Player uke)
    {
        seme.buff("Res");
    }
}
