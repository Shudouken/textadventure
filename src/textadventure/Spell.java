package textadventure;

import textadventure.spells.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public abstract class Spell implements Serializable
{
    public String formula_;
    public String name_;
    public String desc_;
    public int cost_;
    public boolean target_ = false;
    public boolean aoe_ = false;

    public static final Spell fireball_ = new Fireball();
    public static final Spell explosion_ = new Explosion();
    public static final Spell firewall_ = new Firewall();
    public static final Spell strength_up_ = new StrengthUp();
    public static final Spell strength_down_ = new StrengthDown();

    public static final Spell splash_ = new Splash();
    public static final Spell cascade_ = new Cascade();
    public static final Spell waterfall_ = new Waterfall();
    public static final Spell vitality_up_ = new VitalityUp();
    public static final Spell vitality_down_ = new VitalityDown();

    public static final Spell tornado_ = new Tornado();
    public static final Spell gust_ = new Gust();
    public static final Spell hurricane_ = new Hurricane();
    public static final Spell agility_up_ = new AgilityUp();
    public static final Spell agility_down_ = new AgilityDown();

    public static final Spell lightning_ = new Lightning();
    public static final Spell spark_ = new Spark();
    public static final Spell thunder_ = new Thunder();
    public static final Spell dexterity_up_ = new DexterityUp();
    public static final Spell dexterity_down_ = new DexterityDown();

    public static final Spell ray_ = new Ray();
    public static final Spell prism_blast_ = new PrismBlast();
    public static final Spell holy_light_ = new HolyLight();
    public static final Spell resistance_up_ = new ResistanceUp();
    public static final Spell resistance_down_ = new ResistanceDown();

    public static final Spell dark_lance_ = new DarkLance();
    public static final Spell gravity_sphere_ = new GravitySphere();
    public static final Spell negative_gate_ = new NegativeGate();
    public static final Spell intelligence_up_ = new IntelligenceUp();
    public static final Spell intelligence_down_ = new IntelligenceDown();

    public static final Spell heal_ = new Heal();
    public static final Spell cure_ = new Cure();
    public static final Spell healing_field_ = new HealingField();
    public static final Spell full_heal_ = new FullHeal();

    public static final Spell antidote_ = new Antidote();
    public static final Spell poison_ = new Poison();
    public static final Spell dispel_ = new Dispel();
    public static final Spell revert_ = new Revert();
    public static final Spell inspect_ = new Inspect();
    public static final Spell warp_ = new Warp();


    public static final Spell flare_ = new Flare();
    public static final Spell judgement_ = new Judgement();

    public static final ArrayList<Spell> spells_;
    public static final Map<String, String> spell_translation_;
    public static final Map<String, Spell> spell_action_;

    static
    {
        spells_ = new ArrayList<>();

        spells_.add(fireball_);
        spells_.add(explosion_);
        spells_.add(firewall_);
        spells_.add(strength_up_);
        spells_.add(strength_down_);

        spells_.add(splash_);
        spells_.add(cascade_);
        spells_.add(waterfall_);
        spells_.add(vitality_up_);
        spells_.add(vitality_down_);

        spells_.add(tornado_);
        spells_.add(gust_);
        spells_.add(hurricane_);
        spells_.add(agility_up_);
        spells_.add(agility_down_);

        spells_.add(lightning_);
        spells_.add(spark_);
        spells_.add(thunder_);
        spells_.add(dexterity_up_);
        spells_.add(dexterity_down_);

        spells_.add(ray_);
        spells_.add(prism_blast_);
        spells_.add(holy_light_);
        spells_.add(resistance_up_);
        spells_.add(resistance_down_);

        spells_.add(dark_lance_);
        spells_.add(gravity_sphere_);
        spells_.add(negative_gate_);
        spells_.add(intelligence_up_);
        spells_.add(intelligence_down_);

        spells_.add(heal_);
        spells_.add(cure_);
        spells_.add(healing_field_);
        spells_.add(full_heal_);

        spells_.add(antidote_);
        spells_.add(poison_);
        spells_.add(dispel_);
        spells_.add(revert_);
        spells_.add(inspect_);
        spells_.add(warp_);

        spells_.add(flare_);
        spells_.add(judgement_);

        spell_translation_ = new HashMap<>();
        spell_action_ = new HashMap<>();

        for(Spell s : spells_)
        {
            spell_translation_.put(s.formula_, s.name_);
            spell_action_.put(s.name_, s);
        }

    }


    public Spell(String formula, String name, String desc, int cost)
    {
        formula_ = formula;
        name_ = name;
        desc_ = desc;
        cost_ = cost;
    }

    public static boolean spellCost(Player caster, int mp)
    {
        boolean cost_met = false;

        if (caster.stats_.mp_ >= mp)
        {
            cost_met = true;
            caster.stats_.loseMP(mp);
        } else
        {
            System.out.println(caster.name_ + " has run out of mp!");
        }

        return cost_met;
    }

    public boolean target(Player seme, Player uke)
    {
        if (uke == null)
        {
            System.out.println("No target for " + name_ + "!");
            return false;
        }

        if(StatusEffects.checkCharm(seme,uke))
            return false;

        return true;
    }

    public void cast(Player seme, Player uke)
    {
        if(target_ && ! target(seme,uke))
        {
            return;
        }

        if(spellCost(seme,cost_))
            execute(seme,uke);
    }

    public abstract void execute(Player seme, Player uke);

    public void aoeDamage(Player seme)
    {

    }

    @Override
    public String toString()
    {
        String temp = "";

        String aoe = aoe_ ? " " + CP.getColorString("AOE", CP.PURPLE) + " " : "";

        temp += formula_ + ": " + name_ + aoe + " (" + CP.getColorString(""+cost_, CP.BLUE) + " MP) - " + desc_;

        return temp;
    }

    @Override
    public int hashCode()
    {
        return name_.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof Spell))
            return false;
        if (obj == this)
            return true;
        if (((Spell) obj).name_.equals(name_))
            return true;

        return false;
    }
}
