package textadventure;

import jline.ConsoleReader;
import textadventure.tiles.BankTile;
import textadventure.tiles.GateTile;
import textadventure.tiles.PlayerTile;
import textadventure.tiles.ResetTile;

import java.io.IOException;

public class Game implements java.io.Serializable
{

    public static transient String title_ = "TextAdventure";
    public Player player_;
    public Enemy enemy_;
    public Location map_;
    public Location town_;
    public PlayerTile pt_;
    public PlayerTile town_pt_;
    public BankTile bt_;
    public GateTile town_gate_;
    public int depth_ = 0;
    public Command.Mode mode_;
    public static ConsoleReader reader;
    public MonsterBook mb_;
    private static Game instance;
    public static String prompt_;

    public static Game getInstance()
    {
        if (Game.instance == null)
        {
            Game.instance = new Game();
        }
        return Game.instance;
    }

    public static void setInstance(Game game)
    {
        Game.instance = game;
    }

    private Game()
    {
        try
        {
            reader = new ConsoleReader();
            reader.clearScreen();
        } catch (IOException ex)
        {
        }
    }

    public void newGame()
    {
        player_ = new Player();
        player_.characterCreation();
        mb_ = new MonsterBook();
        Location.setStartTown();
    }

    public void run()
    {
        String input = "";
        mode_ = Command.Mode.MAP;

        while (true)
        {
            prompt_ = "";
            String hp = CP.getColorString("" + player_.stats_.hp_, CP.RED);
            String mp = CP.getColorString("" + player_.stats_.mp_, CP.BLUE);

            if (mode_ == Command.Mode.MAP)
                prompt_ += "map(" + hp + "/" + mp + ")> ";
            else if (mode_ == Command.Mode.BATTLE)
            {
                prompt_ += "battle(" + hp + "/" + mp + ")> ";
            }

            try
            {
                reader.flushConsole();
                System.out.print("\r");
                input = reader.readLine(prompt_);
            } catch (Exception e)
            {
            }

            if (input.toLowerCase().equals("quit") || input.toLowerCase().equals("q"))
            {
                System.out.println("May your soul find peace!");
                break;
            } else if (input.length() > 0)
            {
                Command.handleCommand(input.toLowerCase(), this, mode_);
                System.out.println();
            }

            if (player_ != null && player_.stats_.hp_ <= 0)
            {
                CP.println(player_.name_ + " perished!", CP.RED);
                System.out.println("Made it up to depth " + CP.getColorString("" + depth_, CP.BOLD));

                player_.shekels_ = player_.shekels_ / 2;
                player_.exp_ = player_.exp_ / 2;

                System.out.println(player_.name_ + "'s soul wanders back to town, losing half of");
                System.out.println("shekels(" + player_.shekels_ + ") and experience(" + player_.exp_ + ") in the process.");
                System.out.println();

                if (player_.stats_ instanceof Buff) player_.stats_ = ((Buff) player_.stats_).original_;
                player_.stats_.hp_ = player_.stats_.max_hp_;
                player_.stats_.mp_ = player_.stats_.max_mp_;
                StatusEffects.reset(player_);

                enemy_.et_ = null;
                enemy_ = null;
                player_.wait_turn_ = false;
                mode_ = Command.Mode.MAP;

                ResetTile.resetMaps();
            }

            if (enemy_ != null && enemy_.stats_.hp_ <= 0)
            {
                CP.println(enemy_.name_ + " perished!", CP.BOLD);
                player_.getSpoils(enemy_);
                player_.wait_turn_ = false;
                StatusEffects.reset(player_);
                player_.aoe_ = null;
                map_.removeTile(enemy_.et_.pos_);
                map_.enemies_.remove(enemy_.et_);
                enemy_.et_ = null;
                enemy_ = null;
                mode_ = Command.Mode.MAP;
            }

        }
    }

}
