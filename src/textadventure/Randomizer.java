package textadventure;

import java.util.Random;

public class Randomizer
{

    private static final Random rand = new Random();

    public static int randInt(int min, int max)
    {
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

}
