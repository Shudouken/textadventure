package textadventure;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Casting
{

    public static final Map<Integer, String> lit_;

    static
    {
        lit_ = new HashMap<>();

        lit_.put(101, "Eldur ");
        lit_.put(110, "Nen ");
        lit_.put(122, "Zrak ");
        lit_.put(97, "Aska ");
        lit_.put(99, "Calina ");
        lit_.put(109, "Myrkur ");
        lit_.put(108, "Liv ");
        lit_.put(104, "Hlava ");
        lit_.put(121, "Yad ");
        lit_.put(118, "Volat ");
        lit_.put(115, "Sai ");
        lit_.put(111, "Obt ");
        lit_.put(103, "Gael ");
        lit_.put(106, "Jiehu ");
        lit_.put(112, "Port ");
        lit_.put(100, "Dom ");
        lit_.put(116, "Tiet ");
        lit_.put(107, "Kuv ");
        lit_.put(114, "Rus ");
    }

    public static void casting(Game game)
    {
        System.out.print("Cast-");
        Map<String,String> t = Game.getInstance().player_.tome_;

        int k = 0;
        Stack<Integer> prev = new Stack<>();
        String spell = "";
        while (k != 10)
        {
            try
            {
                k = game.reader.readVirtualKey();
            } catch (IOException ex)
            {
            }

            //System.out.println(k);

            if (lit_.containsKey(k) && t.containsKey(lit_.get(k)))
            {
                System.out.print(lit_.get(k));
                spell += lit_.get(k);
                prev.add(k);
            } else if (k == 8 && (!prev.isEmpty()) && lit_.containsKey(prev.lastElement()))
            {
                System.out.print("\r");
                for (int i = 0; i < ("Cast-" + spell).length(); i++)
                {
                    System.out.print(" ");
                }

                spell = (String) spell.subSequence(0, spell.length() - lit_.get(prev.lastElement()).length());
                prev.pop();
                System.out.print("\rCast-" + spell);
            }
        }

        matchSpell(game, spell);
    }

    private static void matchSpell(Game game, String spell)
    {
        System.out.println();

        if (Spell.spell_translation_.containsKey(spell))
        {
            String spell_name = Spell.spell_translation_.get(spell);
            System.out.println("(" + spell_name + ")");
            System.out.println();
            if (Spell.spell_action_.containsKey(spell_name))
            {
                Command.last_spell_ = Spell.spell_action_.get(spell_name);
                Command.last_spell_.cast(game.player_, game.enemy_);

                if(! game.player_.known_spells_.contains(Command.last_spell_))
                    game.player_.known_spells_.add(Command.last_spell_);
            }

        } else
            CP.println("Unknown Spell!", CP.RED);
    }

    private static void playSpellSound(String spell)
    {

        Sound.playWAV(spell.toLowerCase() + ".wav");
    }

    public static void putToTome(String lit, String desc)
    {
        Map<String,String> t = Game.getInstance().player_.tome_;

        if(t.containsKey(lit))
            t.replace(lit, desc);
        else
            t.put(lit, desc);
    }

}
