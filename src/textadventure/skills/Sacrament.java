package textadventure.skills;

import textadventure.CP;
import textadventure.Game;
import textadventure.Player;
import textadventure.Skill;

public class Sacrament extends Skill
{

    public Sacrament()
    {
        super("Sacrament", 0, 4);
        offensive_ = false;
    }

    @Override
    public String getDescription()
    {
        return "A holy ritual which feeds on 15% of your max HP to restore 30% of your max MP.";
    }

    @Override
    public int getCost()
    {
        return 0;
    }

    @Override
    public void effect(Game game, Player seme, Player uke)
    {
        int hp_loss = seme.stats_.max_hp_ * 15 / 100;
        int mp_gain = (seme.stats_.max_mp_ * 30 / 100);

        hp_loss /= 1 + skilllevel_ / 2;
        mp_gain *= 1 + skilllevel_ / 2;

        seme.stats_.loseHP(hp_loss);
        seme.stats_.gainMP(mp_gain);

        System.out.println(seme.name_ + " fed on " + CP.getColorString("" + hp_loss, CP.RED)
                + " HP to restore " + CP.getColorString("" + mp_gain, CP.BLUE) + " MP!");
    }

}
