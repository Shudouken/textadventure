package textadventure.skills;

import textadventure.Buff;
import textadventure.Game;
import textadventure.Player;
import textadventure.Skill;

public class Blessing extends Skill
{

    public Blessing()
    {
        super("Blessing", 10, 10);
        offensive_ = false;
    }

    @Override
    public String getDescription()
    {
        return "Call upon the gods to temporarily push your body to its limits.";
    }

    @Override
    public int getCost()
    {
        return (basecost_ + (1 * skilllevel_));
    }

    @Override
    public void effect(Game game, Player seme, Player uke)
    {

        seme.heal(30);

        if (seme.stats_ instanceof Buff)
        {
            System.out.println("Only a pure soul may recieve the god's blessings!");
            return;
        }

        seme.buff("Str");
        ((Buff) seme.stats_).turns_active_ = 3 + skilllevel_;

        seme.buff("Int");
        ((Buff) seme.stats_).turns_active_ = 3 + skilllevel_;

        seme.buff("Dex");
        ((Buff) seme.stats_).turns_active_ = 3 + skilllevel_;

        System.out.println(seme.name_ + " was blessed by the gods!");

    }

}
