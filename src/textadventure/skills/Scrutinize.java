package textadventure.skills;

import textadventure.CP;
import textadventure.Enemy;
import textadventure.Game;
import textadventure.Player;
import textadventure.Randomizer;
import textadventure.Skill;

public class Scrutinize extends Skill
{

    public Scrutinize()
    {
        super("Scrutinize", 30, 10);
    }

    @Override
    public String getDescription()
    {
        return "Closely watch an enemy in order to commit their special traits to memory.";
    }

    @Override
    public int getCost()
    {
        return (int) (basecost_ - (20 * skilllevel_ / 10.0f));
    }

    @Override
    public void effect(Game game, Player seme, Player uke)
    {

        if (uke == null)
            System.out.println("No target for skill: " + name_);
        else if (skillCost(seme, getCost()))
        {
            int success = (int) ((seme.stats_.getDex() + 15) * (skilllevel_ / 10.0f));

            if (uke instanceof Enemy)
            {
                Skill skill = ((Enemy) uke).skill_;

                if (skill != null && !skill.passive_)
                {
                    if (seme.skills_.containsValue(skill))
                    {
                        System.out.println(seme.name_ + " has already scrutinized " + skill.name_ + "!");
                        return;
                    }

                    if (Randomizer.randInt(0, 100) <= success)
                    {
                        seme.skills_.put(skill.name_, skill);
                        System.out.println(seme.name_ + " scrutinized " + skill.name_ + "!");
                        checkPerfection(seme);
                        return;
                    }
                }
            }
            System.out.println(name_ + " failed!");
        }
    }

    private void checkPerfection(Player seme)
    {
        if (!seme.skills_.containsValue(perfection_))
        {
            if (seme.skills_.containsValue(steal_) && seme.skills_.containsValue(shenanigan_)
                    && seme.skills_.containsValue(charm_))
            {
                CP.println(seme.name_ + " has reached perfection!", CP.GREEN);
                perfection_.effect(null, seme, null);
                seme.skills_.put(perfection_.name_, perfection_);
            }
        }
    }

}
