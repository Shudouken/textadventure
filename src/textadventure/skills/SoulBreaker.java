package textadventure.skills;

import textadventure.Game;
import textadventure.Player;
import textadventure.Skill;

public class SoulBreaker extends Skill
{

    public SoulBreaker()
    {
        super("Soul Breaker", 15, 10);
    }

    @Override
    public String getDescription()
    {
        return "Attack that deals damage to the MP of an enemy.";
    }

    @Override
    public int getCost()
    {
        return (basecost_ + (3 * skilllevel_));
    }

    @Override
    public void effect(Game game, Player seme, Player uke)
    {

        if (uke == null)
            System.out.println("No target for skill: " + name_);
        else if (skillCost(seme, getCost()))
        {
            int atk = (int) (seme.stats_.getInt() / 2 * (1 + (0.1f * skilllevel_)));
            float acc = seme.equip_.weapon_.calcAccuracy(seme.stats_);

            //uke
            int def = uke.equip_.armor_.calcDefense(uke.stats_);
            int res = uke.equip_.armor_.calcResistance(uke.stats_);
            float eva = uke.equip_.armor_.calcEvasion(uke.stats_);

            uke.takeMPDamage(seme, atk, acc, def, res, eva);
        }
    }

}
