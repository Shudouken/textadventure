package textadventure.skills;

import textadventure.Game;
import textadventure.Player;
import textadventure.Skill;
import textadventure.CP;

public class Feast extends Skill
{

    public Feast()
    {
        super("Feast", 2, 5);
    }

    @Override
    public String getDescription()
    {
        return "Take a bite of the enemy's flesh to heal yourself";
    }

    @Override
    public int getCost()
    {
        return (basecost_ + (1 * skilllevel_));
    }

    @Override
    public void effect(Game game, Player seme, Player uke)
    {

        if (uke == null)
            System.out.println("No target for skill: " + name_);
        else if (skillCost(seme, getCost()))
        {
            int atk = seme.equip_.weapon_.calcAttack(seme.stats_);
            float acc = seme.equip_.weapon_.calcAccuracy(seme.stats_);
            float crit = seme.equip_.weapon_.calcCritical(seme.stats_);

            //uke
            int def = uke.equip_.armor_.calcDefense(uke.stats_);
            int res = uke.equip_.armor_.calcResistance(uke.stats_);
            float eva = uke.equip_.armor_.calcEvasion(uke.stats_);

            int dmg = uke.takeDamage(seme, atk, acc, crit, def, res, eva);

            int heal = dmg / (8 - skilllevel_);
            seme.stats_.gainHP(heal);
            System.out.println(seme.name_ + " fed on " + CP.getColorString("" + heal, CP.GREEN) + " HP!");
        }
    }

}
