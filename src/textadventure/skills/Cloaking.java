package textadventure.skills;

import textadventure.Game;
import textadventure.Player;
import textadventure.Skill;

public class Cloaking extends Skill
{

    public Cloaking()
    {
        super("Cloaking", 1, 1);
        offensive_ = false;
    }

    @Override
    public String getDescription()
    {
        return "Stay hidden from enemies on the map, but gradually consume MP while moving.";
    }

    @Override
    public int getCost()
    {
        return 1;
    }

    @Override
    public void effect(Game game, Player seme, Player uke)
    {
        if (seme.stealth_)
        {
            deactivate();
        } else
        {
            if (skillCost(seme, getCost()))
                activate();
        }
    }

    public static void consumeMP()
    {
        if (skillCost(Game.getInstance().player_, 1))
            if (!(Game.getInstance().player_.stats_.mp_ == 0))
                return;

        deactivate();
    }

    public static void activate()
    {
        Game.getInstance().player_.stealth_ = true;
        Game.getInstance().pt_.symbol_ = "忍";
        System.out.println(Game.getInstance().player_.name_ + " concealed " + Player.pronoun_map_.get(Game.getInstance().player_.sex_) + " presence!");
    }

    public static void deactivate()
    {
        Game.getInstance().player_.stealth_ = false;
        Game.getInstance().pt_.symbol_ = "私";
        System.out.println(Game.getInstance().player_.name_ + " went out of cloaking!");
    }

}
