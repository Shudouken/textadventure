package textadventure.skills;

import textadventure.*;

import java.util.ArrayList;
import java.util.List;

public class Steal extends Skill
{

    public Steal()
    {
        super("Steal", 3, 15);
    }

    @Override
    public String getDescription()
    {
        return "Steals the item an Enemy is holding. Success rate is based on Dexterity.";
    }

    @Override
    public void effect(Game game, Player seme, Player uke)
    {
        System.out.println(seme.name_ + " wants to steal an item!");

        if (uke == null)
            System.out.println("No target for skill: " + name_);
        else if (skillCost(seme, getCost()))
        {
            int success = 0;

            if (uke.getClass() == Enemy.class)
                success = game.enemy_.droprate_ * skilllevel_ + (game.player_.stats_.dex_ / 2);
            else
                success = (5 * skilllevel_) + (seme.stats_.dex_ / 2);

            if (Randomizer.randInt(0, 100) <= success)
            {
                if (uke.getClass() == Enemy.class && ((Enemy) uke).drop_ != null)
                {
                    System.out.println(seme.name_ + " stole " + game.enemy_.drop_.name_ + "!");
                    game.player_.addItemToInventory(game.enemy_.drop_);
                    game.enemy_.drop_ = null;
                } else if (!uke.inventory_.isEmpty())
                {
                    List<Item> keys = new ArrayList<Item>(game.player_.inventory_.keySet());
                    Item randomItem = keys.get(Randomizer.randInt(0, keys.size() - 1));

                    System.out.println(seme.name_ + " stole " + randomItem.name_ + "!");
                    uke.removeItemFromInventory(randomItem);
                } else
                    System.out.println("Nothing to steal!");
            } else
                System.out.println("Failed to steal an item!");
        }
    }

    @Override
    public int getCost()
    {
        return (basecost_ + (1 * skilllevel_));
    }

}
