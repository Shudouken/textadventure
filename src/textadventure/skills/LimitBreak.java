package textadventure.skills;

import textadventure.CP;
import textadventure.Game;
import textadventure.Player;
import textadventure.Skill;

public class LimitBreak extends Skill
{

    public LimitBreak()
    {
        super("Limit Break", 0, 5);
    }

    @Override
    public String getDescription()
    {
        return "Exceed the utmost limit of your powers, but barely survive the process.";
    }

    @Override
    public int getCost()
    {
        return 0;
    }

    @Override
    public void effect(Game game, Player seme, Player uke)
    {

        if (uke == null)
            System.out.println("No target for skill: " + name_);
        else if (skillCost(seme, getCost()))
        {
            int hp = seme.stats_.hp_ - 1;
            int mp = seme.stats_.mp_ - 1;

            if (hp == 0 || mp == 0)
            {
                System.out.println("Cannot break limits!");
                return;
            }

            seme.stats_.loseHP(hp);
            seme.stats_.loseMP(mp);

            System.out.println(seme.name_ + " uses " + CP.getColorString("" + hp, CP.RED) + " HP and "
                    + CP.getColorString("" + mp, CP.BLUE) + " MP to break the limits.");

            //seme
            int atk = (seme.equip_.weapon_.calcAttack(seme.stats_) * skilllevel_) + (hp + mp);
            float acc = (seme.equip_.weapon_.calcAccuracy(seme.stats_) * skilllevel_) + (hp + mp);
            float crit = seme.equip_.weapon_.calcCritical(seme.stats_) + skilllevel_;

            //uke
            int def = uke.equip_.armor_.calcDefense(uke.stats_);
            int res = uke.equip_.armor_.calcResistance(uke.stats_);
            float eva = uke.equip_.armor_.calcEvasion(uke.stats_);

            uke.takeDamage(seme, atk, acc, crit, def, res, eva);
        }
    }

}
