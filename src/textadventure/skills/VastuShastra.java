package textadventure.skills;

import textadventure.Buff;
import textadventure.Game;
import textadventure.Player;
import textadventure.Skill;
import textadventure.Stats;

public class VastuShastra extends Skill
{

    public VastuShastra()
    {
        super("Vastu Shastra", 0, 0);
    }

    @Override
    public String getDescription()
    {
        return "Continuous knowledge of the basic elements has hardened your spiritual defenses.";
    }

    @Override
    public int getCost()
    {
        return 0;
    }

    @Override
    public void effect(Game game, Player seme, Player uke)
    {
        Stats stats = seme.stats_;

        Buff buffs = null;
        if (stats instanceof Buff)
        {
            buffs = ((Buff) stats);
            stats = ((Buff) stats).original_;
        }

        stats.res_fire_ *= 1.25f;
        stats.res_water_ *= 1.25f;
        stats.res_wind_ *= 1.25f;
        stats.res_thunder_ *= 1.25f;

        if (buffs != null)
            seme.stats_ = buffs.reapplyBuffs(stats);
        else
            seme.stats_ = stats;
    }

}
