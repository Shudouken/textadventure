package textadventure.skills;

import textadventure.Buff;
import textadventure.Game;
import textadventure.Player;
import textadventure.Skill;
import textadventure.Stats;

public class Perfection extends Skill
{

    public Perfection()
    {
        super("Perfection", 0, 0);
    }

    @Override
    public String getDescription()
    {
        return "Practice makes perfect, granting you a bonus to all stats.";
    }

    @Override
    public int getCost()
    {
        return 0;
    }

    @Override
    public void effect(Game game, Player seme, Player uke)
    {
        Stats stats = seme.stats_;

        Buff buffs = null;
        if (stats instanceof Buff)
        {
            buffs = ((Buff) stats);
            stats = ((Buff) stats).original_;
        }

        stats.str_ += 5;
        stats.vit_ += 5;
        stats.int_ += 5;
        stats.res_ += 5;
        stats.dex_ += 5;
        stats.agi_ += 5;

        if (buffs != null)
            seme.stats_ = buffs.reapplyBuffs(stats);
        else
            seme.stats_ = stats;
    }

}
