package textadventure.skills;

import textadventure.Game;
import textadventure.Item;
import textadventure.Player;
import textadventure.Randomizer;
import textadventure.Skill;
import textadventure.Weapon;

public class DropWeapon extends Skill
{

    public DropWeapon()
    {
        super("Drop Weapon", 10, 10);
    }

    @Override
    public String getDescription()
    {
        return "Try to make the enemy drop his weapon.";
    }

    @Override
    public int getCost()
    {
        return (basecost_ + (1 * skilllevel_));
    }

    @Override
    public void effect(Game game, Player seme, Player uke)
    {

        if (uke == null)
            System.out.println("No target for skill: " + name_);
        else if (skillCost(seme, getCost()))
        {
            int success = 15 + 2 * skilllevel_ + (seme.stats_.getDex() / 3);

            System.out.println(success);

            if (Randomizer.randInt(0, 100) <= success)
            {
                uke.equip_.weapon_ = (Weapon) Item.bare_hand_;
                System.out.println(uke.name_ + " has dropped his weapon!");
                return;
            }
            System.out.println(name_ + " has failed!");
        }
    }

}
