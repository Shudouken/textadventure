package textadventure.skills;

import textadventure.Game;
import textadventure.Player;
import textadventure.Skill;

import static java.lang.Math.min;

public class Shenanigan extends Skill
{

    public Shenanigan()
    {
        super("Shenanigans", 8, 1);
    }

    @Override
    public String getDescription()
    {
        return "Skill used by Imps, evenly redistributes target's HP and MP.";
    }

    @Override
    public int getCost()
    {
        return (basecost_ + (1 * skilllevel_));
    }

    @Override
    public void effect(Game game, Player seme, Player uke)
    {

        System.out.println(seme.name_ + " starts to chant: ÆÐØþŧŶƕ");

        if (uke == null)
            System.out.println("No target for skill: " + name_);
        else if (skillCost(seme, getCost()))
        {
            int hp = uke.stats_.hp_;
            int mp = uke.stats_.mp_;

            int mean = (hp + mp) / 2;

            int newhp = min(mean, uke.stats_.max_hp_);
            int newmp = min(mean, uke.stats_.max_mp_);

            if (newhp > hp)
                uke.stats_.gainHP(newhp - hp);
            else
                uke.stats_.loseHP(hp - newhp);

            if (newmp > mp)
                uke.stats_.gainMP(newmp - mp);
            else
                uke.stats_.loseMP(mp - newmp);

            System.out.println(uke.name_ + "'s HP and MP were altered!");
        }
    }

}
