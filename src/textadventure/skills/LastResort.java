package textadventure.skills;

import textadventure.Game;
import textadventure.Player;
import textadventure.Skill;
import textadventure.Buff;

import static java.lang.Math.min;

public class LastResort extends Skill
{

    public LastResort()
    {
        super("Last Resort", 3, 10);
    }

    @Override
    public String getDescription()
    {
        String temp = "";
        String eol = System.getProperty("line.separator");

        temp += "Musters up all the strength left in the body for an undodgeable" + eol;
        temp += "powerful blow, but leaves the user defenseless for a turn.";

        return temp;
    }

    @Override
    public int getCost()
    {
        return (basecost_ + (1 * skilllevel_));
    }

    @Override
    public void effect(Game game, Player seme, Player uke)
    {
        System.out.println(seme.name_ + " wants to go all out!");

        if (uke == null)
            System.out.println("No target for skill: " + name_);
        else if (skillCost(seme, getCost()))
        {
            int atk = (int) (seme.equip_.weapon_.calcAttack(seme.stats_) * (1 + (skilllevel_ * 0.05f))
                    + (seme.stats_.max_hp_) / 3);

            //uke
            int def = uke.equip_.armor_.calcDefense(uke.stats_);
            int res = uke.equip_.armor_.calcResistance(uke.stats_);
            float eva = uke.equip_.armor_.calcEvasion(uke.stats_);

            uke.takeDamage(seme, atk, 100, 0, def, res, eva);

            seme.debuff("Str");
            ((Buff) seme.stats_).turns_active_ = 15 - min(skilllevel_, 10);
            seme.wait_turn_ = true;
        }
    }

}
