package textadventure.skills;

import textadventure.Buff;
import textadventure.Game;
import textadventure.Player;
import textadventure.Skill;
import textadventure.Stats;

public class Longevity extends Skill
{

    public Longevity()
    {
        super("Longevity", 0, 0);
    }

    @Override
    public String getDescription()
    {
        return "Bread and soup, a rough diet has trained your body for survival.";
    }

    @Override
    public int getCost()
    {
        return 0;
    }

    @Override
    public void effect(Game game, Player seme, Player uke)
    {
        Stats stats = seme.stats_;

        Buff buffs = null;
        if (stats instanceof Buff)
        {
            buffs = ((Buff) stats);
            stats = ((Buff) stats).original_;
        }

        stats.hp_bonus_ += 50;

        if (buffs != null)
            seme.stats_ = buffs.reapplyBuffs(stats);
        else
            seme.stats_ = stats;
    }

}
