package textadventure.skills;

import textadventure.Game;
import textadventure.Player;
import textadventure.Skill;

public class AuraBlade extends Skill
{

    public AuraBlade()
    {
        super("Aura Blade", 4, 10);
    }

    @Override
    public String getDescription()
    {
        return "Coat your weapon with magic for a decent strike.";
    }

    @Override
    public int getCost()
    {
        return (basecost_ + (1 * skilllevel_));
    }

    @Override
    public void effect(Game game, Player seme, Player uke)
    {

        if (uke == null)
            System.out.println("No target for skill: " + name_);
        else if (skillCost(seme, getCost()))
        {
            //seme
            int atk = seme.equip_.weapon_.calcAttack(seme.stats_) + (seme.stats_.int_ * skilllevel_);
            float acc = seme.equip_.weapon_.calcAccuracy(seme.stats_);
            float crit = seme.equip_.weapon_.calcCritical(seme.stats_);

            //uke
            int def = uke.equip_.armor_.calcDefense(uke.stats_);
            int res = uke.equip_.armor_.calcResistance(uke.stats_);
            float eva = uke.equip_.armor_.calcEvasion(uke.stats_);

            uke.takeDamage(seme, atk, acc, crit, def, res, eva);
        }
    }

}
