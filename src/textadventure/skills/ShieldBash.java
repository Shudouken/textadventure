package textadventure.skills;

import textadventure.Game;
import textadventure.Player;
import textadventure.Skill;
import textadventure.items.IronShield;
import textadventure.items.TemplarsCross;

public class ShieldBash extends Skill
{

    public ShieldBash()
    {
        super("Shield Bash", 6, 10);
    }

    @Override
    public String getDescription()
    {
        return "A powerful bash carrying the full weight of your body. Can only be used when wielding a shield.";
    }

    @Override
    public int getCost()
    {
        return (basecost_ + (2 * skilllevel_));
    }

    @Override
    public void effect(Game game, Player seme, Player uke)
    {

        if (seme.equip_.weapon_ instanceof IronShield || seme.equip_.weapon_ instanceof TemplarsCross)
        {

            if (uke == null)
                System.out.println("No target for skill: " + name_);
            else if (skillCost(seme, getCost()))
            {
                //seme
                int atk = seme.equip_.weapon_.calcAttack(seme.stats_) + seme.stats_.vit_ + (skilllevel_ * seme.stats_.str_ / 2);
                float acc = seme.equip_.weapon_.calcAccuracy(seme.stats_);
                float crit = seme.equip_.weapon_.calcCritical(seme.stats_);

                //uke
                int def = uke.equip_.armor_.calcDefense(uke.stats_);
                int res = uke.equip_.armor_.calcResistance(uke.stats_);
                float eva = uke.equip_.armor_.calcEvasion(uke.stats_);

                uke.takeDamage(seme, atk, acc, crit, def, res, eva);
            }
        } else
            System.out.println(seme.name_ + " is not wielding a shield!");
    }

}
