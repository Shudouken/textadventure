package textadventure.skills;

import textadventure.*;
import textadventure.items.Lingerie;
import textadventure.items.Swimwear;

public class Charm extends Skill
{

    public Charm()
    {
        super("Charm", 10, 5);
    }

    @Override
    public String getDescription()
    {
        return "Skill used by Succubi and Incubi, charms a target of the opposite gender.";
    }

    @Override
    public int getCost()
    {
        return (basecost_ + (1 * skilllevel_));
    }

    @Override
    public void effect(Game game, Player seme, Player uke)
    {

        if (uke == null)
            System.out.println("No target for skill: " + name_);
        else if (skillCost(seme, getCost()))
        {
            if(uke.sex_ != seme.sex_ && ! uke.charmed_)
            {
                int success = 25 + 5*skilllevel_;
                if(seme.equip_.armor_ instanceof Swimwear)
                    success += 10;
                if(seme.equip_.armor_ instanceof Lingerie)
                    success += 20;

                success = (int) (success / uke.stats_.susceptibility_);

                charmQuote(seme);
                if (Randomizer.randInt(0, 100) <= success)
                {
                    uke.charmed_ = true;
                    System.out.println(uke.name_ + " fell under " + seme.name_ + "'s charm!");
                    return;
                }
            }
            System.out.println(name_ + " has failed!");
        }
    }

    private final static String[][] quotes_;
    static
    {
        quotes_ = new String[2][6];
        //  male quotes
        quotes_[0][0] = "How you doin'?";
        quotes_[0][1] = "Want me to give you a massage?";
        quotes_[0][2] = "You look stunning today, are you going to a ball?";
        quotes_[0][3] = "Why fight when we could have some fun instead?";
        quotes_[0][4] = "Let's chat some more instead of fighting.";
        quotes_[0][5] = "I've never seen such a beauty before";

        //female quotes
        quotes_[1][0] = "Whoops, dropped my weapon! Can't you pick it up for me, honey?";
        quotes_[1][1] = "Is it me or is it getting really hot? Mind if I take off some clothes?";
        quotes_[1][2] = "I could teach you other ways to use this 'weapon' of yours..";
        quotes_[1][3] = "Why fight when we could have some fun instead?";
        quotes_[1][4] = "My back itches, won't you give it a scratch for me?";
        quotes_[1][5] = "Oh my.. what a strong and handsome man do we have here..?";
    }

    public void charmQuote(Player seme)
    {
        int rand = Randomizer.randInt(0,5);
        System.out.println(seme.name_ + ": " + quotes_[seme.sex_-1][rand]);
    }

}
