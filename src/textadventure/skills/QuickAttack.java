package textadventure.skills;

import textadventure.Game;
import textadventure.Player;
import textadventure.Skill;

public class QuickAttack extends Skill
{

    public QuickAttack()
    {
        super("Quick Attack", 2, 10);
    }

    @Override
    public String getDescription()
    {
        String temp = "";
        String eol = System.getProperty("line.separator");

        temp += "Wager everything on your speed. Deals more damage the higher" + eol;
        temp += "your speed is in comparison with the enemy's.";

        return temp;
    }

    @Override
    public int getCost()
    {
        return (basecost_ + (1 * skilllevel_));
    }

    @Override
    public void effect(Game game, Player seme, Player uke)
    {

        if (uke == null)
            System.out.println("No target for skill: " + name_);
        else if (skillCost(seme, getCost()))
        {
            int diff = (int) (seme.stats_.agi_ * (1.25f + (0.1f * skilllevel_)) - uke.stats_.agi_);
            //seme
            int atk = seme.equip_.weapon_.calcAttack(seme.stats_) + diff;
            float acc = seme.equip_.weapon_.calcAccuracy(seme.stats_) + diff;
            float crit = seme.equip_.weapon_.calcCritical(seme.stats_);

            //uke
            int def = uke.equip_.armor_.calcDefense(uke.stats_) - diff;
            int res = uke.equip_.armor_.calcResistance(uke.stats_) - diff;
            float eva = uke.equip_.armor_.calcEvasion(uke.stats_) - diff;

            uke.takeDamage(seme, atk, acc, crit, def, res, eva);
        }
    }

}
