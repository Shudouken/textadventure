package textadventure.skills;

import textadventure.Enemy;
import textadventure.Game;
import textadventure.Player;
import textadventure.Randomizer;
import textadventure.Skill;

public class Exorcise extends Skill
{

    public Exorcise()
    {
        super("Exorcise", 15, 10);
    }

    @Override
    public String getDescription()
    {
        return "Channel divine energy to exorcise a demon in gods place.";
    }

    @Override
    public int getCost()
    {
        return (basecost_ + (1 * skilllevel_));
    }

    @Override
    public void effect(Game game, Player seme, Player uke)
    {
        System.out.println(seme.name_ + " wants to exorcise a monster!");

        if (uke == null)
            System.out.println("No target for skill: " + name_);
        else if (skillCost(seme, getCost()))
        {
            int success = (2 * skilllevel_) + (seme.stats_.res_ / 2) + (seme.stats_.int_ / 4);

            if (((Enemy) uke).demon_ && Randomizer.randInt(0, 100) <= success)
            {
                System.out.println("Successfully exorcised " + uke.name_);
                uke.stats_.hp_ = 0;
            } else
                System.out.println("Failed to exorcise target!");
        }
    }

}
