package textadventure.skills;

import textadventure.Buff;
import textadventure.Game;
import textadventure.Player;
import textadventure.Skill;

public class Sanctus extends Skill
{

    public Sanctus()
    {
        super("Sanctus", 10, 10);
    }

    @Override
    public String getDescription()
    {
        return "Performs a holy ritual that weakens the evil's defenses.";
    }

    @Override
    public int getCost()
    {
        return (basecost_ + (1 * skilllevel_) + (skilllevel_ / 2));
    }

    @Override
    public void effect(Game game, Player seme, Player uke)
    {

        if (uke == null)
            System.out.println("No target for skill: " + name_);
        else
        {
            if (uke.stats_ instanceof Buff)
            {
                System.out.println("Something is interfering with the ritual!");
                return;
            }

            uke.debuff("Vit");
            ((Buff) uke.stats_).turns_active_ = 3 + (skilllevel_ / 2);

            uke.debuff("Res");
            ((Buff) uke.stats_).turns_active_ = 3 + (skilllevel_ / 2);

            System.out.println(uke.name_ + "'s defenses were weakened!");
        }
    }

}
