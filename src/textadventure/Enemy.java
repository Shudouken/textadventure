
package textadventure;

import textadventure.tiles.EnemyTile;


public class Enemy extends Player
{

    public int droprate_;
    public Item drop_;
    public boolean demon_ = false;
    public EnemyTile et_;
    public int base_exp_;
    public static Skill skill_;

    public Enemy()
    {
        super();

        equip_ = new Equipment();
        equip_.weapon_ = (Weapon) Item.bare_hand_;
        equip_.armor_ = (Armor) Item.bare_body_;

        sex_ = Randomizer.randInt(1,2);

        skill_ = null;

        lvlscaling_.str_ = 100;
        lvlscaling_.vit_ = 150;
        lvlscaling_.int_ = 100;
        lvlscaling_.res_ = 150;
        lvlscaling_.dex_ = 100;
        lvlscaling_.agi_ = 150;
    }

    public void levelUp(int monsterlevel)
    {
        for (int i = 0; i < monsterlevel; i++)
            levelUp();
    }

    @Override
    public void levelUp()
    {
        if (level_ < 99)
        {
            exp_ = base_exp_ + level_;
            level_ += 1;

            Stats old = new Stats(stats_);

            if (lvlscaling_.str_ >= 100)
                stats_.str_ += 1;
            stats_.str_ += level_ % ((100 - (lvlscaling_.str_ % 100)) / 10) == 0 ? 1 : 0;

            if (lvlscaling_.vit_ >= 100)
                stats_.vit_ += 1;
            stats_.vit_ += level_ % ((100 - (lvlscaling_.vit_ % 100)) / 10) == 0 ? 1 : 0;

            if (lvlscaling_.int_ >= 100)
                stats_.int_ += 1;
            stats_.int_ += level_ % ((100 - (lvlscaling_.int_ % 100)) / 10) == 0 ? 1 : 0;

            if (lvlscaling_.res_ >= 100)
                stats_.res_ += 1;
            stats_.res_ += level_ % ((100 - (lvlscaling_.res_ % 100)) / 10) == 0 ? 1 : 0;

            if (lvlscaling_.dex_ >= 100)
                stats_.dex_ += 1;
            stats_.dex_ += level_ % ((100 - (lvlscaling_.dex_ % 100)) / 10) == 0 ? 1 : 0;

            if (lvlscaling_.agi_ >= 100)
                stats_.agi_ += 1;
            stats_.agi_ += level_ % ((100 - (lvlscaling_.agi_ % 100)) / 10) == 0 ? 1 : 0;

            stats_.updateHPMP(level_);
        }
    }

    @Override
    public String toString()
    {
        String temp = "";
        String eol = System.getProperty("line.separator");

        temp += "Name: " + name_ + "\t" + "Shekel Drop: " + shekels_ + eol;
        temp += "LVL:  " + level_ + "/99";
        temp += "\tEXP Gain:  " + exp_ + eol;
        temp += stats_;
        temp += equip_.armor_.toResistances(stats_);
        if (drop_ != null)
            temp += "Item Drop: " + drop_.name_ + " (" + droprate_ + "%)" + eol;
        else
            temp += "Item Drop: - (-%)" + eol;
        temp += "Weapon: " + equip_.weapon_.name_;
        temp += ", Armor : " + equip_.armor_.name_;
        if(skill_ != null)
            temp += ", Skill: " + skill_.name_ + eol;
        else
            temp += ", Skill: None" + eol;

        return temp;
    }

    public void action(Game game, Player player)
    {
        Command.attack(this, player);
    }

    public void cast(Spell spell, Player player)
    {
        System.out.println(name_ + " tries to cast " + spell.name_ + "..");
        spell.cast(this, player);
    }

    public void useSkill(Game game, Player player)
    {
        if (skill_ != null && ! StatusEffects.checkCharm(this, player))
        {
            skill_.effect(game, this, player);
        }
    }

    public void talk(String line)
    {
        System.out.println(name_ + " says: " + line);
    }

    @Override
    public int hashCode()
    {
        return name_.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof Enemy))
            return false;
        if (obj == this)
            return true;
        if (((Enemy) obj).name_.equals(name_))
            return true;

        return false;
    }

}
