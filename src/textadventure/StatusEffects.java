package textadventure;

public class StatusEffects
{
    public static boolean checkCharm(Player seme, Player uke)
    {
        if(uke != null && seme.charmed_)
        {
            System.out.println(seme.name_ + " is charmed by " + uke.name_ + "!");
            int rand = Randomizer.randInt(0,5);

            if(rand <= 3)
            {
                System.out.println(seme.name_ + " can't bear to hurt " + uke.name_ + "!");
                return true;
            }

            if(rand == 5)
            {
                seme.charmed_ = false;
                System.out.println(seme.name_ + " snapped out of it!");
            }
        }

        return false;
    }

    public static void checkPoison(Player player)
    {
        if(player != null && player.poisoned_)
        {
            int rand = Randomizer.randInt(0,5);
            if(rand == 5)
            {
                player.poisoned_ = false;
                System.out.println(player.name_ + " got cured of poison!");
                return;
            }

            System.out.print(player.name_ + " is poisoned! ");
            int damage = (int) ((1 + (player.stats_.max_hp_/50)) / player.stats_.susceptibility_);
            player.stats_.loseHP(damage);
            System.out.println(player.name_ + " lost " + CP.getColorString(""+damage, CP.RED) + " HP!");
        }
    }

    public static void reset(Player player)
    {
        player.charmed_ = false;
        player.poisoned_ = false;
    }
}
