package textadventure;

public class Position implements Comparable<Position>, java.io.Serializable
{

    public int x_ = 0;
    public int y_ = 0;

    public Position(int x, int y)
    {
        x_ = x;
        y_ = y;
    }

    public Position(Position other)
    {
        this.x_ = other.x_;
        this.y_ = other.y_;
    }

    @Override
    public int compareTo(Position o)
    {
        //TODO if needed
        return this.hashCode() - o.hashCode();
    }

    @Override
    public boolean equals(Object other)
    {

        if (this == other) return true;

        if (other == null || (this.getClass() != other.getClass()))
        {
            return false;
        }

        Position pos = (Position) other;
        if (this.x_ == pos.x_ && this.y_ == pos.y_)
            return true;

        return false;
    }

    @Override
    public int hashCode()
    {
        int result = 0;
        result = 31 * result + x_;
        result = 31 * result + y_;

        return result;
    }

    @Override
    public String toString()
    {
        return "(" + x_ + "," + y_ + ")";
    }

}
