package textadventure;

public class LevelScaling implements java.io.Serializable
{

    //determines player scaling on levelup
    public int str_ = 35;
    public int vit_ = 35;
    public int int_ = 35;
    public int dex_ = 35;
    public int agi_ = 35;
    public int res_ = 35;

    public void increaseAll(int num)
    {
        str_ += num;
        vit_ += num;
        int_ += num;
        dex_ += num;
        agi_ += num;
        res_ += num;
    }

    @Override
    public String toString()
    {
        String temp = "";
        String eol = System.getProperty("line.separator");

        temp += "StrScaling: " + str_ + eol;
        temp += "VitScaling: " + vit_ + eol;
        temp += "IntScaling: " + int_ + eol;
        temp += "ResScaling: " + res_ + eol;
        temp += "DexScaling: " + dex_ + eol;
        temp += "AgiScaling: " + agi_ + eol;

        return temp;
    }

}
