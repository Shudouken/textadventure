
package textadventure;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class Buff extends Stats
{

    public Stats under_;
    public Stats original_;
    public int turns_active_ = 4;
    private String attribute_;
    private boolean debuff_ = false;

    public Buff(Stats stats)
    {
        super(stats);

        if (stats instanceof Buff)
        {
            under_ = stats;
            original_ = ((Buff) stats).original_;
        } else
        {
            under_ = stats;
            original_ = stats;
        }
    }

    public Stats countDown()
    {
        turns_active_--;
        //System.out.println("Counting down " + attribute_ + " buff: " + turns_active_);

        if (turns_active_ <= 0)
            return removeBuff();
        else if (under_ instanceof Buff)
            under_ = ((Buff) under_).countDown();

        return this;
    }

    public void applyBuff(String attribute)
    {
        attribute_ = attribute;
    }

    public void applyBuff(String attribute, int duration)
    {
        applyBuff(attribute);
        this.turns_active_ = duration;
    }

    public void applyDebuff(String attribute)
    {
        attribute_ = attribute;
        debuff_ = true;
    }

    public void applyDebuff(String attribute, int duration)
    {
        applyDebuff(attribute);
        this.turns_active_ = duration;
    }

    public Stats reapplyBuffs(Stats original)
    {
        this.original_ = original;

        if (under_ instanceof Buff)
            this.under_ = ((Buff) under_).reapplyBuffs(original);
        else
            this.under_ = original;

        Buff buff = new Buff(under_);
        buff.applyBuff(attribute_);
        buff.debuff_ = this.debuff_;
        buff.turns_active_ = this.turns_active_;

        //System.out.println("Reapply buff for " + buff.attribute_ + " with turns active " + buff.turns_active_);

        return buff;
    }

    public Stats removeBuff()
    {
        if (debuff_)
            System.out.println(attribute_ + " debuff wore off!");
        else
            System.out.println(attribute_ + " buff wore off!");

        return under_;
    }

    public Stats removeAllBuffs()
    {
        if (debuff_)
            System.out.println(attribute_ + " debuff wore off!");
        else
            System.out.println(attribute_ + " buff wore off!");

        if (under_ instanceof Buff)
            return ((Buff) under_).removeAllBuffs();
        else
            return under_;
    }

    @Override
    public String toString()
    {
        String temp = "";
        String eol = System.getProperty("line.separator");

        String str = String.format("%4d", getStr());
        str = colorStat(getStr(), original_.str_, str);

        String vit = String.format("%4d", getVit());
        vit = colorStat(getVit(), original_.vit_, vit);

        String inte = String.format("%4d", getInt());
        inte = colorStat(getInt(), original_.int_, inte);

        String res = String.format("%4d", getRes());
        res = colorStat(getRes(), original_.res_, res);

        String dex = String.format("%4d", getDex());
        dex = colorStat(getDex(), original_.dex_, dex);

        String agi = String.format("%4d", getAgi());
        agi = colorStat(getAgi(), original_.agi_, agi);

        temp += "HP: " + String.format("%4d", hp_) + "/" + max_hp_ + "\t";
        temp += "MP: " + String.format("%4d", mp_) + "/" + max_mp_ + eol;
        temp += "Str: " + str + "\t";
        temp += "Vit: " + vit + eol;
        temp += "Int: " + inte + "\t";
        temp += "Res: " + res + eol;
        temp += "Dex: " + dex + "\t";
        temp += "Agi: " + agi + eol;

        return temp;
    }

    private String colorStat(int buffed, int normal, String string)
    {
        if (buffed > normal)
            return CP.getColorString(string, CP.GREEN);
        else if (buffed < normal)
            return CP.getColorString(string, CP.RED);
        else
            return string;
    }

    @Override
    public int getStr()
    {
        return getBuffedStat("Str", under_.getStr());
    }

    @Override
    public int getVit()
    {
        return getBuffedStat("Vit", under_.getVit());
    }

    @Override
    public int getInt()
    {
        return getBuffedStat("Int", under_.getInt());
    }

    @Override
    public int getRes()
    {
        return getBuffedStat("Res", under_.getRes());
    }

    @Override
    public int getDex()
    {
        return getBuffedStat("Dex", under_.getDex());
    }

    @Override
    public int getAgi()
    {
        return getBuffedStat("Agi", under_.getAgi());
    }

    public int getBuffedStat(String attr, int stat)
    {
        return (int) (attribute_.contains(attr) ? (debuff_ ? stat / 1.33f : stat * 1.33f) : stat);
    }

    @Override
    public void loseHP(int amount)
    {
        hp_ = max(hp_ - amount, 0);
        under_.loseHP(amount);
    }

    @Override
    public void gainHP(int amount)
    {
        hp_ = min(hp_ + amount, max_hp_);
        under_.gainHP(amount);
    }

    @Override
    public void loseMP(int amount)
    {
        mp_ = max(mp_ - amount, 0);
        under_.loseMP(amount);
    }

    @Override
    public void gainMP(int amount)
    {
        mp_ = min(mp_ + amount, max_mp_);
        under_.gainMP(amount);
    }

}
