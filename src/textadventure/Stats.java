package textadventure;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class Stats implements java.io.Serializable
{

    //normal stats
    //Strength, Vitality, Intelligence, Dexterity, Agility, Resistance
    public int str_ = 6;
    public int vit_ = 6;
    public int int_ = 6;
    public int res_ = 6;
    public int dex_ = 6;
    public int agi_ = 6;

    public int hp_;
    public int max_hp_;
    public int mp_;
    public int max_mp_;

    public int hp_bonus_ = 0;
    public int mp_bonus_ = 0;
    public float eva_bonus_ = 0.0f;
    public float crit_bonus_ = 0.0f;

    public float res_fire_ = 1.0f;
    public float res_water_ = 1.0f;
    public float res_wind_ = 1.0f;
    public float res_thunder_ = 1.0f;
    public float res_light_ = 1.0f;
    public float res_darkness_ = 1.0f;

    //hidden stats
    public float exp_rate_ = 1.0f;
    public float susceptibility_ = 1.0f;

    public Stats()
    {
    }

    //lazy copy, only for levelup
    public Stats(Stats another)
    {
        this.max_hp_ = another.max_hp_;
        this.max_mp_ = another.max_mp_;
        this.hp_ = another.hp_;
        this.mp_ = another.mp_;
        this.str_ = another.str_;
        this.vit_ = another.vit_;
        this.int_ = another.int_;
        this.res_ = another.res_;
        this.dex_ = another.dex_;
        this.agi_ = another.agi_;
        this.hp_bonus_ = another.hp_bonus_;
        this.mp_bonus_ = another.mp_bonus_;
        this.eva_bonus_ = another.eva_bonus_;
        this.crit_bonus_ = another.crit_bonus_;
        this.res_wind_ = another.res_wind_;
        this.res_darkness_ = another.res_darkness_;
        this.res_fire_ = another.res_fire_;
        this.res_light_ = another.res_light_;
        this.res_thunder_ = another.res_thunder_;
        this.res_water_ = another.res_water_;
        this.exp_rate_ = another.exp_rate_;
        this.susceptibility_ = another.susceptibility_;
    }

    public void updateHPMP(int level)
    {
        max_hp_ = (30 + hp_bonus_ + (str_ / 2)) * ((vit_ / 10) + 1) + level + vit_;
        hp_ = max_hp_;

        max_mp_ = (10 + mp_bonus_ + (res_ / 2)) * ((int_ / 10) + 1) + (level / 2) + int_;
        mp_ = max_mp_;
    }

    public float getAcc()
    {
        return (getDex() * 1.5f);
    }

    public float getEva()
    {
        return (min(getAgi() * 0.5f, 85.0f) + eva_bonus_);
    }

    public float getCrit()
    {
        return min((getDex() + (getAgi() / 3)) * 0.5f, 65.0f) + crit_bonus_;
    }

    @Override
    public String toString()
    {
        String temp = "";
        String eol = System.getProperty("line.separator");

        temp += "HP: " + String.format("%4d", hp_) + "/" + max_hp_ + "\t";
        temp += "MP: " + String.format("%4d", mp_) + "/" + max_mp_ + eol;
        temp += "Str: " + String.format("%4d", str_) + "\t";
        temp += "Vit: " + String.format("%4d", vit_) + eol;
        temp += "Int: " + String.format("%4d", int_) + "\t";
        temp += "Res: " + String.format("%4d", res_) + eol;
        temp += "Dex: " + String.format("%4d", dex_) + "\t";
        temp += "Agi: " + String.format("%4d", agi_) + eol;

        return temp;
    }

    public void printHiddenStats()
    {
        System.out.println("Exp Rate: " + exp_rate_);
        System.out.println("Susceptibility: " + susceptibility_);
        System.out.println("Accuracy: " + getAcc());
        System.out.println("Evasion: " + getEva());
        System.out.println("Critical: " + getCrit());
        System.out.println();
    }

    public int getStr()
    {
        return str_;
    }

    public int getVit()
    {
        return vit_;
    }

    public int getInt()
    {
        return int_;
    }

    public int getRes()
    {
        return res_;
    }

    public int getDex()
    {
        return dex_;
    }

    public int getAgi()
    {
        return agi_;
    }

    public float getFireRes()
    {
        return res_fire_;
    }

    public float getWaterRes()
    {
        return res_water_;
    }

    public float getWindRes()
    {
        return res_wind_;
    }

    public float getThunderRes()
    {
        return res_thunder_;
    }

    public float getDarkRes()
    {
        return res_darkness_;
    }

    public float getLightRes()
    {
        return res_light_;
    }

    public void loseHP(int amount)
    {
        hp_ = max(hp_ - amount, 0);
    }

    public void gainHP(int amount)
    {
        hp_ = min(hp_ + amount, max_hp_);
    }

    public void loseMP(int amount)
    {
        mp_ = max(mp_ - amount, 0);
    }

    public void gainMP(int amount)
    {
        mp_ = min(mp_ + amount, max_mp_);
    }

}
