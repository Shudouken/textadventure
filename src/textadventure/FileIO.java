package textadventure;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class FileIO
{

    public static void saveFile(String filename, Game game)
    {
        filename = appendSuffix(filename);
        System.out.println("Saving File: " + filename);

        try
        {
            FileOutputStream fileOut = new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(game);
            out.close();
            fileOut.close();
        } catch (IOException i)
        {
            i.printStackTrace();
        }
    }

    public static Game loadFile(String filename)
    {
        Game game = Game.getInstance();

        try
        {
            FileInputStream fileIn = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            game = (Game) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException i)
        {
            i.printStackTrace();
            return null;
        } catch (ClassNotFoundException c)
        {
            System.out.println("Game class not found");
            c.printStackTrace();
            return null;
        }

        System.out.println(game.player_);

        return game;
    }

    private static String appendSuffix(String filename)
    {
        if (filename.endsWith(".sav"))
        {
            return filename;
        } else
        {
            return filename + ".sav";
        }
    }

}
