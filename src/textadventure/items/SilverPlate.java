package textadventure.items;

import textadventure.Armor;
import textadventure.Stats;

public class SilverPlate extends Armor
{

    public SilverPlate()
    {
        super("Silver Plate",
                "Armor plating made from silver, equally resistant against magical and physical attacks",
                300);
    }

    @Override
    public int calcDefense(Stats stats)
    {
        return (int) (5 + (stats.getVit() * 1.5f));
    }

    @Override
    public int calcResistance(Stats stats)
    {
        return (int) (5 + (stats.getRes() * 1.5f));
    }

    @Override
    public float calcEvasion(Stats stats)
    {
        return Math.max(stats.getEva() - 10, 0);
    }

}
