package textadventure.items;

import textadventure.Player;
import textadventure.UseableItem;

public class Potion extends UseableItem
{

    public Potion()
    {
        super("Potion",
                "Heals 30 HP",
                15);
    }

    @Override
    public void effect(Player player)
    {
        recoverHP(player, 30);
    }

}
