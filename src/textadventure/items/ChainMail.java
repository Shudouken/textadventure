package textadventure.items;

import textadventure.Armor;
import textadventure.Stats;

public class ChainMail extends Armor
{

    public ChainMail()
    {
        super("Chain Mail",
                "Befitting of a knight, but weak against magical attacks.",
                300);
    }

    @Override
    public int calcDefense(Stats stats)
    {
        return (int) (10 + (stats.getVit() * 1.5f));
    }

    @Override
    public int calcResistance(Stats stats)
    {
        return (int) (stats.getRes() / 2f);
    }

    @Override
    public float calcEvasion(Stats stats)
    {
        return Math.max(stats.getEva() - 10, 0);
    }


}
