package textadventure.items;

import textadventure.CP;
import textadventure.Stats;
import textadventure.Weapon;

public class Whip extends Weapon
{

    public Whip()
    {
        super("Whip",
                "A whip commonly used by Succubi, requires " + CP.getColorString("intelligence", CP.RED) + " to wield.",
                150);
        targets_res_ = true;
    }

    @Override
    public int calcAttack(Stats stats)
    {
        return stats.getInt();
    }

    @Override
    public float calcAccuracy(Stats stats)
    {
        return stats.getAcc();
    }

    @Override
    public float calcCritical(Stats stats)
    {
        return (stats.getCrit());
    }

    @Override
    public int getScaling(Stats stats)
    {
        return (int) (stats.getStr() * 1.25f);
    }

}
