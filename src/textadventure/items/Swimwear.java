package textadventure.items;

import textadventure.Armor;
import textadventure.Stats;

public class Swimwear extends Armor
{

    public Swimwear()
    {
        super("Swimwear",
                "Made from super soft material, but is not so suitable for battle.",
                2500);
    }

    @Override
    public int calcDefense(Stats stats)
    {
        return (int) (stats.getVit() / 1.25f);
    }

    @Override
    public int calcResistance(Stats stats)
    {
        return (int) (stats.getRes() / 1.25f);
    }

    @Override
    public float calcEvasion(Stats stats)
    {
        return 15 + (stats.getEva() * 1.5f);
    }

    @Override
    public float getWaterRes(Stats stats)
    {
        return stats.getWaterRes() * 1.25f;
    }

    @Override
    public float getThunderRes(Stats stats)
    {
        return stats.getWaterRes() * 0.9f;
    }

}
