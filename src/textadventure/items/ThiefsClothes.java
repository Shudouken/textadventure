package textadventure.items;

import textadventure.Armor;
import textadventure.Stats;

public class ThiefsClothes extends Armor
{

    public ThiefsClothes()
    {
        super("Thief's Clothes",
                "Clothing made from cheap materials with the intention of evading attacks",
                300);
    }

    @Override
    public int calcDefense(Stats stats)
    {
        return (int) (stats.getVit() / 1.2f);
    }

    @Override
    public int calcResistance(Stats stats)
    {
        return (int) (stats.getRes() / 1.2f);
    }

    @Override
    public float calcEvasion(Stats stats)
    {
        return 10 + (stats.getEva() * 1.5f);
    }

}
