package textadventure.items;

import textadventure.Armor;
import textadventure.Stats;

public class HolyRobe extends Armor
{

    public HolyRobe()
    {
        super("Holy Robe",
                "Robe of the holy order, decent magical resistance",
                300);
    }

    @Override
    public int calcDefense(Stats stats)
    {
        return (int) (4 + stats.getVit());
    }

    @Override
    public int calcResistance(Stats stats)
    {
        return (int) (5 + (stats.getRes() * 1.5f));
    }

    @Override
    public float calcEvasion(Stats stats)
    {
        return stats.getEva();
    }

}
