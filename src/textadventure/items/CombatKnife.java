package textadventure.items;

import textadventure.CP;
import textadventure.Stats;
import textadventure.Weapon;

public class CombatKnife extends Weapon
{

    public CombatKnife()
    {
        super("Combat Knife",
                "A weapon for tactitians, requires " + CP.getColorString("agility", CP.RED)
                        + " and " + CP.getColorString("dexterity", CP.RED) + " to wield",
                3000);
    }

    @Override
    public int calcAttack(Stats stats)
    {
        return stats.getAgi() + (stats.getDex() / 2);
    }

    @Override
    public float calcAccuracy(Stats stats)
    {
        return (stats.getAcc() + (stats.getAgi() / 2));
    }

    @Override
    public float calcCritical(Stats stats)
    {
        return stats.getCrit();
    }

    @Override
    public int getScaling(Stats stats)
    {
        int scale = stats.getAgi() + stats.getDex();

        if (stats.getAgi() < 18 || stats.getDex() < 14)
            return scale / 2;
        else
            return scale;
    }

}
