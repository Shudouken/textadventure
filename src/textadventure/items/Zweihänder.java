package textadventure.items;

import textadventure.CP;
import textadventure.Stats;
import textadventure.Weapon;

public class Zweihänder extends Weapon
{

    public Zweihänder()
    {
        super("Zweihänder",
                "Firm sword made from durable steel, requires " + CP.getColorString("strength", CP.RED) + " to wield.",
                150);
    }

    @Override
    public int calcAttack(Stats stats)
    {
        return (int) (6 + (stats.getStr() * 1.25f));
    }

    @Override
    public float calcAccuracy(Stats stats)
    {
        if (stats.getStr() > 24)
            return stats.getAcc();
        else
            return (stats.getAcc() / 2f);
    }

    @Override
    public float calcCritical(Stats stats)
    {
        return stats.getCrit();
    }

    @Override
    public int getScaling(Stats stats)
    {
        if (stats.getStr() > 24)
            return stats.getStr();
        else
            return stats.getStr() / 2;
    }

}
