package textadventure.items;

import textadventure.CP;
import textadventure.Stats;
import textadventure.Weapon;

public class CursedDoll extends Weapon
{

    public CursedDoll()
    {
        super("Cursed Doll",
                "A cursed doll from ancient rituals, requires " + CP.getColorString("intelligence", CP.RED)
                        + " to wield." + System.getProperty("line.separator")
                        + "Drains 1% of max MP on every attack.",
                1500);
    }

    @Override
    public int calcAttack(Stats stats)
    {
        return (stats.getStr() + stats.getInt()) / 2;
    }

    @Override
    public float calcAccuracy(Stats stats)
    {
        return stats.getAcc();
    }

    @Override
    public float calcCritical(Stats stats)
    {
        return stats.getCrit();
    }

    @Override
    public int getScaling(Stats stats)
    {
        int drain = 1 + (stats.max_mp_ * 1 / 100);
        if (stats.mp_ >= drain)
        {
            stats.loseMP(drain);
            return (int) (stats.getInt() * 1.5f);
        } else
            return stats.getInt() / 3;
    }

}
