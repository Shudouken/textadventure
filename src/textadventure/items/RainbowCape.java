package textadventure.items;

import textadventure.Armor;
import textadventure.Stats;

public class RainbowCape extends Armor
{

    public RainbowCape()
    {
        super("Rainbow Cape",
                "Cape infused with Fire, Water, Wind and Thunder magic.",
                1000);
    }

    @Override
    public int calcDefense(Stats stats)
    {
        return stats.getVit();
    }

    @Override
    public int calcResistance(Stats stats)
    {
        return stats.getRes();
    }

    @Override
    public float calcEvasion(Stats stats)
    {
        return stats.getEva();
    }

    @Override
    public float getFireRes(Stats stats)
    {
        return stats.getFireRes() * 1.25f;
    }

    @Override
    public float getWaterRes(Stats stats)
    {
        return stats.getWaterRes() * 1.25f;
    }

    @Override
    public float getWindRes(Stats stats)
    {
        return stats.getWindRes() * 1.25f;
    }

    @Override
    public float getThunderRes(Stats stats)
    {
        return stats.getThunderRes() * 1.25f;
    }

}
