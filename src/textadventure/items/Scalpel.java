package textadventure.items;

import textadventure.CP;
import textadventure.Stats;
import textadventure.Weapon;

public class Scalpel extends Weapon
{

    public Scalpel()
    {
        super("Scalpel",
                "A weak weapon with a razor sharp edge, requires " + CP.getColorString("dexterity", CP.RED) + " to wield.",
                1900);
    }

    @Override
    public int calcAttack(Stats stats)
    {
        return stats.getDex();
    }

    @Override
    public float calcAccuracy(Stats stats)
    {
        return stats.getAcc() - 5;
    }

    @Override
    public float calcCritical(Stats stats)
    {
        if (stats.getDex() > 20)
            return stats.getCrit() + 15;
        else
            return stats.getCrit() / 2;
    }

    @Override
    public int getScaling(Stats stats)
    {
        return 30;
    }

}
