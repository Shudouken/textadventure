package textadventure.items;

import textadventure.Player;
import textadventure.UseableItem;

public class Ether extends UseableItem
{

    public Ether()
    {
        super("Ether",
                "Restores 30 MP",
                125);
    }

    @Override
    public void effect(Player player)
    {
        recoverMP(player, 30);
    }


}
