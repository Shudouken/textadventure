package textadventure.items;

import textadventure.CP;
import textadventure.Stats;
import textadventure.Weapon;

public class Scimitar extends Weapon
{

    public Scimitar()
    {
        super("Scimitar",
                "A swift but sturdy weapon, requires " + CP.getColorString("dexterity", CP.RED) + " and "
                        + CP.getColorString("strength", CP.RED) + " to wield.",
                150);
    }

    @Override
    public int calcAttack(Stats stats)
    {
        return (int) (stats.getStr() + (stats.getDex() * 1.5f));
    }

    @Override
    public float calcAccuracy(Stats stats)
    {
        return stats.getAcc() + 5;
    }

    @Override
    public float calcCritical(Stats stats)
    {
        return stats.getCrit();
    }

    @Override
    public int getScaling(Stats stats)
    {
        return stats.getDex();
    }

}
