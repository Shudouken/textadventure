package textadventure.items;

import textadventure.CP;
import textadventure.Stats;
import textadventure.Weapon;

public class IronShield extends Weapon
{

    public IronShield()
    {
        super("Iron Shield",
                "A shield used for bashing enemies, requires " + CP.getColorString("strength", CP.RED) + " to wield.",
                150);
    }

    @Override
    public int calcAttack(Stats stats)
    {
        return (int) (stats.getVit() + (stats.getStr() * 1.5f));
    }

    @Override
    public float calcAccuracy(Stats stats)
    {
        return Math.max(stats.getAcc() - 5, 0);
    }

    @Override
    public float calcCritical(Stats stats)
    {
        return stats.getCrit();
    }

    @Override
    public int getScaling(Stats stats)
    {
        return stats.getVit();
    }

}
