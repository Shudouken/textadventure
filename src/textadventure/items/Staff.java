package textadventure.items;

import textadventure.CP;
import textadventure.Stats;
import textadventure.Weapon;

public class Staff extends Weapon
{

    public Staff()
    {
        super("Staff",
                "A staff used for performing magical attacks, requires " + CP.getColorString("intelligence", CP.RED) + " to wield.",
                150);
    }

    @Override
    public int calcAttack(Stats stats)
    {
        return stats.getStr();
    }

    @Override
    public float calcAccuracy(Stats stats)
    {
        return stats.getAcc();
    }

    @Override
    public float calcCritical(Stats stats)
    {
        return (stats.getCrit() / 2);
    }

    @Override
    public int getScaling(Stats stats)
    {
        return (int) (stats.getInt() * 1.25f);
    }

}
