package textadventure.items;

import textadventure.CP;
import textadventure.Stats;
import textadventure.Weapon;

public class Hammer extends Weapon
{

    public Hammer()
    {
        super("Hammer",
                "A divine hammer, requires " + CP.getColorString("strength", CP.RED) + " and "
                        + CP.getColorString("intelligence", CP.RED) + " to wield.",
                2000);
    }

    @Override
    public int calcAttack(Stats stats)
    {
        if (stats.getInt() > 30)
            return (int) (stats.getInt() + (stats.getStr() * 2f));
        else
            return stats.getInt();
    }

    @Override
    public float calcAccuracy(Stats stats)
    {
        return stats.getAcc();
    }

    @Override
    public float calcCritical(Stats stats)
    {
        return stats.getCrit();
    }

    @Override
    public int getScaling(Stats stats)
    {
        return (stats.getStr() + stats.getInt()) / 2;
    }

}
