package textadventure.items;

import textadventure.Armor;
import textadventure.Stats;

public class Muffler extends Armor
{

    public Muffler()
    {
        super("Muffler",
                "Grants both physically and magically weak defenses, but makes up for it by allowing easier movement",
                300);
    }

    @Override
    public int calcDefense(Stats stats)
    {
        return (int) (2 + stats.getVit());
    }

    @Override
    public int calcResistance(Stats stats)
    {
        return (int) (2 + stats.getRes());
    }

    @Override
    public float calcEvasion(Stats stats)
    {
        return 5 + (stats.getEva() * 1.2f);
    }

}
