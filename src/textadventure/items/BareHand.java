package textadventure.items;

import textadventure.Stats;
import textadventure.Weapon;

public class BareHand extends Weapon
{

    public BareHand()
    {
        super("None",
                "Barehand attack",
                0);
    }

    @Override
    public int calcAttack(Stats stats)
    {
        return Math.max(stats.getStr() - 4, 0);
    }

    @Override
    public float calcAccuracy(Stats stats)
    {
        return stats.getAcc();
    }

    @Override
    public float calcCritical(Stats stats)
    {
        return stats.getCrit();
    }

    @Override
    public int getScaling(Stats stats)
    {
        return stats.getStr();
    }


}
