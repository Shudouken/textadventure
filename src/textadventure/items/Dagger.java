package textadventure.items;

import textadventure.CP;
import textadventure.Stats;
import textadventure.Weapon;

public class Dagger extends Weapon
{

    public Dagger()
    {
        super("Dagger",
                "A weak weapon, but its user can attack twice (requires " + CP.getColorString("agility", CP.RED) + ")",
                150);
        attack_twice_ = true;
    }

    @Override
    public int calcAttack(Stats stats)
    {
        return Math.max(stats.getStr() - 2, 0);
    }

    @Override
    public float calcAccuracy(Stats stats)
    {
        return (stats.getAcc() + (stats.getAgi() / 2));
    }

    @Override
    public float calcCritical(Stats stats)
    {
        return stats.getCrit();
    }

    @Override
    public int getScaling(Stats stats)
    {
        return stats.getStr() + (stats.getAgi() / 2);
    }

}
