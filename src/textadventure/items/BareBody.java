package textadventure.items;

import textadventure.Armor;
import textadventure.Stats;

public class BareBody extends Armor
{

    public BareBody()
    {
        super("None",
                "Undefended",
                0);
    }

    @Override
    public int calcDefense(Stats stats)
    {
        return (int) (stats.getVit() / 2f);
    }

    @Override
    public int calcResistance(Stats stats)
    {
        return (int) (stats.getRes() / 2f);
    }

    @Override
    public float calcEvasion(Stats stats)
    {
        return (stats.getEva() + 10);
    }


}
