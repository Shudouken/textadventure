package textadventure.items;

import textadventure.Player;
import textadventure.UseableItem;

public class Banana extends UseableItem
{

    public Banana()
    {
        super("Banana",
                "Rich in Manganese, recovers 100 HP",
                150);
    }

    @Override
    public void effect(Player player)
    {
        recoverHP(player, 100);
    }

}
