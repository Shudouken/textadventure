package textadventure.items;

import textadventure.Armor;
import textadventure.Stats;

public class Lingerie extends Armor
{

    public Lingerie()
    {
        super("Lingerie",
                "Expensive undergarments, makes you look your best for your special someone.",
                8000);
    }

    @Override
    public int calcDefense(Stats stats)
    {
        return stats.getVit();
    }

    @Override
    public int calcResistance(Stats stats)
    {
        return stats.getRes();
    }

    @Override
    public float calcEvasion(Stats stats)
    {
        return 3 + stats.getEva();
    }

}
