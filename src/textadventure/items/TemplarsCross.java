package textadventure.items;

import textadventure.CP;
import textadventure.Stats;
import textadventure.Weapon;

public class TemplarsCross extends Weapon
{

    public TemplarsCross()
    {
        super("Templar's Cross",
                "A cross shaped shield passed down between templars, requires " + CP.getColorString("vitality", CP.RED) + " to wield.",
                3500);
    }

    @Override
    public int calcAttack(Stats stats)
    {
        return (int) ((stats.getVit() * 1.5f) + stats.getStr());
    }

    @Override
    public float calcAccuracy(Stats stats)
    {
        return Math.max(stats.getAcc() - 5, 0);
    }

    @Override
    public float calcCritical(Stats stats)
    {
        return stats.getCrit();
    }

    @Override
    public int getScaling(Stats stats)
    {
        if (stats.getVit() > 25)
            return (int) (stats.getVit() * 1.25f);
        else
            return stats.getVit() / 2;
    }

}
