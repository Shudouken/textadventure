package textadventure.items;

import textadventure.CP;
import textadventure.Stats;
import textadventure.Weapon;

public class MorningStar extends Weapon
{

    public MorningStar()
    {
        super("Morning Star",
                "A blessed weapon for fighting evil, requires " + CP.getColorString("intelligence", CP.RED) + " and "
                        + CP.getColorString("strength", CP.RED) + " to wield.",
                150);
    }

    @Override
    public int calcAttack(Stats stats)
    {
        return (int) (stats.getInt() + (stats.getStr() * 1.5f));
    }

    @Override
    public float calcAccuracy(Stats stats)
    {
        return stats.getAcc();
    }

    @Override
    public float calcCritical(Stats stats)
    {
        return stats.getCrit();
    }

    @Override
    public int getScaling(Stats stats)
    {
        return (stats.getStr() + stats.getInt()) / 2;
    }

}
