package textadventure.items;

import textadventure.Player;
import textadventure.UseableItem;

public class SoulDrop extends UseableItem
{

    public SoulDrop()
    {
        super("Soul Drop",
                "Restores 100 MP",
                125);
    }

    @Override
    public void effect(Player player)
    {
        recoverMP(player, 100);
    }


}
