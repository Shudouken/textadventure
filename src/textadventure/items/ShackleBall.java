package textadventure.items;

import textadventure.CP;
import textadventure.Stats;
import textadventure.Weapon;

public class ShackleBall extends Weapon
{

    public ShackleBall()
    {
        super("Shackle Ball",
                "An iron ball tied to a shackle, requires " + CP.getColorString("strength", CP.RED) + " to wield.",
                1000);
    }

    @Override
    public int calcAttack(Stats stats)
    {
        return (int) (stats.getStr() * 2f);
    }

    @Override
    public float calcAccuracy(Stats stats)
    {
        return (stats.getAcc() / 1.25f);
    }

    @Override
    public float calcCritical(Stats stats)
    {
        return (stats.getCrit() / 1.25f);
    }

    @Override
    public int getScaling(Stats stats)
    {
        if (stats.getStr() > 40)
            return (int) (stats.getStr() * 1.5f);
        else
            return (int) (stats.getStr() / 1.25f);
    }

}
