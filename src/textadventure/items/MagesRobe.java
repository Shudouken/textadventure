package textadventure.items;

import textadventure.Armor;
import textadventure.Stats;

public class MagesRobe extends Armor
{

    public MagesRobe()
    {
        super("Mage's Robe",
                "Piece of cloth resistant against magical, but weak against physical attacks",
                300);
    }

    @Override
    public int calcDefense(Stats stats)
    {
        return (int) (1 + stats.getVit() / 2);
    }

    @Override
    public int calcResistance(Stats stats)
    {
        return (int) (8 + (stats.getRes() * 1.5f));
    }

    @Override
    public float calcEvasion(Stats stats)
    {
        return stats.getEva();
    }

}
