package textadventure;

import textadventure.tiles.EnemyTile;

import java.util.HashSet;

public class MonsterBook implements java.io.Serializable
{

    private HashSet<Enemy> book_;

    public MonsterBook()
    {
        book_ = new HashSet<>();
    }

    public void listAll()
    {
        if (book_.isEmpty())
        {
            System.out.println("Monsterbook is empty!");
            return;
        }

        int count = 1;
        for (Enemy e : book_)
        {
            System.out.print(e);
            if(count != book_.size())
                System.out.println();
            count++;
        }
    }

    public void getMonsterInfoStartingWith(String name)
    {
        if (!book_.isEmpty())
        {
            for (Enemy e : book_)
            {
                if (e.name_.toLowerCase().startsWith(name.toLowerCase()))
                {
                    System.out.print(e);
                    return;
                }
            }
            System.out.println("Monster starting with '" + name + "' not found!");
        }
        else
            System.out.println("Monsterbook is empty!");
    }

    public void getMonsterInfoStartingWith(String name, String level)
    {
        int monsterlevel = 0;
        try
        {
            monsterlevel = Integer.parseInt(level);
        } catch (Exception e)
        {}

        monsterlevel -= 1;

        if(monsterlevel < 0)
        {
            System.out.println(level + " is not valid!");
            return;
        }

        if (!book_.isEmpty())
        {
            for (Enemy e : book_)
            {
                if (e.name_.toLowerCase().startsWith(name.toLowerCase()))
                {
                    Enemy temp = EnemyTile.spawnEnemy(e.name_);
                    temp.levelUp(monsterlevel);
                    System.out.print(temp);
                    return;
                }
            }
            System.out.println("Monster starting with '" + name + "' not found!");
        }
        else
            System.out.println("Monsterbook is empty!");
    }

    public void add(Enemy enemy)
    {
        if(! book_.contains(enemy))
        {
            Enemy e = EnemyTile.spawnEnemy(enemy.name_);
            e.exp_ = e.base_exp_;
            book_.add(e);
        }
    }

}

