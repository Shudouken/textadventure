package textadventure;

public abstract class Armor extends Item
{

    public Armor(String name, String desc, int price)
    {
        super(name, desc, price);
    }

    public abstract int calcDefense(Stats stats);

    public abstract int calcResistance(Stats stats);

    public abstract float calcEvasion(Stats stats);

    public float getFireRes(Stats stats)
    {
        return stats.getFireRes();
    }

    public float getWaterRes(Stats stats)
    {
        return stats.getWaterRes();
    }

    public float getWindRes(Stats stats)
    {
        return stats.getWindRes();
    }

    public float getThunderRes(Stats stats)
    {
        return stats.getThunderRes();
    }

    public float getDarkRes(Stats stats)
    {
        return stats.getDarkRes();
    }

    public float getLightRes(Stats stats)
    {
        return stats.getLightRes();
    }

    public String toResistances(Stats stats)
    {
        String temp = "";
        String eol = System.getProperty("line.separator");

        int fire = (int) (getFireRes(stats) * 100);
        int water = (int) (getWaterRes(stats) * 100);
        int wind = (int) (getWindRes(stats) * 100);
        int thunder = (int) (getThunderRes(stats) * 100);
        int light = (int) (getLightRes(stats) * 100);
        int darkness = (int) (getDarkRes(stats) * 100);

        String fs = String.format("%3d", fire);
        fs = fire <= 90 ? CP.getColorString(fs, CP.RED) :
                fire >= 110 ? CP.getColorString(fs, CP.GREEN) : fs;
        String ws = String.format("%3d", water);
        ws = water <= 90 ? CP.getColorString(ws, CP.RED) :
                water >= 110 ? CP.getColorString(ws, CP.GREEN) : ws;
        String as = String.format("%3d", wind);
        as = wind <= 90 ? CP.getColorString(as, CP.RED) :
                wind >= 110 ? CP.getColorString(as, CP.GREEN) : as;
        String ts = String.format("%3d", thunder);
        ts = thunder <= 90 ? CP.getColorString(ts, CP.RED) :
                thunder >= 110 ? CP.getColorString(ts, CP.GREEN) : ts;
        String ls = String.format("%3d", light);
        ls = light <= 90 ? CP.getColorString(ls, CP.RED) :
                light >= 110 ? CP.getColorString(ls, CP.GREEN) : ls;
        String ds = String.format("%3d", darkness);
        ds = darkness <= 90 ? CP.getColorString(ds, CP.RED) :
                darkness >= 110 ? CP.getColorString(ds, CP.GREEN) : ds;

        temp += "     - Resistances -" + eol;
        temp += "Fire:  " + fs + "%\t";
        temp += "Water:    " + ws + "%" + eol;
        temp += "Wind:  " + as + "%\t";
        temp += "Thunder:  " + ts + "%" + eol;
        temp += "Light: " + ls + "%\t";
        temp += "Darkness: " + ds + "%" + eol;

        return temp;
    }

}
