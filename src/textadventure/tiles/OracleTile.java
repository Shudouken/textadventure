package textadventure.tiles;

import textadventure.*;

import java.lang.reflect.Method;
import java.util.*;

public class OracleTile extends Tile
{

    public OracleTile()
    {
        super("覡");
        interactable_ = true;
    }

    @Override
    public void interact()
    {
        System.out.println("I am the oracle. What can I tell you more about today?");
        System.out.println();
        System.out.println("1. Equipment");       //how good players weapon/armor is relative to the ones in inventory
        System.out.println("2. Growth");          //details on players level scaling
        System.out.println("3. Dormant Powers");  //when player learns next skill
        System.out.println("4. Leave");
        System.out.println();

        int num = 0;
        try
        {
            String hp = CP.getColorString("" + Game.getInstance().player_.stats_.hp_, CP.RED);
            String mp = CP.getColorString("" + Game.getInstance().player_.stats_.mp_, CP.BLUE);

            String prompt = "map(" + hp + "/" + mp + ")> ";
            Game.reader.flushConsole();
            System.out.print("\r");
            String input = Game.reader.readLine(prompt);
            num = Integer.parseInt(input);
        } catch (Exception e)
        {
            error();
        }

        if (num == 1)
        {
            inspectEquipment();
        } else if (num == 2)
        {
            inspectGrowth();
        } else if (num == 3)
        {
            inspectPowers();
        } else
            leave();
    }

    public void inspectEquipment()
    {
        Weapon w = Game.getInstance().player_.equip_.weapon_;
        Armor a = Game.getInstance().player_.equip_.armor_;

        HashMap<Item, Integer> in = Game.getInstance().player_.inventory_;
        ArrayList<Item> wl = getMatching(in, w);
        ArrayList<Item> al = getMatching(in, a);

        inspect();
        System.out.println("Out of all the equipment you currently possess:");
        Class[] cArg = new Class[1];
        cArg[0] = Stats.class;
        try
        {
            getEquipmentRank("weapon", "attack", w, wl, Weapon.class.getMethod("getScaledAtk", cArg));
            getEquipmentRank("weapon", "accuracy", w, wl, Weapon.class.getMethod("calcAccuracy", cArg));
            getEquipmentRank("weapon", "criticals", w, wl, Weapon.class.getMethod("calcCritical", cArg));

            getEquipmentRank("armor", "defense", a, al, Armor.class.getMethod("calcDefense", cArg));
            getEquipmentRank("armor", "resistance", a, al, Armor.class.getMethod("calcResistance", cArg));
            getEquipmentRank("armor", "evasion", a, al, Armor.class.getMethod("calcEvasion", cArg));
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void getEquipmentRank(String type, String attr, Item item, ArrayList<Item> list, Method method)
    {
        System.out.print("The " + type + " you are wielding is the number ");
        Stats stats = Game.getInstance().player_.stats_;

        if (list.isEmpty())
            CP.print("1", CP.GREEN);
        else
        {
            TreeMap<Float, Item> set = new TreeMap<>();
            set.put(getValue(item, stats, method), item);

            for (Item i : list)
                set.put(getValue(i, stats, method), i);

            System.out.print(getRank(set, item));
        }

        System.out.println(" for " + attr + "!");
    }

    public Float getValue(Item item, Stats stats, Method method)
    {
        Float value = 0.0f;

        try
        {
            if (method.invoke(item, stats) instanceof Integer)
                value = ((Integer) method.invoke(item, stats)).floatValue();
            else
                value = (Float) method.invoke(item, stats);
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return value;
    }

    public String getRank(TreeMap<Float, Item> set, Item o)
    {

        int pos = set.size();
        for (Item i : set.values())
        {
            //System.out.println("Check " + i.name_);
            if (i == o)
                return CP.getColorString(String.format("%d", pos), CP.GREEN);
            pos--;
        }
        return CP.getColorString(String.format("%d", pos + 1), CP.GREEN);
    }

    public ArrayList<Item> getMatching(HashMap<Item, Integer> in, Item item)
    {
        ArrayList<Item> list = new ArrayList<>();

        for (Map.Entry<Item, Integer> entry : in.entrySet())
        {
            if (entry.getKey().getClass().getSuperclass() == item.getClass().getSuperclass())
            {
                list.add(entry.getKey());
            }
        }

        return list;
    }

    public void inspectGrowth()
    {
        LevelScaling sc = Game.getInstance().player_.lvlscaling_;

        ArrayList<Integer> temp = new ArrayList<>();
        temp.add(sc.str_);
        temp.add(sc.vit_);
        temp.add(sc.int_);
        temp.add(sc.res_);
        temp.add(sc.dex_);
        temp.add(sc.agi_);

        Collections.sort(temp);
        int best = temp.get(5);
        int second = temp.get(4);
        int worst = temp.get(0);

        inspect();
        System.out.println("It seems you can expect the most growth in " + CP.getColorString(getStatName(sc, best), CP.GREEN) + " and "
                + CP.getColorString(getStatName(sc, second), CP.BLUE) + ".");
        System.out.println("I wouldn't bet on seeing any improvement in your " + CP.getColorString(getStatName(sc, worst), CP.RED) + ".");
    }

    public String getStatName(LevelScaling sc, int i)
    {
        if (sc.str_ == i)
            return "Strength";
        if (sc.vit_ == i)
            return "Vitality";
        if (sc.int_ == i)
            return "Intelligence";
        if (sc.res_ == i)
            return "Resistance";
        if (sc.dex_ == i)
            return "Dexterity";
        return "Agility";
    }

    public void inspectPowers()
    {
        SkillTree st = Game.getInstance().player_.skilltree_;
        int level = Game.getInstance().player_.level_;

        inspect();
        if (level == 99)
        {
            System.out.println("There are no powers lying dormant within you!");
            return;
        }

        int next = st.getNextSkillCount(level);
        if (next != 0)
            System.out.println("It seems your next power will awaken in " + CP.getColorString("" + next, CP.GREEN) + " levels");
        else
            System.out.println("There are no powers lying dormant within you!");
    }

    public void inspect()
    {
        System.out.println("Ah, let me see..");
        System.out.print("Abura Kadabura Simsala");
        for (int i = 0; i < 3; i++)
        {
            try
            {
                Thread.sleep(300);
                System.out.print(".");
            } catch (InterruptedException e)
            {
                //Handle exception
            }
        }
        System.out.println(" BIM!");
        System.out.println();
    }

    private void error()
    {
        try
        {
            Game.reader.clearScreen();
            Game.reader.flushConsole();
        } catch (Exception e)
        {
        }
        System.out.print("\r");
        System.out.println("Please see me again when you've made up your mind.");
    }

    private void leave()
    {
        try
        {
            Game.reader.clearScreen();
            Game.reader.flushConsole();
        } catch (Exception e)
        {
        }
        System.out.print("\r");
        System.out.println("See me again whenever you wish to know more.");
    }
}
