package textadventure.tiles;

import textadventure.Game;
import textadventure.Tile;

public class ResetTile extends Tile
{

    public ResetTile()
    {
        super("石");
        interactable_ = true;
    }

    @Override
    public void collide()
    {
        System.out.println("You touched the holy stone...");
        System.out.println("The outside world has been reset!");
        resetMaps();
    }

    public static void resetMaps()
    {
        Game.getInstance().map_ = Game.getInstance().town_;
        Game.getInstance().pt_ = Game.getInstance().town_pt_;
        Game.getInstance().town_gate_.symbol_ = "扉";
        Game.getInstance().town_gate_.next_map_ = null;
        Game.getInstance().town_gate_.next_pt_ = null;
        Game.getInstance().depth_ = 0;
    }

}
