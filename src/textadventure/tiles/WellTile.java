package textadventure.tiles;

import textadventure.Game;
import textadventure.Tile;

public class WellTile extends Tile
{

    public WellTile()
    {
        super("井");
        interactable_ = true;
    }

    @Override
    public void collide()
    {
        System.out.println(Game.getInstance().player_.name_ + " took a sip of the town's well water. HP/MP fully restored!");
        Game.getInstance().player_.stats_.hp_ = Game.getInstance().player_.stats_.max_hp_;
        Game.getInstance().player_.stats_.mp_ = Game.getInstance().player_.stats_.max_mp_;
    }

}
