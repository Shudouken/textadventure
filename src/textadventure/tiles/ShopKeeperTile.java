package textadventure.tiles;

import textadventure.CP;
import textadventure.Game;
import textadventure.Item;
import textadventure.Tile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ShopKeeperTile extends Tile
{

    public ShopKeeperTile()
    {
        super("店");
        interactable_ = true;
    }

    @Override
    public void interact()
    {
        System.out.println("Oh hey there jolly mate, what can I do for you?");
        System.out.println();
        System.out.println("1. Buy Items");
        System.out.println("2. Sell Items");
        System.out.println("3. Leave");
        System.out.println();

        int num = 0;
        try
        {
            String prompt = CP.getColorString("shekels(" + Game.getInstance().player_.shekels_ + ")> ", CP.BOLD);
            Game.reader.flushConsole();
            System.out.print("\r");
            String input = Game.reader.readLine(prompt);
            num = Integer.parseInt(input);
        } catch (Exception e)
        {
            error();
        }

        if (num == 1)
        {
            System.out.println("What would you like to purchase?");
            System.out.println();
            listBuyItems();
        } else if (num == 2)
        {
            System.out.println("What would you like to sell?");
            System.out.println();
            listSellItems();
        } else
            leave();
    }

    public void listBuyItems()
    {
        ArrayList<Item> ki = new ArrayList<Item>(Game.getInstance().player_.known_items_);
        int number = 1;

        for (Item entry : ki)
        {
            System.out.println(number + ". " + entry.name_ + " (" + entry.price_ + " shekels)");
            System.out.println(entry.desc_);
            System.out.println();
            number++;
        }
        String input = "";

        try
        {
            String prompt = CP.getColorString("shekels(" + Game.getInstance().player_.shekels_ + ")> ", CP.BOLD);
            Game.reader.flushConsole();
            System.out.print("\r");
            input = Game.reader.readLine(prompt);
            String params[] = input.split(" (?=(([^\"]*[\"]){2})*[^\"]*$)");
            int num = Integer.parseInt(params[0]);

            int amount = 1;
            if (params.length == 2)
                amount = Integer.parseInt(params[1]);

            Item item = getItem(ki, num - 1);

            if (item != null && amount > 0)
                buyItem(item, amount);
            else
                error();

        } catch (Exception e)
        {
            error();
        }
    }

    public void listSellItems()
    {
        HashMap<Item, Integer> in = new HashMap<Item, Integer>(Game.getInstance().player_.inventory_);
        if (in.isEmpty())
        {
            empty();
            return;
        }

        int number = 1;

        for (Map.Entry<Item, Integer> entry : in.entrySet())
        {
            System.out.println(number + ". " + entry.getKey().name_ + " [x" + entry.getValue() + "] " + " (" + entry.getKey().price_ / 2 + " shekels)");
            System.out.println(entry.getKey().desc_);
            System.out.println();
            number++;
        }
        String input = "";

        try
        {
            String prompt = CP.getColorString("shekels(" + Game.getInstance().player_.shekels_ + ")> ", CP.BOLD);
            Game.reader.flushConsole();
            System.out.print("\r");
            input = Game.reader.readLine(prompt);
            String params[] = input.split(" (?=(([^\"]*[\"]){2})*[^\"]*$)");
            int num = Integer.parseInt(params[0]);

            int amount = 1;
            if (params.length == 2)
                amount = Integer.parseInt(params[1]);

            Map.Entry<Item, Integer> entry = getItem(in, num - 1);

            if (entry != null && amount > 0)
                sellItem(entry, amount);
            else
                error();

        } catch (Exception e)
        {
            error();
        }
    }

    public Item getItem(ArrayList<Item> ki, int num)
    {
        Item item = ki.get(num);
        return item;
    }

    public Map.Entry<Item, Integer> getItem(HashMap<Item, Integer> in, int num)
    {
        int i = 0;
        for (Map.Entry<Item, Integer> entry : in.entrySet())
        {
            if (num == i)
                return entry;
            i++;
        }

        return null;
    }

    private void error()
    {
        try
        {
            Game.reader.clearScreen();
            Game.reader.flushConsole();
        } catch (Exception e)
        {
        }
        System.out.print("\r");
        System.out.println("I don't understand a bloody word coming from your mouth!");
    }

    private void leave()
    {
        try
        {
            Game.reader.clearScreen();
            Game.reader.flushConsole();
        } catch (Exception e)
        {
        }
        System.out.print("\r");
        System.out.println("Hope to make business with you soon!");
    }

    private void empty()
    {
        try
        {
            Game.reader.clearScreen();
            Game.reader.flushConsole();
        } catch (Exception e)
        {
        }
        System.out.print("\r");
        System.out.println("You got nothing to sell mate! Are you shittin' me?");
    }

    private void noShekels()
    {
        try
        {
            Game.reader.clearScreen();
            Game.reader.flushConsole();
        } catch (Exception e)
        {
        }
        System.out.print("\r");
        System.out.println("You got not enough shekels mate! Are you shittin' me?");
    }

    public void buyItem(Item item, int amount)
    {
        try
        {
            Game.reader.clearScreen();
            Game.reader.flushConsole();
        } catch (Exception e)
        {
        }
        System.out.print("\r");
        if (Game.getInstance().player_.shekels_ >= (item.price_ * amount))
        {
            Game.getInstance().player_.shekels_ -= (item.price_ * amount);
            for (int i = 0; i < amount; i++)
                Game.getInstance().player_.addItemToInventory(item);
            System.out.println(Game.getInstance().player_.name_ + " bought " + amount
                    + " " + item.name_ + "(s) for " + (item.price_ * amount) + " shekels.");
        } else
        {
            amount = Game.getInstance().player_.shekels_ / item.price_;
            if (amount > 0)
                buyItem(item, amount);
            else
                noShekels();
        }
    }

    public void sellItem(Map.Entry<Item, Integer> item, int amount)
    {
        try
        {
            Game.reader.clearScreen();
            Game.reader.flushConsole();
        } catch (Exception e)
        {
        }
        System.out.print("\r");

        if (amount > item.getValue())
            amount = item.getValue();


        for (int i = 0; i < amount; i++)
        {
            Game.getInstance().player_.shekels_ += (item.getKey().price_ / 2);
            Game.getInstance().player_.removeItemFromInventory(item.getKey());
        }

        System.out.println(Game.getInstance().player_.name_ + " sold " + amount
                + " " + item.getKey().name_ + "(s) for " + (item.getKey().price_ / 2 * amount) + " shekels.");

    }

}
