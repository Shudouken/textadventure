package textadventure.tiles;

import textadventure.*;

import java.util.ArrayList;

public class MerchantTile extends Tile
{

    private float mood_;

    private static final ArrayList<Item> stock_;

    static
    {
        stock_ = new ArrayList<>();

        stock_.add(Item.combat_knife_);
        stock_.add(Item.cursed_doll_);
        stock_.add(Item.hammer_);
        stock_.add(Item.scalpel_);
        stock_.add(Item.shackle_ball_);
        stock_.add(Item.templars_cross_);
        stock_.add(Item.swimwear_);
    }

    public MerchantTile()
    {
        super("商");
        interactable_ = true;

        mood_ = (Randomizer.randInt(80, 150) / 100.0f);
    }


    @Override
    public void interact()
    {
        System.out.println("Got some good things on sale, stranger!");
        System.out.println();
        System.out.println("1. Buy Items");
        System.out.println("2. Leave");
        System.out.println();

        int num = 0;
        try
        {
            String prompt = CP.getColorString("shekels(" + Game.getInstance().player_.shekels_ + ")> ", CP.BOLD);
            Game.reader.flushConsole();
            System.out.print("\r");
            String input = Game.reader.readLine(prompt);
            num = Integer.parseInt(input);
        } catch (Exception e)
        {
            error();
        }

        if (num == 1)
        {
            System.out.println("What are you buying?");
            System.out.println();
            listBuyItems();
        } else
            leave();
    }

    public void listBuyItems()
    {
        int number = 1;

        for (Item entry : stock_)
        {
            System.out.println(number + ". " + entry.name_ + " (" + (int) (entry.price_ * mood_) + " shekels)");
            System.out.println(entry.desc_);
            System.out.println();
            number++;
        }
        String input = "";

        try
        {
            String prompt = CP.getColorString("shekels(" + Game.getInstance().player_.shekels_ + ")> ", CP.BOLD);
            Game.reader.flushConsole();
            System.out.print("\r");
            input = Game.reader.readLine(prompt);
            String params[] = input.split(" (?=(([^\"]*[\"]){2})*[^\"]*$)");
            int num = Integer.parseInt(params[0]);

            int amount = 1;
            if (params.length == 2)
                amount = Integer.parseInt(params[1]);

            Item item = getItem(stock_, num - 1);

            if (item != null && amount > 0)
                buyItem(item, amount);
            else
                error();

        } catch (Exception e)
        {
            error();
        }
    }

    public Item getItem(ArrayList<Item> ki, int num)
    {
        Item item = ki.get(num);
        return item;
    }

    private void error()
    {
        try
        {
            Game.reader.clearScreen();
            Game.reader.flushConsole();
        } catch (Exception e)
        {
        }
        System.out.print("\r");
        System.out.println("I don't understand!");
    }

    private void leave()
    {
        try
        {
            Game.reader.clearScreen();
            Game.reader.flushConsole();
        } catch (Exception e)
        {
        }
        System.out.print("\r");
        System.out.println("Come back any time!");
    }

    private void noShekels()
    {
        try
        {
            Game.reader.clearScreen();
            Game.reader.flushConsole();
        } catch (Exception e)
        {
        }
        System.out.print("\r");
        System.out.println("Not enough cash!");
    }

    public void buyItem(Item item, int amount)
    {
        try
        {
            Game.reader.clearScreen();
            Game.reader.flushConsole();
        } catch (Exception e)
        {
        }
        System.out.print("\r");
        if (Game.getInstance().player_.shekels_ >= (int) (item.price_ * mood_ * amount))
        {
            Game.getInstance().player_.shekels_ -= (int) (item.price_ * mood_ * amount);
            for (int i = 0; i < amount; i++)
                Game.getInstance().player_.addItemToInventory(item);
            System.out.println(Game.getInstance().player_.name_ + " bought " + amount
                    + " " + item.name_ + "(s) for " + (int) (item.price_ * mood_ * amount) + " shekels.");
        } else
        {
            amount = (int) (Game.getInstance().player_.shekels_ / (item.price_ * mood_));
            if (amount > 0)
                buyItem(item, amount);
            else
                noShekels();
        }
    }

}
