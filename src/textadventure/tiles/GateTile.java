package textadventure.tiles;

import textadventure.Game;
import textadventure.Item;
import textadventure.Location;
import textadventure.Position;
import textadventure.Randomizer;
import textadventure.Tile;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class GateTile extends Tile
{

    public Position pos_ = null;
    public Location next_map_ = null;
    public PlayerTile next_pt_ = null;
    public int depth_ = 0;

    public GateTile(Position pos)
    {
        super("扉");
        interactable_ = true;
        pos_ = pos;
    }

    @Override
    public void collide()
    {
        if (next_map_ == null && next_pt_ == null)
            generateMap();
        else
            loadMap();
    }

    public void generateMap()
    {
        int old_w = Game.getInstance().map_.w_ - 1;
        int old_h = Game.getInstance().map_.h_ - 1;
        int w = Randomizer.randInt(5, 12);
        int h = Randomizer.randInt(5, 12);
        next_map_ = new Location(w, h);

        //set connecting gate
        int x = pos_.x_ == 0 ? w - 1 : pos_.x_ == old_w ? 0 : w / 2;
        int y = pos_.y_ == 0 ? h - 1 : pos_.y_ == old_h ? 0 : h / 2;
        GateTile gt = new GateTile(new Position(x, y));
        gt.symbol_ = "開";
        gt.next_map_ = Game.getInstance().map_;
        next_map_.addTile(gt.pos_, gt);

        //set player
        int px = gt.pos_.x_ == 0 ? 1 : gt.pos_.x_ == w - 1 ? w - 2 : w / 2;
        int py = gt.pos_.y_ == 0 ? 1 : gt.pos_.y_ == h - 1 ? h - 2 : h / 2;
        next_pt_ = new PlayerTile(new Position(px, py));
        gt.next_pt_ = Game.getInstance().pt_;
        next_map_.addTile(next_pt_.pos_, next_pt_);

        //set enemies
        int num_enemies = Randomizer.randInt(1, 1 + depth_ / 10);
        for (int i = 0; i < num_enemies; i++)
            generateRandomEnemy();

        //set loot
        generateRandomTreasure();

        generateRandomMerchant();

        //set soul
        generateRandomSoul();

        setCornerBlocks();

        //set additional gates
        int num_gates = Randomizer.randInt(1, 3);
        for (int i = 0; i < num_gates; i++)
            generateGates(gt);

        if (depth_ > Game.getInstance().depth_)
            Game.getInstance().depth_ = depth_;

        loadMap();
    }

    public void generateRandomEnemy()
    {
        //TODO: get enemy based on players level and distance
        int monsterlevel = Game.getInstance().player_.level_;
        monsterlevel += depth_ / 3;
        EnemyTile enemy = null;

        if (depth_ <= 15)
            enemy = getFieldEnemy(monsterlevel);
        if (depth_ > 15)
            enemy = getForestEnemy(monsterlevel);
        /*if(depth_ > 30)
            symbol = "沼";
        if(depth_ > 50)
            symbol = "墓";
        if(depth_ > 75)
            symbol = "淵";*/

        next_map_.addRandomEnemyTile(enemy);
    }

    public EnemyTile getFieldEnemy(int monsterlevel)
    {
        EnemyTile enemy = new EnemyTile("Zombie");

        int rand = Randomizer.randInt(0, 100);
        if (rand <= 25)
            enemy = new EnemyTile("Imp");
        else if (rand <= 50)
            enemy = new EnemyTile("Shaman");
        else if (rand <= 75)
            enemy = new EnemyTile("Thief");

        monsterlevel = (int) (monsterlevel * 0.75f);
        enemy.monsterlevel_ = (int) monsterlevel == 0 ? 1 : monsterlevel;
        ;
        return enemy;
    }

    public EnemyTile getForestEnemy(int monsterlevel)
    {
        EnemyTile enemy = new EnemyTile("Monkey");

        int rand = Randomizer.randInt(0, 100);
        if (rand <= 25)
            enemy = new EnemyTile("Lizardman");
        else if (rand <= 50)
            enemy = new EnemyTile("Priest");
        else if (rand <= 75)
            enemy = new EnemyTile("Wizard");

        enemy.monsterlevel_ = monsterlevel;
        return enemy;
    }

    //succubus - incubus with charm

    //specter - ghost that can't be hurt with normal attacks

    //surgeon that drugs himself

    public void generateRandomTreasure()
    {
        if (Randomizer.randInt(0, 100 - min(25, depth_ / 3)) <= 5)
            next_map_.addRandomTile(new TreasureTile(Item.getRandomItem()));
    }

    public void generateRandomMerchant()
    {
        if (Randomizer.randInt(0, 100 - min(5, depth_ / 15)) <= 5)
            next_map_.addRandomTile(new MerchantTile());
    }

    public void generateRandomSoul()
    {
        if (Randomizer.randInt(0 + max(7, depth_ / 6), 100 + max(7, depth_ / 6)) <= 15)
        {
            SoulTile st = new SoulTile(null);
            st.pos_ = next_map_.addRandomTile(st);
        }
    }

    public void generateGates(GateTile gt)
    {
        int w, h;
        if (Randomizer.randInt(0, 1) == 0)      //Gate on left/right side
        {
            h = next_map_.h_ / 2;
            if (Randomizer.randInt(0, 1) == 0)  //Gate left
                w = 0;
            else                                //Gate right
                w = next_map_.w_ - 1;
        } else                                  //Gate on top/bottom side
        {
            w = next_map_.w_ / 2;
            if (Randomizer.randInt(0, 1) == 0)  //Gate top
                h = 0;
            else                                //Gate bottom
                h = next_map_.h_ - 1;
        }

        Position gatepos = new Position(w, h);

        if (gatepos.equals(gt.pos_))
            generateGates(gt);
        else
        {
            GateTile gate = new GateTile(gatepos);
            gate.depth_ = this.depth_ + 1;
            next_map_.addTile(gate.pos_, gate);
        }

    }

    public void setCornerBlocks()
    {
        String symbol = "畑";
        if (depth_ <= 15)
            return;
        if (depth_ > 15)
            symbol = "森";
        if (depth_ > 30)
            symbol = "沼";
        if (depth_ > 50)
            symbol = "墓";
        if (depth_ > 75)
            symbol = "淵";

        next_map_.setCornerStones(symbol);
    }

    public void loadMap()
    {
        this.symbol_ = "開";
        Game.getInstance().map_ = next_map_;
        Game.getInstance().pt_ = next_pt_;

        if (Game.getInstance().player_.stealth_)
            Game.getInstance().pt_.symbol_ = "忍";
        else
            Game.getInstance().pt_.symbol_ = "私";
    }

}
