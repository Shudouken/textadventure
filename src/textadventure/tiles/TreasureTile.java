package textadventure.tiles;

import textadventure.Game;
import textadventure.Item;
import textadventure.Tile;

public class TreasureTile extends Tile
{

    public boolean closed_ = true;
    public Item drop_;

    public TreasureTile(Item drop)
    {
        super("宝");
        interactable_ = true;
        drop_ = drop;
    }

    @Override
    public void collide()
    {
        if (closed_)
        {
            this.symbol_ = "無";
            System.out.println(Game.getInstance().player_.name_ + " opened the treasure chest and found a " + drop_.name_);
            Game.getInstance().player_.addItemToInventory(drop_);
            closed_ = false;
        } else
        {
            System.out.println("Treasure chest is empty.");
        }


    }

}
