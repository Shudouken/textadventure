package textadventure.tiles;

import textadventure.*;

public class WitchTile extends Tile
{

    private static final String name_ = "Emilia";
    private boolean introduction_ = false;
    public Position pos_;

    public WitchTile(Position pos)
    {
        super("魔");
        pos_ = pos;
        interactable_ = true;
    }

    @Override
    public void interact()
    {
        if (! introduction_)
            firstIntroduction();
        else
            secondTalk();
    }

    public void firstIntroduction()
    {
        System.out.println("Oh, a new face in town! Let me introduce myself, I am " + name_ + ".");
        System.out.println("I might not look like it, but I'm actually the only one is this town");
        System.out.println("who is able to use magic! Isn't that great? What? You want to learn about");
        System.out.println("magic too?");
        pause();
        System.out.println("Hah! Don't make laugh! Only a few people can count themselves");
        System.out.println("lucky enough to even sense magic, let alone use it!");
        pause();
        System.out.println("Hmm.. Let me see..");
        pause();
        System.out.println("What? How can this..? N-not bad, it seems you have what it takes..");
        System.out.println("I guess some of my magical talent has caused this awakening in you!");
        pause();
        System.out.println("I'll give you one of my old tomes, it has to be around here somewhere..");
        pause();
        System.out.println("Found it! With this you'll be able to make your own notes about the");
        System.out.println("magic phrases you encounter in this world. I've gone ahead and filled");
        System.out.println("some in for you, but you can change them at anytime yourself through");
        System.out.println("the " + CP.getColorString("tome",CP.PURPLE) + " command");
        pause();
        System.out.println("Huh? You're unsure how to cast magic? Well aren't you a dummy!");
        System.out.println("You just have to chain the keywords together to form a spell!");
        System.out.println("Isn't it obvious?");
        pause();
        System.out.println("Still confused? Just use the " + CP.getColorString("cast",CP.BLUE)  + " command and give it a try!");
        System.out.println("Type the first letter of a keyword to add it to the chain.");
        System.out.println("Delete words from the chain with 'Backspace' and finalize spells with 'Enter'");
        pause();

        introduction_ = true;
        Casting.putToTome("Eldur ", "That feeling you get when eating spicy food!");
        Casting.putToTome("Liv "  , "The throb-throb in your chest");
        Casting.putToTome("Volat ", "When you want to throw it away!");
        Casting.putToTome("Obt "  , "Sometimes you want to keep it all to yourself..");
        Casting.putToTome("Port " , "Moves stuff to an other location");
        Casting.putToTome("Dom "  , "This is you!");

        System.out.println("Oh, you want to pass through here? I'll move all the way over to the left.");
        pause();

        Game.getInstance().town_.removeTile(pos_);
        pos_ = new Position(1,1);
        Game.getInstance().town_.addTile(pos_, this);

        leave();
    }

    public void secondTalk()
    {
        System.out.println("If it isn't " + Game.getInstance().player_.name_ + "! What brings you here?");

        System.out.println();
        System.out.println("1. Chat");
        System.out.println("2. Explain Magic");
        //battle
        System.out.println("3. Leave");

        int num = 0;
        try
        {
            Game.reader.flushConsole();
            System.out.print("\r");
            String input = Game.reader.readLine(Game.getInstance().prompt_);
            num = Integer.parseInt(input);
        } catch (Exception e)
        {
            leave("You gotta speak English with me!");
        }

        if (num == 1)
        {
            chat();
        } else if (num == 2)
        {
            explainMagic();
        } else
            leave();
    }

    private final static String quotes[];
    static
    {
        quotes = new String[6];

        quotes[0] = "The other night, I dreamt all of my magic powers were gone. Horrible!";
        quotes[1] = "The oracle told me my magic was the best in town!";
        quotes[2] = "We should try having a magic duel one day when you're stronger..";
        quotes[3] = "I use magic for everything, even for getting dressed!" + CP.EOL + name_ +
                    ": Huh? My socks are inside out? How did that happen?";
        quotes[4] = "Sometimes I think my wand acts on its own, must just be my imagination.";
        quotes[5] = "When you want to refresh the basics of magic I'm here for you!";
    }

    public void chat()
    {
        leave(quotes[Randomizer.randInt(0,5)]);
    }

    public void explainMagic()
    {
        System.out.println("You still have that tome I gave you? It should contain all the magical");
        System.out.println("keywords known to you.");
        System.out.println("To view them use the " + CP.getColorString("tome",CP.PURPLE) + " command");
        pause();
        System.out.println("To actually cast spells you have to build a chain of at least two keywords.");
        System.out.println("This can be done by using the " + CP.getColorString("cast",CP.BLUE)  + " command.");
        System.out.println("In there, type the first letter of a keyword to add it to the chain.");
        System.out.println("Delete words from the chain with 'Backspace' and finalize spells with 'Enter'");

        pause();
        leave();
    }

    public void pause()
    {
        System.out.print(" ▼");
        try
        {
            System.in.read();
        }
        catch(Exception e)
        {}
        System.out.print("\r");
        System.out.println("  ");
    }

    private void leave(String msg)
    {
        try
        {
            Game.reader.clearScreen();
            Game.reader.flushConsole();
        } catch (Exception e)
        {
        }
        System.out.print("\r");
        System.out.println(name_ + ": " + msg);
    }

    private void leave()
    {
        try
        {
            Game.reader.clearScreen();
            Game.reader.flushConsole();
        } catch (Exception e)
        {
        }
        System.out.print("\r");
        System.out.println(name_ + ": Don't forget to check your tome!");
    }

}
