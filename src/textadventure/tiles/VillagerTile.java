package textadventure.tiles;

import textadventure.Tile;

public class VillagerTile extends Tile
{

    public VillagerTile()
    {
        super("老");
        interactable_ = true;
    }

    @Override
    public void collide()
    {
        System.out.println("Ah, it is nice to see fresh life in this village!");
        System.out.println("The outside world is too dangerous for an old man");
        System.out.println("like me...");
    }

}
