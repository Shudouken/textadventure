package textadventure.tiles;

import textadventure.*;
import textadventure.skills.Cloaking;

import java.util.ArrayList;

public class PlayerTile extends Tile
{

    public Position pos_;

    public static enum Direction
    {
        LEFT, RIGHT, UP, DOWN
    }

    public PlayerTile(Position pos)
    {
        super("私");
        pos_ = pos;
    }

    public void move(Location map, Direction dir)
    {
        if (map.objects_.containsKey(pos_))
        {
            Position move = new Position(pos_);

            if (dir == Direction.LEFT)
                move.x_ -= 1;
            else if (dir == Direction.RIGHT)
                move.x_ += 1;
            else if (dir == Direction.UP)
                move.y_ -= 1;
            else
                move.y_ += 1;

            if (map.objects_.containsKey(move))
            {
                Tile tile = map.objects_.get(move);
                if (tile.interactable_)
                    tile.collide();

                if (tile instanceof GateTile)
                    return;
            } else
            {
                map.removeTile(pos_);
                map.addTile(move, this);
                pos_ = move;
            }

            if (map.enemies_ != null && !Game.getInstance().player_.stealth_)
            {

                ArrayList<EnemyTile> copy = new ArrayList<EnemyTile>(map.enemies_);
                for (EnemyTile enemy : copy)
                    enemy.move(map, pos_);
            }

            if (Game.getInstance().player_.stealth_)
                Cloaking.consumeMP();

            if (Game.getInstance().player_.stats_.getClass() == Buff.class)
            {
                Game.getInstance().player_.stats_ = ((Buff) Game.getInstance().player_.stats_).countDown();
            }
        }
    }

}
