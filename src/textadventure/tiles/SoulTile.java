package textadventure.tiles;

import textadventure.Buff;
import textadventure.Game;
import textadventure.Position;
import textadventure.Randomizer;
import textadventure.Tile;

public class SoulTile extends Tile
{

    public Position pos_;

    public SoulTile(Position pos)
    {
        super("魂");
        interactable_ = true;
        pos_ = pos;
    }

    @Override
    public void collide()
    {
        System.out.println(Game.getInstance().player_.name_ + " consumed the lingering soul. HP/MP fully restored!");
        Game.getInstance().player_.stats_.hp_ = Game.getInstance().player_.stats_.max_hp_;
        Game.getInstance().player_.stats_.mp_ = Game.getInstance().player_.stats_.max_mp_;

        getRandomBuff();
        Game.getInstance().map_.removeTile(pos_);
    }

    public void getRandomBuff()
    {
        int rand = Randomizer.randInt(1, 6);

        if (rand == 1)
            Game.getInstance().player_.buff("Str");
        else if (rand == 2)
            Game.getInstance().player_.buff("Vit");
        else if (rand == 3)
            Game.getInstance().player_.buff("Int");
        else if (rand == 4)
            Game.getInstance().player_.buff("Res");
        else if (rand == 5)
            Game.getInstance().player_.buff("Dex");
        else
            Game.getInstance().player_.buff("Agi");

        if (Game.getInstance().player_.stats_ instanceof Buff)
            ((Buff) Game.getInstance().player_.stats_).turns_active_ = 15;
    }

}
