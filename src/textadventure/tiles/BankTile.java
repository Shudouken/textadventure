package textadventure.tiles;

import textadventure.CP;
import textadventure.Game;
import textadventure.Item;
import textadventure.Tile;

import java.util.HashMap;
import java.util.Map;

public class BankTile extends Tile
{

    private float funds_;
    private HashMap<Item, Integer> storage_;

    public BankTile()
    {
        super("貯");
        interactable_ = true;
        funds_ = 0;
        storage_ = new HashMap<>();
    }

    @Override
    public void interact()
    {
        System.out.println("Private Storage - Shekels: " + (int) funds_ + " Items: " + getStorageSize());
        System.out.println();
        System.out.println("1. Deposit Shekels\t5. Deposit All Shekels");
        System.out.println("2. Withdraw Shekels\t6. Withdraw All Shekels");
        System.out.println("3. Deposit Items\t7. Deposit All Items");
        System.out.println("4. Withdraw Items\t8. Withdraw All Items");
        System.out.println("                  9. Leave");
        System.out.println();

        int num = 0;
        try
        {
            String prompt = CP.getColorString("storage(" + (int) funds_ + ")> ", CP.YELLOW);
            Game.reader.flushConsole();
            System.out.print("\r");
            String input = Game.reader.readLine(prompt);
            num = Integer.parseInt(input);
        } catch (Exception e)
        {
            error();
        }

        if (num == 1)
        {
            System.out.println("How much shekels would you like to deposit? (" + Game.getInstance().player_.shekels_ + ")");
            System.out.println();
            depositMoney(false);
        } else if (num == 2)
        {
            System.out.println("How much shekels would you like to withdraw? (" + (int) funds_ + ")");
            System.out.println();
            withdrawMoney(false);
        } else if (num == 3)
        {
            System.out.println("What item would you like to deposit?");
            System.out.println();
            depositItem(false);
        } else if (num == 4)
        {
            System.out.println("What item would you like to withdraw?");
            System.out.println();
            BankTile.this.withdrawItem(false);
        } else if (num == 5)
        {
            depositMoney(true);
        } else if (num == 6)
        {
            withdrawMoney(true);
        } else if (num == 7)
        {
            depositItem(true);
        } else if (num == 8)
        {
            BankTile.this.withdrawItem(true);
        } else
            leave();
    }

    public void depositMoney(boolean all)
    {
        int num = 0;
        if (all)
            num = Game.getInstance().player_.shekels_;
        else
        {
            try
            {
                String prompt = CP.getColorString("storage(" + (int) funds_ + ")> ", CP.YELLOW);
                Game.reader.flushConsole();
                System.out.print("\r");
                String input = Game.reader.readLine(prompt);
                num = Integer.parseInt(input);
            } catch (Exception e)
            {
                error();
            }
        }

        if (num < 1)
        {
            msg("Cannot deposit " + num + " shekels!");
            return;
        }

        if (num > Game.getInstance().player_.shekels_)
            num = Game.getInstance().player_.shekels_;

        funds_ += num;
        Game.getInstance().player_.shekels_ -= num;
        msg(Game.getInstance().player_.name_ + " deposited " + num + " shekels. Total funds: " + (int) funds_);
    }


    public void withdrawMoney(boolean all)
    {
        int num = 0;
        if (all)
            num = (int) funds_;
        else
        {
            try
            {
                String prompt = CP.getColorString("storage(" + (int) funds_ + ")> ", CP.YELLOW);
                Game.reader.flushConsole();
                System.out.print("\r");
                String input = Game.reader.readLine(prompt);
                num = Integer.parseInt(input);
            } catch (Exception e)
            {
                error();
            }
        }

        if (num < 1)
        {
            msg("Cannot withdraw " + num + " shekels!");
            return;
        }

        if (num > funds_)
            num = (int) funds_;

        funds_ -= num;
        Game.getInstance().player_.shekels_ += num;
        msg(Game.getInstance().player_.name_ + " withdrew " + num + " shekels. Total funds: " + (int) funds_);
    }

    public void applyInterest()
    {
        float interest = 1 + (0.001f * (Game.getInstance().depth_+1));
        //System.out.println("Depth: " + Game.getInstance().depth_ + " - Interest rate: " + interest);
        funds_ *= interest;
    }

    public void depositItem(boolean all)
    {
        HashMap<Item, Integer> in = new HashMap<Item, Integer>(Game.getInstance().player_.inventory_);
        if (in.isEmpty())
        {
            empty();
            return;
        }

        if (!all)
        {
            int number = 1;
            for (Map.Entry<Item, Integer> entry : in.entrySet())
            {
                System.out.println(number + ". " + entry.getKey().name_ + " [x" + entry.getValue() + "]");
                number++;
            }
            String input = "";

            try
            {
                String prompt = CP.getColorString("storage(" + (int) funds_ + ")> ", CP.YELLOW);
                Game.reader.flushConsole();
                System.out.print("\r");
                input = Game.reader.readLine(prompt);
                String params[] = input.split(" (?=(([^\"]*[\"]){2})*[^\"]*$)");
                int num = Integer.parseInt(params[0]);

                int amount = 1;
                if (params.length == 2)
                    amount = Integer.parseInt(params[1]);

                Map.Entry<Item, Integer> entry = getItem(in, num - 1);

                try
                {
                    Game.reader.clearScreen();
                    Game.reader.flushConsole();
                } catch (Exception e)
                {
                }
                System.out.print("\r");
                if (entry != null && amount > 0)
                    storeItem(entry, amount);
                else
                    error();

            } catch (Exception e)
            {
                error();
            }
        } else
        {
            try
            {
                Game.reader.clearScreen();
                Game.reader.flushConsole();
            } catch (Exception e)
            {
            }
            System.out.print("\r");
            for (Map.Entry<Item, Integer> entry : in.entrySet())
            {
                storeItem(entry, entry.getValue());
            }
        }
    }

    public void withdrawItem(boolean all)
    {
        HashMap<Item, Integer> in = new HashMap<Item, Integer>(storage_);
        if (in.isEmpty())
        {
            empty();
            return;
        }

        if (!all)
        {
            int number = 1;
            for (Map.Entry<Item, Integer> entry : in.entrySet())
            {
                System.out.println(number + ". " + entry.getKey().name_ + " [x" + entry.getValue() + "]");
                number++;
            }
            String input = "";

            try
            {
                String prompt = CP.getColorString("storage(" + (int) funds_ + ")> ", CP.YELLOW);
                Game.reader.flushConsole();
                System.out.print("\r");
                input = Game.reader.readLine(prompt);
                String params[] = input.split(" (?=(([^\"]*[\"]){2})*[^\"]*$)");
                int num = Integer.parseInt(params[0]);

                int amount = 1;
                if (params.length == 2)
                    amount = Integer.parseInt(params[1]);

                Map.Entry<Item, Integer> entry = getItem(in, num - 1);

                try
                {
                    Game.reader.clearScreen();
                    Game.reader.flushConsole();
                } catch (Exception e)
                {
                }
                System.out.print("\r");
                if (entry != null && amount > 0)
                    withdrawItem(entry, amount);
                else
                    error();

            } catch (Exception e)
            {
                error();
            }
        } else
        {
            try
            {
                Game.reader.clearScreen();
                Game.reader.flushConsole();
            } catch (Exception e)
            {
            }
            System.out.print("\r");
            for (Map.Entry<Item, Integer> entry : in.entrySet())
            {
                withdrawItem(entry, entry.getValue());
            }
        }
    }

    public Map.Entry<Item, Integer> getItem(HashMap<Item, Integer> in, int num)
    {
        int i = 0;
        for (Map.Entry<Item, Integer> entry : in.entrySet())
        {
            if (num == i)
                return entry;
            i++;
        }

        return null;
    }

    public void storeItem(Map.Entry<Item, Integer> item, int amount)
    {
        if (amount > item.getValue())
            amount = item.getValue();

        for (int i = 0; i < amount; i++)
        {
            addItemToStorage(item.getKey());
            Game.getInstance().player_.removeItemFromInventory(item.getKey());
        }

        System.out.println(Game.getInstance().player_.name_ + " stored " + amount
                + " " + item.getKey().name_ + "(s)");
    }

    public void withdrawItem(Map.Entry<Item, Integer> item, int amount)
    {
        if (amount > item.getValue())
            amount = item.getValue();

        for (int i = 0; i < amount; i++)
        {
            removeItemFromStorage(item.getKey());
            Game.getInstance().player_.addItemToInventory(item.getKey());
        }

        System.out.println(Game.getInstance().player_.name_ + " withdrew " + amount
                + " " + item.getKey().name_ + "(s)");
    }

    public int getStorageSize()
    {
        int num = 0;
        for (Map.Entry<Item, Integer> entry : storage_.entrySet())
        {
            num += entry.getValue();
        }
        return num;
    }

    private void error()
    {
        try
        {
            Game.reader.clearScreen();
            Game.reader.flushConsole();
        } catch (Exception e)
        {
        }
        System.out.print("\r");
        System.out.println("Invalid input!");
    }

    private void msg(String msg)
    {
        try
        {
            Game.reader.clearScreen();
            Game.reader.flushConsole();
        } catch (Exception e)
        {
        }
        System.out.print("\r");
        System.out.println(msg);
    }

    private void empty()
    {
        try
        {
            Game.reader.clearScreen();
            Game.reader.flushConsole();
        } catch (Exception e)
        {
        }
        System.out.print("\r");
        System.out.println("No item to deposit or withdraw!");
    }

    private void leave()
    {
        try
        {
            Game.reader.clearScreen();
            Game.reader.flushConsole();
        } catch (Exception e)
        {
        }
        System.out.print("\r");
    }

    public void addItemToStorage(Item item)
    {
        if (storage_.containsKey(item))
            storage_.replace(item, storage_.get(item) + 1);
        else
            storage_.put(item, 1);
    }

    public void removeItemFromStorage(Item item)
    {
        if (storage_.containsKey(item))
        {
            int amount = storage_.get(item);
            if (amount > 1)
                storage_.replace(item, amount - 1);
            else
                storage_.remove(item);
        }
    }

}
