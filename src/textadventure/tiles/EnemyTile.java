package textadventure.tiles;

import textadventure.*;

import static java.lang.Math.abs;

public class EnemyTile extends Tile
{

    private String name_;
    public Position pos_;
    public int monsterlevel_ = 1;

    public EnemyTile(String name)
    {
        super("敵");
        interactable_ = true;
        name_ = name;
    }

    public EnemyTile(String name, Position pos)
    {
        super("敵");
        interactable_ = true;
        name_ = name;
        pos_ = pos;
    }

    @Override
    public void interact()
    {
        if (Game.getInstance().enemy_ == null)
        {
            Game.getInstance().enemy_ = spawnEnemy(name_);
            Game.getInstance().enemy_.levelUp(monsterlevel_);
            Game.getInstance().enemy_.et_ = this;
            Game.getInstance().mode_ = Command.Mode.BATTLE;
            System.out.println(Game.getInstance().enemy_.name_ + " wants to battle!");
        }
    }

    public static Enemy spawnEnemy(String monster)
    {
        try
        {
            return (Enemy) Class.forName("textadventure.enemies." + monster).newInstance();
        } catch (Exception ex)
        {
            CP.println("Cannot spawn: " + monster, CP.RED);
            return null;
        }
    }

    public void move(Location map, Position player)
    {
        Position move = new Position(pos_);

        int distx = player.x_ - pos_.x_;
        int disty = player.y_ - pos_.y_;

        if (abs(distx) >= abs(disty))
            move.x_ = distx <= 0 ? move.x_ - 1 : move.x_ + 1;
        else
            move.y_ = disty <= 0 ? move.y_ - 1 : move.y_ + 1;

        if (map.objects_.containsKey(move))
        {
            //enemy can't move, let's try the other direction!
            move.x_ = pos_.x_;
            move.y_ = pos_.y_;
            if (abs(distx) < abs(disty))
                move.x_ = distx == 0 ? move.x_ : distx <= 0 ? move.x_ - 1 : move.x_ + 1;
            else
                move.y_ = disty == 0 ? move.y_ : disty <= 0 ? move.y_ - 1 : move.y_ + 1;
        }

        if (!map.objects_.containsKey(move))
        {
            map.removeTile(pos_);
            map.addTile(move, this);
            pos_ = move;
        }

        checkPlayerVicinity(player);
    }

    public void checkPlayerVicinity(Position player)
    {
        int distx = abs(player.x_ - pos_.x_);
        int disty = abs(player.y_ - pos_.y_);


        if (distx + disty == 1)
            collide();
    }

}
