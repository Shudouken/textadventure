package textadventure.tiles;

import textadventure.Tile;

public class DogTile extends Tile
{

    public DogTile()
    {
        super("犬");
        interactable_ = true;
    }

    @Override
    public void collide()
    {
        System.out.println("The dog wears a collar, it reads: aggy doggy");
    }

}
