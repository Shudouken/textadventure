package textadventure;

public class Equipment implements java.io.Serializable
{

    public Weapon weapon_;
    public Armor armor_;

    @Override
    public String toString()
    {
        String temp = "";
        String eol = System.getProperty("line.separator");

        temp += "   - Weapon -" + eol;
        temp += "Name: " + weapon_.name_ + eol;
        temp += "Desc: " + weapon_.desc_ + eol + eol;

        temp += "   - Armor -" + eol;
        temp += "Name: " + armor_.name_ + eol;
        temp += "Desc: " + armor_.desc_;

        return temp;
    }

}
