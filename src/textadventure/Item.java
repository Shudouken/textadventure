package textadventure;

import textadventure.items.*;

import java.util.ArrayList;
import java.util.List;

public abstract class Item implements java.io.Serializable
{

    public String name_;
    public String desc_;
    public int price_;

    //Individual items
    public final static Item banana_ = new Banana();
    public final static Item bare_body_ = new BareBody();
    public final static Item bare_hand_ = new BareHand();
    public final static Item chain_mail_ = new ChainMail();
    public final static Item combat_knife_ = new CombatKnife();
    public final static Item cursed_doll_ = new CursedDoll();
    public final static Item dagger_ = new Dagger();
    public final static Item ether_ = new Ether();
    public final static Item hammer_ = new Hammer();
    public final static Item holy_robe_ = new HolyRobe();
    public final static Item iron_shield_ = new IronShield();
    public final static Item lingerie_ = new Lingerie();
    public final static Item mages_robe_ = new MagesRobe();
    public final static Item morning_star_ = new MorningStar();
    public final static Item muffler_ = new Muffler();
    public final static Item potion_ = new Potion();
    public final static Item rainbow_cape_ = new RainbowCape();
    public final static Item scalpel_ = new Scalpel();
    public final static Item scimitar_ = new Scimitar();
    public final static Item shackle_ball_ = new ShackleBall();
    public final static Item silver_plate_ = new SilverPlate();
    public final static Item soul_drop_ = new SoulDrop();
    public final static Item staff_ = new Staff();
    public final static Item swimwear_ = new Swimwear();
    public final static Item templars_cross_ = new TemplarsCross();
    public final static Item thiefs_clothes_ = new ThiefsClothes();
    public final static Item whip_ = new Whip();
    public final static Item zweihänder_ = new Zweihänder();

    private final static List<Item> items_;

    static
    {
        items_ = new ArrayList<>();

        items_.add(banana_);
        items_.add(chain_mail_);
        items_.add(combat_knife_);
        items_.add(cursed_doll_);
        items_.add(dagger_);
        items_.add(ether_);
        items_.add(hammer_);
        items_.add(holy_robe_);
        items_.add(iron_shield_);
        items_.add(mages_robe_);
        items_.add(morning_star_);
        items_.add(muffler_);
        items_.add(potion_);
        items_.add(rainbow_cape_);
        items_.add(scalpel_);
        items_.add(scimitar_);
        items_.add(shackle_ball_);
        items_.add(silver_plate_);
        items_.add(soul_drop_);
        items_.add(staff_);
        items_.add(swimwear_);
        items_.add(templars_cross_);
        items_.add(thiefs_clothes_);
        items_.add(zweihänder_);
    }

    public Item(String name, String desc, int price)
    {
        name_ = name;
        desc_ = desc;
        price_ = price;
    }

    @Override
    public String toString()
    {
        String temp = "";
        String eol = System.getProperty("line.separator");

        temp += "Name: " + name_ + eol;
        temp += "Desc: " + desc_ + eol;

        return temp;
    }

    public static Item getRandomItem()
    {
        int num = Randomizer.randInt(0, items_.size() - 1);
        Item item = items_.get(num);

        return item;
    }

    @Override
    public int hashCode()
    {
        return name_.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof Item))
            return false;
        if (obj == this)
            return true;
        if (((Item) obj).name_.equals(name_))
            return true;

        return false;
    }

}
