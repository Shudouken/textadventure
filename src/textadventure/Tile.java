package textadventure;

public class Tile implements java.io.Serializable
{

    public String symbol_;
    public boolean interactable_ = false;

    public Tile(String symbol)
    {
        symbol_ = symbol;
    }

    @Override
    public String toString()
    {
        return symbol_;
    }

    public void collide()
    {
        interact();
        Command.freerun_ = false;
    }

    public void interact()
    {
    }

}
