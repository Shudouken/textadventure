package textadventure;

import javax.sound.sampled.*;
import java.io.File;
import java.net.URLDecoder;

public class Sound
{

    public static void playWAV(String filename)
    {
        try
        {
            String path = TextAdventure.class.getProtectionDomain().getCodeSource().getLocation().getPath();
            String decodedPath = URLDecoder.decode(path, "UTF-8");
            decodedPath = decodedPath.replace("dist/TextAdventure.jar", "");
            decodedPath += "sound/" + filename;
            File file = new File(decodedPath);

            AudioInputStream stream;
            AudioFormat format;
            DataLine.Info info;
            Clip clip;

            stream = AudioSystem.getAudioInputStream(file);
            format = stream.getFormat();
            info = new DataLine.Info(Clip.class, format);
            clip = (Clip) AudioSystem.getLine(info);
            clip.open(stream);
            clip.start();
        } catch (Exception e)
        {
            CP.println(e.toString(), CP.RED);
        }
    }

}
