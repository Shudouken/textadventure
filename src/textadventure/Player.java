package textadventure;

import textadventure.SkillTree.Profession;

import java.util.*;

public class Player implements java.io.Serializable
{

    public String name_ = "";
    public int sex_ = -1;
    public int strength_ = -1;
    public int shortcoming_ = -1;
    public int mentality_ = -1;
    public int fear_ = -1;

    public Stats stats_;
    public LevelScaling lvlscaling_;
    public Equipment equip_;
    public HashMap<Item, Integer> inventory_ = new HashMap<>();
    public TreeMap<String, String> tome_ = new TreeMap<>();
    public HashSet<Spell> known_spells_ = new HashSet<>();
    public ArrayList<Item> known_items_ = new ArrayList<>();
    public LinkedHashMap<String, Skill> skills_ = new LinkedHashMap<>();
    public SkillTree skilltree_;
    public boolean wait_turn_ = false;
    public int shekels_;
    public int skillpoints_ = 0;

    //status effects
    public boolean stealth_ = false;
    public boolean charmed_ = false;
    public boolean poisoned_ = false;
    public Spell aoe_ = null;
    public int aoe_duration_;

    public int level_ = 1;
    public int exp_;
    public static final Map<Integer, Integer> exp_map_;

    static
    {
        exp_map_ = new HashMap<>();
        int base = 30;
        exp_map_.put(1, base);
        //System.out.println("Exp-Map(" + 1 + ") = " + exp_map_.get(1));
        for (int i = 2; i <= 98; i++)
        {
            exp_map_.put(i, (int) (exp_map_.get(i - 1) + (exp_map_.get(i - 1) * 0.20f)));
            //System.out.println("Exp-Map(" + i + ") = " + exp_map_.get(i));
        }

    }


    public Player()
    {
        stats_ = new Stats();
        lvlscaling_ = new LevelScaling();
    }

    public void characterCreation()
    {
        String answer;

        System.out.println("Oh brave soul, thy time has come to cease your");
        System.out.println("endless slumber and embark on a new journey");

        System.out.println();

        System.out.println("But before you make your leave, you will need");
        System.out.println("to attain a vessel for your spirit...");

        System.out.println();

        while (sex_ != 1 && sex_ != 2)
        {
            System.out.println("Which body type suits your preference?");
            System.out.println("  1. Masculine");
            System.out.println("  2. Feminine");

            try
            {
                answer = Game.reader.readLine("> ");
                sex_ = Integer.parseInt(answer);

                if (sex_ != 1 && sex_ != 2)
                {
                    System.out.println("Thy soul seems to err");
                    System.out.println();
                }

            } catch (Exception e)
            {
                System.out.println("Thy soul seems to err");
                System.out.println();
                sex_ = -1;
            }
        }

        System.out.println("Your vessel's gender will be " + sex_map_.get(sex_));
        System.out.println();

        while (strength_ != 1 && strength_ != 2 && strength_ != 3 && strength_ != 4)
        {
            System.out.println("In which of the following qualities lies your");
            System.out.println("biggest strength?");
            System.out.println("  1. Force");
            System.out.println("  2. Precision");
            System.out.println("  3. Intelligence");
            System.out.println("  4. Agility");

            try
            {
                answer = Game.reader.readLine("> ");
                strength_ = Integer.parseInt(answer);

                if (strength_ != 1 && strength_ != 2 && strength_ != 3 && strength_ != 4)
                {
                    System.out.println("Thy soul seems to err");
                    System.out.println();
                }

            } catch (Exception e)
            {
                System.out.println("Thy soul seems to err");
                System.out.println();
                strength_ = -1;
            }
        }

        System.out.println("Your vessel's trait will be " + strength_map_.get(strength_));
        System.out.println();

        while (shortcoming_ != 1 && shortcoming_ != 2 && shortcoming_ != 3)
        {
            System.out.println("In which of the following qualities lies your");
            System.out.println("biggest flaw?");
            System.out.println("  1. Endurance");
            System.out.println("  2. Diligence");
            System.out.println("  3. Composure");

            try
            {
                answer = Game.reader.readLine("> ");
                shortcoming_ = Integer.parseInt(answer);

                if (shortcoming_ != 1 && shortcoming_ != 2 && shortcoming_ != 3)
                {
                    System.out.println("Thy soul seems to err");
                    System.out.println();
                }

            } catch (Exception e)
            {
                System.out.println("Thy soul seems to err");
                System.out.println();
                shortcoming_ = -1;
            }
        }

        System.out.print("Your vessel will ");
        if (shortcoming_ == 1)
            System.out.println("be a scholar with a calm mind");
        else if (shortcoming_ == 2)
            System.out.println("be like an immovable fortress");
        else
            System.out.println("never rest when faced with trouble");
        System.out.println();

        while (mentality_ != 1 && mentality_ != 2 && mentality_ != 3)
        {
            System.out.println("Which of these mindsets do you agree with most?");
            System.out.println("  1. Always help those below you");
            System.out.println("  2. Change comes from within");
            System.out.println("  3. Never stop polishing your skills");

            try
            {
                answer = Game.reader.readLine("> ");
                mentality_ = Integer.parseInt(answer);

                if (mentality_ != 1 && mentality_ != 2 && mentality_ != 3)
                {
                    System.out.println("Thy soul seems to err");
                    System.out.println();
                }

            } catch (Exception e)
            {
                System.out.println("Thy soul seems to err");
                System.out.println();
                mentality_ = -1;
            }
        }

        System.out.println("Your vessels mentality will be akin to:");
        System.out.println("  " + mentality_map_.get(mentality_));
        System.out.println();

        while (fear_ != 1 && fear_ != 2 && fear_ != 3)
        {
            System.out.println("Wherein lies your greatest fear?");
            System.out.println("  1. Losing a loved one");
            System.out.println("  2. Getting lost in thought");
            System.out.println("  3. Being unable to move forward");

            try
            {
                answer = Game.reader.readLine("> ");
                fear_ = Integer.parseInt(answer);

                if (fear_ != 1 && fear_ != 2 && fear_ != 3)
                {
                    System.out.println("Thy soul seems to err");
                    System.out.println();
                }

            } catch (Exception e)
            {
                System.out.println("Thy soul seems to err");
                System.out.println();
                fear_ = -1;
            }
        }

        System.out.println("Your vessels dread stems from:");
        System.out.println("  " + fear_map_.get(fear_));
        System.out.println();

        //try {
        System.out.print("Your spirit will now be affixed to your body");
        System.out.print(".");
        //TimeUnit.SECONDS.sleep(1);
        System.out.print(".");
        //TimeUnit.SECONDS.sleep(1);
        System.out.print(".");
        //TimeUnit.SECONDS.sleep(1);
        System.out.println(" Done!");
        //} catch (InterruptedException e) {}

        this.initStats();
        this.generateName();
        shekels_ = 50;

        this.setStartEquipment();
        System.out.println();
        System.out.println("Name: " + name_ + "\t" + "Shekels: " + shekels_);
        System.out.print("LVL:  " + level_ + "/99");
        System.out.println("\tEXP:  " + exp_ + "/" + exp_map_.get(level_));
        System.out.print(stats_);
        System.out.println("You will start your journey with the following belongings");
        System.out.println();
        System.out.println(equip_);
        System.out.println();
        Inventory.listAll(inventory_);
        System.out.println();

        System.out.println("May your journey be successful!");

    }

    private void setStartEquipment()
    {
        equip_ = new Equipment();

        ArrayList<Integer> temp = new ArrayList<>();
        temp.add(stats_.str_);
        temp.add(stats_.vit_);
        temp.add(stats_.int_);
        temp.add(stats_.res_);
        temp.add(stats_.dex_);
        temp.add(stats_.agi_);

        Collections.sort(temp);
        int max = temp.get(5);

        if (stats_.str_ == max)                   //Strength Baseclass
        {
            equip_.weapon_ = (Weapon) Item.zweihänder_;
            equip_.armor_ = (Armor) Item.chain_mail_;
            inventory_.put(Item.potion_, 3);

            skills_.put(Skill.last_resort_.name_, Skill.last_resort_);
            skilltree_ = new SkillTree(Profession.BRUTE);
        } else if (stats_.vit_ == max)              //Vitality Baseclass
        {
            equip_.weapon_ = (Weapon) Item.iron_shield_;
            equip_.armor_ = (Armor) Item.silver_plate_;
            inventory_.put(Item.potion_, 3);
            inventory_.put(Item.ether_, 1);

            skills_.put(Skill.blessing_.name_, Skill.blessing_);
            skilltree_ = new SkillTree(Profession.TEMPLAR);
        } else if (stats_.int_ == max)              //Intelligence Baseclass
        {
            equip_.weapon_ = (Weapon) Item.staff_;
            equip_.armor_ = (Armor) Item.mages_robe_;
            inventory_.put(Item.potion_, 1);
            inventory_.put(Item.ether_, 3);

            skills_.put(Skill.sacrament_.name_, Skill.sacrament_);
            skilltree_ = new SkillTree(Profession.WIZARD);
        } else if (stats_.res_ == max)              //Resistance Baseclass
        {
            equip_.weapon_ = (Weapon) Item.morning_star_;
            equip_.armor_ = (Armor) Item.holy_robe_;
            inventory_.put(Item.potion_, 2);
            inventory_.put(Item.ether_, 2);

            skills_.put(Skill.exorcise_.name_, Skill.exorcise_);
            skilltree_ = new SkillTree(Profession.MONK);
        } else if (stats_.dex_ == max)              //Dexterity Baseclass
        {
            equip_.weapon_ = (Weapon) Item.scimitar_;
            equip_.armor_ = (Armor) Item.muffler_;
            inventory_.put(Item.potion_, 2);
            inventory_.put(Item.ether_, 1);

            skills_.put(Skill.aura_blade_.name_, Skill.aura_blade_);
            skilltree_ = new SkillTree(Profession.SPECIALIST);
        } else                                     //Agility Baseclass
        {
            equip_.weapon_ = (Weapon) Item.dagger_;
            equip_.armor_ = (Armor) Item.thiefs_clothes_;
            inventory_.put(Item.potion_, 5);
            inventory_.put(Item.ether_, 1);

            skills_.put(Skill.steal_.name_, Skill.steal_);
            skilltree_ = new SkillTree(Profession.PICKPOCKET);
        }

        for (Map.Entry<Item, Integer> entry : inventory_.entrySet())
            known_items_.add(entry.getKey());

        known_items_.add((Item) equip_.weapon_);
        known_items_.add((Item) equip_.armor_);
    }

    public void levelUp()
    {
        if (level_ < 99)
        {
            exp_ = 0;
            level_ += 1;

            Buff buffs = null;
            if (stats_ instanceof Buff)
            {
                buffs = ((Buff) stats_);
                stats_ = ((Buff) stats_).original_;
            }

            Stats old = new Stats(stats_);

            if (lvlscaling_.str_ >= 100)
                stats_.str_ += 1;
            stats_.str_ += level_ % ((100 - (lvlscaling_.str_ % 100)) / 10) == 0 ? 1 : 0;

            if (lvlscaling_.vit_ >= 100)
                stats_.vit_ += 1;
            stats_.vit_ += level_ % ((100 - (lvlscaling_.vit_ % 100)) / 10) == 0 ? 1 : 0;

            if (lvlscaling_.int_ >= 100)
                stats_.int_ += 1;
            stats_.int_ += level_ % ((100 - (lvlscaling_.int_ % 100)) / 10) == 0 ? 1 : 0;

            if (lvlscaling_.res_ >= 100)
                stats_.res_ += 1;
            stats_.res_ += level_ % ((100 - (lvlscaling_.res_ % 100)) / 10) == 0 ? 1 : 0;

            if (lvlscaling_.dex_ >= 100)
                stats_.dex_ += 1;
            stats_.dex_ += level_ % ((100 - (lvlscaling_.dex_ % 100)) / 10) == 0 ? 1 : 0;

            if (lvlscaling_.agi_ >= 100)
                stats_.agi_ += 1;
            stats_.agi_ += level_ % ((100 - (lvlscaling_.agi_ % 100)) / 10) == 0 ? 1 : 0;

            stats_.updateHPMP(level_);

            System.out.println(name_ + " reached Level " + level_ + "!");
            if (level_ % 2 == 0)
            {
                skillpoints_ += 1;
                System.out.println("Gained a skillpoint!");
            }
            System.out.println("HP  +" + (stats_.max_hp_ - old.max_hp_) + " (" + stats_.hp_ + "/" + stats_.max_hp_ + ")");
            System.out.println("MP  +" + (stats_.max_mp_ - old.max_mp_) + " (" + stats_.mp_ + "/" + stats_.max_mp_ + ")");
            System.out.println("Str +" + (stats_.str_ - old.str_) + " (" + stats_.str_ + ")");
            System.out.println("Vit +" + (stats_.vit_ - old.vit_) + " (" + stats_.vit_ + ")");
            System.out.println("Int +" + (stats_.int_ - old.int_) + " (" + stats_.int_ + ")");
            System.out.println("Res +" + (stats_.res_ - old.res_) + " (" + stats_.res_ + ")");
            System.out.println("Dex +" + (stats_.dex_ - old.dex_) + " (" + stats_.dex_ + ")");
            System.out.println("Agi +" + (stats_.agi_ - old.agi_) + " (" + stats_.agi_ + ")");

            advanceSkilltree();

            if (buffs != null)
                stats_ = buffs.reapplyBuffs(stats_);
        }
    }

    public void advanceSkilltree()
    {
        Skill temp = skilltree_.advance(level_);

        if (temp != null)
        {
            skills_.put(temp.name_, temp);
            System.out.println(name_ + " learned new Skill: " + temp.name_);
        }
    }

    public void printHiddenInfo()
    {
        stats_.printHiddenStats();
        System.out.print(lvlscaling_);
        System.out.println("Class: " + skilltree_.class_);
    }

    public int takeDamage(Player seme, int atk, float acc, float crit, int def, int res, float eva)
    {
        int hit = Randomizer.randInt(1, 1 + (int) (acc / 10f));
        boolean critical = false;

        int damage = 0;
        if (seme.equip_.weapon_.targets_res_)
            damage = (atk - res) + hit;
        else
            damage = (atk - def) + hit;

        damage *= 1 + (seme.equip_.weapon_.getScaling(seme.stats_) / 10);

        if (damage < 0)
            damage = 0;

        if (Randomizer.randInt(0, 100) <= crit)
        {
            damage = 1 + (damage * 2);
            critical = true;
        }

        if (Randomizer.randInt(0, 100) <= eva && !critical)
        {
            System.out.println(seme.name_ + " attacked! " + name_ + " evaded the attack!");
            return 0;
        }

        stats_.loseHP(damage);

        System.out.print(seme.name_ + " attacked! " + name_ + " took ");
        CP.print("" + damage, CP.RED);
        System.out.print(" damage!");

        if (critical)
            CP.print(" CRITICAL HIT!", CP.PURPLE);

        System.out.println();
        return damage;
    }

    public void takeMPDamage(Player seme, int atk, float acc, int def, int res, float eva)
    {
        int hit = Randomizer.randInt(1, 1 + (int) (acc / 10f));

        int damage = (atk - res) + hit;

        damage *= (1 + (seme.equip_.weapon_.getScaling(seme.stats_) / 10));

        if (damage < 0)
            damage = 0;

        if (Randomizer.randInt(0, 100) <= eva)
        {
            System.out.println(seme.name_ + " attacked! " + name_ + " evaded the attack!");
            return;
        }

        stats_.loseMP(damage);

        System.out.print(seme.name_ + " attacked! " + name_ + " took ");
        CP.print("" + damage, CP.BLUE);
        System.out.print(" damage!");

        System.out.println();
    }

    public void takeSpellDamage(Player seme, int atk)
    {
        int bonus = seme.stats_.getInt();
        int exp_bonus = 1 + (seme.stats_.getInt() / 10);

        atk *= exp_bonus;
        atk += bonus;

        int damage = atk - equip_.armor_.calcResistance(stats_);

        if (damage < 0)
            damage = 0;

        stats_.loseHP(damage);

        System.out.print(seme.name_ + " cast a spell! " + name_ + " took ");
        CP.print("" + damage, CP.RED);
        System.out.print(" damage!");
        System.out.println();
    }

    public void setAOE(Spell spell)
    {
        aoe_ = spell;
        aoe_duration_ = 3;
        System.out.println(name_ + " was trapped by " + aoe_.name_);
    }

    public void takeAOEDamage(int atk)
    {
        String aoe = aoe_.name_;
        if (aoe_duration_ == 0)
        {
            System.out.println(name_ + "'s " + aoe + " wore off!");
            aoe_ = null;
            return;
        }
        atk = atk / (4-aoe_duration_);

        if (atk < 0)
            atk = 1;

        stats_.loseHP(atk);

        System.out.print(name_ + " was hurt by " + aoe + "! " + name_ + " took ");
        CP.print("" + atk, CP.RED);
        System.out.print(" damage!");
        System.out.println();

        aoe_duration_ -= 1;
    }

    public void takeAOEHeal(int amount)
    {
        String aoe = aoe_.name_;
        if (aoe_duration_ == 0)
        {
            System.out.println(name_ + "'s " + aoe + " wore off!");
            aoe_ = null;
            return;
        }
        amount = amount / (4-aoe_duration_);

        if (amount < 0)
            amount = 1;

        stats_.gainHP(amount);

        System.out.print(name_ + " was healed by " + aoe + "! " + name_ + " gained ");
        CP.print("" + amount, CP.GREEN);
        System.out.print(" HP!");
        System.out.println();

        aoe_duration_ -= 1;
    }

    public void heal(int amount)
    {
        int add = amount + stats_.getInt() + level_;

        stats_.gainHP(add);

        System.out.print(name_ + " cast a healing spell! " + name_ + " gained ");
        CP.print("" + add, CP.GREEN);
        System.out.print(" health!");
        System.out.println();
    }

    public void buff(String attribute)
    {
        Buff buffed = new Buff(stats_);
        buffed.applyBuff(attribute);

        System.out.println(name_ + "'s " + attribute + " was buffed!");

        stats_ = buffed;
    }

    public void debuff(String attribute)
    {
        Buff buffed = new Buff(stats_);
        buffed.applyDebuff(attribute);

        System.out.println(name_ + "'s " + attribute + " was de-buffed!");

        stats_ = buffed;
    }

    public void listSkills()
    {
        for (Map.Entry<String, Skill> entry : skills_.entrySet())
        {
            System.out.println(entry.getValue());
            System.out.println();
        }
        //System.out.println();
    }

    public void listSpellsWith(String keyword)
    {
        String color = CP.getColorString(keyword,CP.RED);

        if(! known_spells_.isEmpty())
        {
            System.out.println();
            System.out.println("   - Spells -");
            for(Spell s : known_spells_)
            {
                if(s.formula_.contains(keyword))
                {
                    System.out.println(s.toString().replace(keyword, color));
                }
            }
        }
    }

    public void getSpoils(Enemy enemy)
    {
        Game.getInstance().bt_.applyInterest();
        if (level_ < 99)
        {
            int exp = (int) (enemy.exp_ * stats_.exp_rate_);
            System.out.println(name_ + " gained " + exp + " EXP!");
            exp_ += exp;

            if (exp_ >= exp_map_.get(level_))
                levelUp();
        }

        if (enemy.drop_ != null && Randomizer.randInt(0, 100) <= enemy.droprate_)
        {
            System.out.println(name_ + " found " + enemy.drop_.name_ + "!");
            addItemToInventory(enemy.drop_);
        }

        if (enemy.shekels_ > 0)
        {
            shekels_ += enemy.shekels_;
            System.out.println(name_ + " found " + enemy.shekels_ + " shekels!");
        }
    }

    public void addItemToInventory(Item item)
    {
        if (inventory_.containsKey(item))
            inventory_.replace(item, inventory_.get(item) + 1);
        else
        {
            if (!known_items_.contains(item))
                known_items_.add(item);

            inventory_.put(item, 1);
        }
    }

    public void removeItemFromInventory(Item item)
    {
        if (inventory_.containsKey(item))
        {
            int amount = inventory_.get(item);
            if (amount > 1)
                inventory_.replace(item, amount - 1);
            else
                inventory_.remove(item);
        }
    }

    private void generateName()
    {

        int r = Randomizer.randInt(1, 6);

        if (sex_ == 1)
        {
            switch (r)
            {
                case 1:
                    name_ = "Johnny";
                    break;
                case 2:
                    name_ = "Carlos";
                    break;
                case 3:
                    name_ = "Brighton";
                    break;
                case 4:
                    name_ = "Hemsway";
                    break;
                case 5:
                    name_ = "Xavier";
                    break;
                case 6:
                    name_ = "Yue Fei";
                    break;
            }
        } else
        {
            switch (r)
            {
                case 1:
                    name_ = "Anne";
                    break;
                case 2:
                    name_ = "Vicky";
                    break;
                case 3:
                    name_ = "Gweneth";
                    break;
                case 4:
                    name_ = "Eva";
                    break;
                case 5:
                    name_ = "Serenity";
                    break;
                case 6:
                    name_ = "Mei Ling";
                    break;
            }
        }
    }

    private void initStats()
    {
        level_ = 1;
        exp_ = 0;

        if (strength_ == 1)       //Force
        {
            stats_.str_ += 3;
            lvlscaling_.str_ += 40;

            stats_.hp_bonus_ = 30;
        } else if (strength_ == 2)  //Precision
        {
            stats_.dex_ += 3;
            lvlscaling_.dex_ += 40;

            stats_.crit_bonus_ = 9.0f;
        } else if (strength_ == 3)  //Intelligence
        {
            stats_.int_ += 3;
            lvlscaling_.int_ += 40;

            stats_.mp_bonus_ = 30;
        } else                      //Agility
        {
            stats_.agi_ += 3;
            lvlscaling_.agi_ += 40;

            stats_.eva_bonus_ = 9.0f;
        }

        if (shortcoming_ == 1)        //-Endurance  (less vit, vit scaling)
        {
            //diligence bonus
            stats_.exp_rate_ = 1.3f;
            lvlscaling_.increaseAll(10);

            //composure bonus
            stats_.susceptibility_ = 0.8f;
            stats_.res_ += 3;
            lvlscaling_.res_ += 30;

            //compound bonus
            stats_.int_ += 3;
            lvlscaling_.int_ += 5;
        } else if (shortcoming_ == 2)   //-Diligence  (slower leveling)
        {
            //endurance bonus
            stats_.vit_ += 3;
            lvlscaling_.vit_ += 30;

            //composure bonus
            stats_.susceptibility_ = 0.8f;
            stats_.res_ += 3;
            lvlscaling_.res_ += 30;

            //compound bonus
            stats_.str_ += 3;
            lvlscaling_.str_ += 5;
        } else                          //-Composure  (less res, res scaling, susceptibility)
        {
            //endurance bonus
            stats_.vit_ += 3;
            lvlscaling_.vit_ += 30;

            //diligence bonus
            stats_.exp_rate_ = 1.3f;
            lvlscaling_.increaseAll(10);

            //compound bonus
            stats_.dex_ += 3;
            lvlscaling_.dex_ += 5;
        }

        if (mentality_ == 1)      //Always help those below you
        {
            stats_.str_ += 1;
            stats_.vit_ += 2;

            stats_.agi_ -= 2;
            lvlscaling_.agi_ -= 15;
            lvlscaling_.str_ += 10;
            lvlscaling_.vit_ += 10;
            lvlscaling_.dex_ += 10;
        } else if (mentality_ == 2) //Change comes from within
        {
            stats_.int_ += 1;
            stats_.res_ += 2;

            stats_.dex_ -= 2;
            lvlscaling_.dex_ -= 15;
            lvlscaling_.int_ += 10;
            lvlscaling_.res_ += 10;
            lvlscaling_.vit_ += 10;
        } else                      //Never stop polishing your skills
        {
            stats_.dex_ += 1;
            stats_.agi_ += 1;

            stats_.int_ -= 1;
            lvlscaling_.int_ -= 15;
            lvlscaling_.str_ += 10;
            lvlscaling_.dex_ += 10;
            lvlscaling_.agi_ += 10;
        }

        if (fear_ == 1)            //Losing a loved one           (+str,vit -int,res)
        {
            stats_.str_ += (stats_.str_ * 50) / 100;
            lvlscaling_.str_ += (lvlscaling_.str_ * 25) / 100;
            stats_.vit_ += (stats_.vit_ * 40) / 100;
            lvlscaling_.vit_ += (lvlscaling_.vit_ * 20) / 100;


            stats_.int_ -= (stats_.int_ * 30) / 100;
            lvlscaling_.int_ -= 15;
            stats_.res_ -= (stats_.res_ * 30) / 100;
            lvlscaling_.res_ -= 15;
        } else if (fear_ == 2)      //Getting lost in thought      (+int,res -str,vit)
        {
            stats_.int_ += (stats_.int_ * 50) / 100;
            lvlscaling_.int_ += (lvlscaling_.int_ * 25) / 100;
            stats_.res_ += (stats_.res_ * 40) / 100;
            lvlscaling_.res_ += (lvlscaling_.res_ * 20) / 100;

            stats_.str_ -= (stats_.str_ * 30) / 100;
            lvlscaling_.str_ -= 15;
            stats_.vit_ -= (stats_.vit_ * 30) / 100;
            lvlscaling_.vit_ -= 15;
        } else                      //Being unable to move forward (+dex,agi -vit,res)
        {
            stats_.dex_ += (stats_.dex_ * 50) / 100;
            lvlscaling_.dex_ += (lvlscaling_.dex_ * 25) / 100;
            stats_.agi_ += (stats_.agi_ * 40) / 100;
            lvlscaling_.agi_ += (lvlscaling_.agi_ * 20) / 100;

            stats_.vit_ -= (stats_.vit_ * 30) / 100;
            lvlscaling_.vit_ -= 15;
            stats_.res_ -= (stats_.res_ * 30) / 100;
            lvlscaling_.res_ -= 15;
        }

        stats_.updateHPMP(level_);
    }

    @Override
    public String toString()
    {
        String temp = "";
        String eol = System.getProperty("line.separator");

        temp += "Name: " + name_ + "\t" + "Shekels: " + shekels_ + eol;
        temp += "LVL:  " + level_ + "/99";

        if (level_ < 99)
            temp += "\tEXP:  " + exp_ + "/" + exp_map_.get(level_) + eol;
        else
            temp += "\tEXP:  -/-" + eol;

        temp += stats_;
        temp += equip_.armor_.toResistances(stats_);

        return temp;
    }

    // ------- map declarations -------

    public static final Map<Integer, String> sex_map_;

    static
    {
        sex_map_ = new HashMap<>();
        sex_map_.put(1, "Male");
        sex_map_.put(2, "Female");
    }

    //pronouns
    public static final Map<Integer, String> pronoun_map_;

    static
    {
        pronoun_map_ = new HashMap<>();
        pronoun_map_.put(1, "his");
        pronoun_map_.put(2, "her");
    }

    //different starting stats, will make certain weapons scale better
    public static final Map<Integer, String> strength_map_;

    static
    {
        strength_map_ = new HashMap<>();
        strength_map_.put(1, "Force");
        strength_map_.put(2, "Precision");
        strength_map_.put(3, "Intelligence");
        strength_map_.put(4, "Agility");
    }

    /*endurance = less HP
      diligence = more exp needed for levelup
      composure = easier to get negative status affliction */
    public static final Map<Integer, String> shortcoming_map_;

    static
    {
        shortcoming_map_ = new HashMap<>();
        shortcoming_map_.put(1, "Endurance");
        shortcoming_map_.put(2, "Diligence");
        shortcoming_map_.put(3, "Composure");
    }

    //some mixed stat bonuses
    public static final Map<Integer, String> mentality_map_;

    static
    {
        mentality_map_ = new HashMap<>();
        mentality_map_.put(1, "Always help those below you");
        mentality_map_.put(2, "Change comes from within");
        mentality_map_.put(3, "Never stop polishing your skills");
    }

    //percentage stat bonuses, can scale with previous choices
    public static final Map<Integer, String> fear_map_;

    static
    {
        fear_map_ = new HashMap<>();
        fear_map_.put(1, "Losing a loved one");
        fear_map_.put(2, "Getting lost in thought");
        fear_map_.put(3, "Being unable to move forward");
    }

}
