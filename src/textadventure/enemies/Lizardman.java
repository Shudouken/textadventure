package textadventure.enemies;

import textadventure.Armor;
import textadventure.Command;
import textadventure.Enemy;
import textadventure.Game;
import textadventure.Item;
import textadventure.Player;
import textadventure.Weapon;

public class Lizardman extends Enemy
{

    public Lizardman()
    {
        super();

        name_ = "Lizardman";

        base_exp_ = 20;
        shekels_ = 20;
        droprate_ = 5;
        drop_ = Item.scimitar_;

        stats_.str_ = 18;
        stats_.vit_ = 18;
        stats_.int_ = 0;
        lvlscaling_.int_ = 10;
        stats_.res_ = 2;
        lvlscaling_.res_ = 30;
        stats_.dex_ = 18;
        stats_.agi_ = 14;

        equip_.weapon_ = (Weapon) Item.scimitar_;
        equip_.armor_ = (Armor) Item.muffler_;

        stats_.updateHPMP(level_);
    }

    @Override
    public void action(Game game, Player player)
    {
        Command.attack(this, player);
    }

}
