package textadventure.enemies;

import textadventure.*;

public class Shaman extends Enemy
{

    public Shaman()
    {
        super();

        name_ = "Shaman";

        base_exp_ = 5;
        shekels_ = 10;
        droprate_ = 5;
        drop_ = Item.ether_;

        stats_.int_ = 8;
        stats_.res_ = 14;
        stats_.vit_ = 4;
        lvlscaling_.vit_ = 50;
        stats_.agi_ = 4;
        stats_.str_ = 3;
        lvlscaling_.str_ = 30;

        stats_.res_fire_ = 1.25f;
        stats_.res_wind_ = 1.25f;
        stats_.res_thunder_ = 1.25f;
        stats_.res_water_ = 1.25f;
        stats_.updateHPMP(level_);
    }

    @Override
    public void action(Game game, Player player)
    {
        int seed = Randomizer.randInt(0, 100);

        if (seed <= 5)
            cast(Spell.strength_down_, player);
        else if (seed <= 45)
            cast(Spell.fireball_, player);
        else
            Command.attack(this, player);
    }

}
