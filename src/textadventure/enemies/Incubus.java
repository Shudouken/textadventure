package textadventure.enemies;

import textadventure.*;

public class Incubus extends Enemy
{

    public Incubus()
    {
        super();

        name_ = "Incubus";
        demon_ = true;

        base_exp_ = 35;
        shekels_ = 300;

        sex_ = 1;

        droprate_ = 2;
        drop_ = Item.lingerie_;

        skill_ = Skill.charm_;

        stats_.str_ = 40;
        stats_.int_ = 10;
        stats_.res_ = 20;
        stats_.vit_ = 4;
        stats_.agi_ = 14;
        stats_.dex_ = 10;

        stats_.res_light_ = 1.50f;

        stats_.updateHPMP(level_);

        equip_.weapon_ = (Weapon) Item.whip_;
        equip_.armor_  = (Armor)  Item.lingerie_;
    }

    @Override
    public void action(Game game, Player player)
    {
        int seed = Randomizer.randInt(0, 100);

        if (seed <= 33 && ! player.charmed_)
        {
            useSkill(game, player);
            return;
        }

        if (seed <= 66)
            Command.attack(this, player);
        else
            cast(Spell.ray_, player);
    }

}
