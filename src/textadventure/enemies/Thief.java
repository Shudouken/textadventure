package textadventure.enemies;

import textadventure.Command;
import textadventure.Enemy;
import textadventure.Game;
import textadventure.Item;
import textadventure.Player;
import textadventure.Randomizer;
import textadventure.Skill;
import textadventure.Weapon;

public class Thief extends Enemy
{

    public Thief()
    {
        super();

        name_ = "Thief";

        base_exp_ = 2;
        shekels_ = 100;

        droprate_ = 15;
        if (Randomizer.randInt(0, 100) <= 50)
            drop_ = Item.ether_;
        else
            drop_ = Item.potion_;

        skill_ = Skill.steal_;

        lvlscaling_.str_ = 70;
        stats_.int_ = 4;
        stats_.res_ = 4;
        stats_.vit_ = 4;
        stats_.agi_ = 9;
        stats_.dex_ = 14;

        stats_.updateHPMP(level_);

        equip_.weapon_ = (Weapon) Item.dagger_;
    }

    @Override
    public void action(Game game, Player player)
    {
        int seed = Randomizer.randInt(0, 100);

        if (seed <= 12)
            useSkill(game, player);
        else
            Command.attack(this, player);
    }

}
