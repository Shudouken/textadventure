package textadventure.enemies;

import textadventure.Enemy;
import textadventure.Item;

public class Zombie extends Enemy
{

    public Zombie()
    {
        super();

        name_ = "Zombie";

        base_exp_ = 3;
        droprate_ = 5;
        drop_ = Item.potion_;

        stats_.str_ = 10;
        stats_.vit_ = 5;
        stats_.res_light_ = 0.75f;
        stats_.updateHPMP(level_);
    }

}
