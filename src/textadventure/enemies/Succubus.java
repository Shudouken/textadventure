package textadventure.enemies;

import textadventure.*;

public class Succubus extends Enemy
{

    public Succubus()
    {
        super();

        name_ = "Succubus";
        demon_ = true;

        base_exp_ = 35;
        shekels_ = 300;

        sex_ = 2;

        droprate_ = 4;
        drop_ = Item.whip_;

        skill_ = Skill.charm_;

        stats_.str_ = 10;
        stats_.int_ = 40;
        stats_.res_ = 4;
        stats_.vit_ = 20;
        stats_.agi_ = 14;
        stats_.dex_ = 10;

        stats_.res_darkness_ = 1.50f;

        stats_.updateHPMP(level_);

        equip_.weapon_ = (Weapon) Item.whip_;
        equip_.armor_  = (Armor)  Item.lingerie_;
    }

    @Override
    public void action(Game game, Player player)
    {
        int seed = Randomizer.randInt(0, 100);

        if (seed <= 33 && ! player.charmed_)
        {
            useSkill(game, player);
            return;
        }

        if (seed <= 66)
            Command.attack(this, player);
        else
            cast(Spell.dark_lance_, player);
    }

}
