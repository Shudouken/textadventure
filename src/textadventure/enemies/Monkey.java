package textadventure.enemies;

import textadventure.Command;
import textadventure.Enemy;
import textadventure.Game;
import textadventure.Item;
import textadventure.Player;
import textadventure.UseableItem;

public class Monkey extends Enemy
{

    public Monkey()
    {
        super();

        name_ = "Monkey";

        base_exp_ = 15;
        droprate_ = 10;
        drop_ = Item.banana_;

        stats_.str_ = 16;
        stats_.vit_ = 14;
        stats_.int_ = 7;
        stats_.res_ = 7;
        stats_.dex_ = 16;
        stats_.agi_ = 20;

        stats_.res_fire_ = 0.75f;
        stats_.updateHPMP(level_);
    }

    @Override
    public void action(Game game, Player player)
    {
        String command = Command.last_command_[0];

        if (stats_.hp_ <= stats_.max_hp_ / 10 && drop_ != null)
        {
            System.out.print("The monkey ate his banana! ");
            ((UseableItem) drop_).effect(this);
            drop_ = null;
            return;
        }

        if (command.equals("useskill") || command.equals("us"))
        {
            if (Command.last_skill_ != null)
            {
                System.out.println("Monkey see, monkey do! ");
                Command.last_skill_.effect(game, this, player);
                return;
            }
        } else if (command.equals("cast") || command.equals("c"))
        {
            if (Command.last_spell_ != null)
            {
                System.out.println("Monkey see, monkey do! ");
                Command.last_spell_.execute(this, player);
                return;
            }
        }

        Command.attack(this, player);
    }

}
