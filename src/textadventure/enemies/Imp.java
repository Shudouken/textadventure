package textadventure.enemies;

import textadventure.Command;
import textadventure.Enemy;
import textadventure.Game;
import textadventure.Item;
import textadventure.Player;
import textadventure.Randomizer;
import textadventure.Skill;

public class Imp extends Enemy
{

    private boolean shenanigans_ = false;

    public Imp()
    {
        super();

        name_ = "Imp";
        demon_ = true;

        base_exp_ = 4;
        droprate_ = 5;
        drop_ = Item.potion_;
        skill_ = Skill.shenanigan_;

        stats_.str_ = 8;
        stats_.vit_ = 8;
        stats_.int_ = 8;
        stats_.res_ = 8;
        stats_.dex_ = 8;
        stats_.agi_ = 8;

        stats_.res_light_ = 0.75f;
        stats_.res_darkness_ = 1.50f;
        stats_.updateHPMP(level_);
    }

    @Override
    public void action(Game game, Player player)
    {
        int seed = Randomizer.randInt(0, 100);

        if (shenanigans_)
        {
            useSkill(game, player);
            shenanigans_ = false;
            return;
        }

        if (seed <= 30)
        {
            talk("Time for some shenanigans!");
            shenanigans_ = true;
        } else
            Command.attack(this, player);
    }

}
