package textadventure.enemies;

import textadventure.*;

public class Priest extends Enemy
{

    public Priest()
    {
        super();

        name_ = "Priest";

        base_exp_ = 14;
        shekels_ = 50;
        droprate_ = 5;
        drop_ = Item.morning_star_;

        stats_.str_ = 12;
        stats_.vit_ = 5;
        lvlscaling_.vit_ = 50;
        stats_.int_ = 20;
        stats_.res_ = 20;
        stats_.dex_ = 14;
        stats_.agi_ = 14;

        equip_.weapon_ = (Weapon) Item.morning_star_;

        stats_.res_light_ = 1.50f;
        stats_.res_darkness_ = 0.50f;
        stats_.updateHPMP(level_);
    }

    @Override
    public void action(Game game, Player player)
    {

        if (stats_ instanceof Buff)
            Command.attack(this, player);
        else
            cast(Spell.strength_up_, this);
    }

}
