package textadventure.enemies;

import textadventure.*;

import java.util.ArrayList;

public class Wizard extends Enemy
{

    private static final ArrayList<Spell> fire_spells_;

    static
    {
        fire_spells_ = new ArrayList<>();

        fire_spells_.add(Spell.fireball_);
        fire_spells_.add(Spell.fireball_);
        fire_spells_.add(Spell.fireball_);
        fire_spells_.add(Spell.strength_down_);
        fire_spells_.add(Spell.resistance_down_);
    }

    private static final ArrayList<Spell> water_spells_;

    static
    {
        water_spells_ = new ArrayList<>();

        water_spells_.add(Spell.splash_);
        water_spells_.add(Spell.splash_);
        water_spells_.add(Spell.splash_);
        water_spells_.add(Spell.vitality_down_);
        water_spells_.add(Spell.resistance_down_);
    }

    private static final ArrayList<Spell> wind_spells_;

    static
    {
        wind_spells_ = new ArrayList<>();

        wind_spells_.add(Spell.tornado_);
        wind_spells_.add(Spell.tornado_);
        wind_spells_.add(Spell.tornado_);
        wind_spells_.add(Spell.agility_down_);
        wind_spells_.add(Spell.resistance_down_);
    }

    private static final ArrayList<Spell> thunder_spells_;

    static
    {
        thunder_spells_ = new ArrayList<>();

        thunder_spells_.add(Spell.lightning_);
        thunder_spells_.add(Spell.lightning_);
        thunder_spells_.add(Spell.lightning_);
        thunder_spells_.add(Spell.dexterity_down_);
        thunder_spells_.add(Spell.resistance_down_);
    }

    private ArrayList<Spell> spells_;

    public Wizard()
    {
        super();


        int rand = Randomizer.randInt(1, 4);

        if (rand == 1)
        {
            name_ = "Red ";
            stats_.res_fire_ = 1.25f;
            stats_.res_water_ = 0.75f;
            spells_ = fire_spells_;
        } else if (rand == 2)
        {
            name_ = "Blue ";
            stats_.res_water_ = 1.25f;
            stats_.res_thunder_ = 0.75f;
            spells_ = water_spells_;
        } else if (rand == 3)
        {
            name_ = "Green ";
            stats_.res_wind_ = 1.25f;
            stats_.res_fire_ = 0.75f;
            spells_ = wind_spells_;
        } else
        {
            name_ = "Purple ";
            stats_.res_thunder_ = 1.25f;
            stats_.res_wind_ = 0.75f;
            spells_ = thunder_spells_;
        }


        name_ += "Wizard";

        base_exp_ = 25;
        shekels_ = 10;
        droprate_ = 9;
        drop_ = Item.soul_drop_;

        equip_.weapon_ = (Weapon) Item.staff_;

        stats_.int_ = 20;
        stats_.res_ = 16;
        stats_.vit_ = 4;
        lvlscaling_.vit_ = 70;
        stats_.agi_ = 4;
        lvlscaling_.agi_ = 10;
        stats_.str_ = 3;
        lvlscaling_.str_ = 10;
        stats_.updateHPMP(level_);
    }

    @Override
    public void action(Game game, Player player)
    {
        int seed = Randomizer.randInt(0, spells_.size() - 1);
        cast(spells_.get(seed), player);
    }

}
