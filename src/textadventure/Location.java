package textadventure;

import textadventure.tiles.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Location implements java.io.Serializable
{

    public Map<Position, Tile> objects_;
    public List<EnemyTile> enemies_;
    public int w_ = 6;
    public int h_ = 6;

    public Location(int w, int h)
    {
        objects_ = new HashMap<Position, Tile>();
        enemies_ = new ArrayList<EnemyTile>();
        w_ = w;
        h_ = h;

        buildWall();
    }

    public void addTile(Position pos, Tile obj)
    {
        if ((pos.x_ >= w_ || pos.y_ >= h_) && Command.debug)
        {
            System.out.println("Warning: Exceeding map boundaries at: " + pos);
            System.out.println("  Tile " + obj + " will not properly be shown");
        }

        objects_.put(pos, obj);
    }

    public Position addRandomTile(Tile obj)
    {
        int offset = (obj instanceof TreasureTile || obj instanceof MerchantTile) ? 2 : 1;
        Position pos = new Position(Randomizer.randInt(offset, w_ - offset), Randomizer.randInt(offset, h_ - offset));

        if (!objects_.containsKey(pos))
        {
            objects_.put(pos, obj);
            return pos;
        } else
            return addRandomTile(obj);
    }

    public void addRandomEnemyTile(EnemyTile obj)
    {
        obj.pos_ = addRandomTile(obj);
        enemies_.add(obj);
    }

    public void removeTile(Position pos)
    {
        if (objects_.isEmpty())
            return;

        objects_.remove(pos);
    }

    public void buildWall()
    {
        Tile lr = new Tile("｜");
        Tile tb = new Tile("ー");

        for (int y = 0; y < h_; y++)
        {
            for (int x = 0; x < w_; x++)
            {
                if (x == 0 || x == w_ - 1)
                    addTile(new Position(x, y), lr);

                if (y == 0 || y == h_ - 1)
                    addTile(new Position(x, y), tb);
            }
        }

        Tile b = new Tile("畑");

        objects_.put(new Position(0, 0), b);
        objects_.put(new Position(0, h_ - 1), b);
        objects_.put(new Position(w_ - 1, 0), b);
        objects_.put(new Position(w_ - 1, h_ - 1), b);
    }

    public void setCornerStones(String symbol)
    {
        Tile b = new Tile(symbol);

        objects_.put(new Position(0, 0), b);
        objects_.put(new Position(0, h_ - 1), b);
        objects_.put(new Position(w_ - 1, 0), b);
        objects_.put(new Position(w_ - 1, h_ - 1), b);
    }

    @Override
    public String toString()
    {
        String temp = "";
        String eol = System.getProperty("line.separator");

        for (int y = 0; y < h_; y++)
        {
            for (int x = 0; x < w_; x++)
            {
                Position pos = new Position(x, y);

                if (objects_.containsKey(pos))
                    temp += objects_.get(pos);
                else
                    temp += "　";
            }
            temp += eol;
        }
        return temp;
    }

    public static void setStartTown()
    {

        Location town = new Location(8, 8);
        PlayerTile pt = new PlayerTile(new Position(1, 1));
        town.addTile(pt.pos_, pt);
        town.addTile(new Position(6, 5), new ShopKeeperTile());
        town.addTile(new Position(1, 6), new VillagerTile());
        town.addTile(new Position(2, 6), new DogTile());
        town.addTile(new Position(3, 4), new WellTile());
        town.addTile(new Position(1, 3), new OracleTile());

        BankTile bt = new BankTile();
        town.addTile(new Position(7, 2), bt);

        //town.addTile(new Position(1,4), new TreasureTile(Item.swimwear_));

        GateTile gt = new GateTile(new Position(5, 0));
        town.addTile(gt.pos_, gt);

        town.addTile(new Position(2, 0), new ResetTile());

        town.setCornerStones("村");

        WitchTile wt = new WitchTile(new Position(5,1));
        town.addTile(wt.pos_, wt);

        //EnemyTile et = new EnemyTile("Incubus", new Position(4,4));
        //town.addTile(et.pos_, et);

        Game.getInstance().map_ = town;
        Game.getInstance().pt_ = pt;
        Game.getInstance().town_ = town;
        Game.getInstance().town_pt_ = pt;
        Game.getInstance().town_gate_ = gt;
        Game.getInstance().bt_ = bt;
    }


}
