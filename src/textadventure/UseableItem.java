package textadventure;

public abstract class UseableItem extends Item
{

    public UseableItem(String name, String desc, int price)
    {
        super(name, desc, price);
    }

    public abstract void effect(Player player);

    public static void recoverHP(Player player, int amount)
    {
        player.stats_.gainHP(amount);
        System.out.println(player.name_ + " recovered " + CP.getColorString("" + amount, CP.GREEN) + " HP");
    }

    public static void recoverMP(Player player, int amount)
    {
        player.stats_.gainMP(amount);
        System.out.println(player.name_ + " recovered " + CP.getColorString("" + amount, CP.BLUE) + " MP");
    }

}
